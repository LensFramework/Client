﻿

namespace Lens.Gameplay.Managers
{
    public enum ETimelineCycleType
    {
        GraphStart,
        GraphEnd,
        BehaviorStart,
        BehaviorEnd,
        BehaviorUpdate
    }
}