using ProtoBuf;
using System;
using System.IO;
using System.Net;

namespace Lens.Framework.Managers
{
  public interface INetListener {
    // 加密
    byte[] Encrypt(byte[] bytes);
    // 解密
    byte[] Decrypt(byte[] bytes);
    object WrapUnpack(int msgId,byte[] bytes);
  }
}