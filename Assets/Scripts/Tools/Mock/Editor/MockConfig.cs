﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ProtocolSimulation
{
    public class MockConfig : ScriptableObject
    {
        public string nameSpace= "com.lh.demo.protocol";
        public string assembly = "protos";
        public string[] protoIDDefinePaths = new string[1];
    }
}