﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Core
{
    public enum EVariantType
    {
        /// <summary>
        /// null
        /// </summary>
        n=0,
        /// <summary>
        /// low
        /// </summary>
        l=1,
        /// <summary>
        /// Middle
        /// </summary>
        m=2,
        /// <summary>
        /// High
        /// </summary>
        h=3
    }
}
