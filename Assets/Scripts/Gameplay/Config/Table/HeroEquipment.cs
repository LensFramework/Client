namespace Lens.Gameplay.Config
{
    [System.Serializable]
    public class THeroEquipment
    {
        private HeroEquipment[] m_array;
        public HeroEquipment GetId(int index)
        {
            for (int i = 0; i < m_array.Length; i++)
            {
                if (m_array[i].id == index)
                {
                    return m_array[i];
                }
            }
            return null;
        }
    }
    [System.Serializable]
    public class HeroEquipment
    {
        public int id { get; set; }
        public int heroId { get; set; }
        public int heroQuality { get; set; }
        public int seq { get; set; }
        public int propId { get; set; }
        public int equipRank { get; set; }
    }
}
