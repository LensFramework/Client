/*
** $Id: linit.c,v 1.14.1.1 2007/12/27 13:02:25 roberto Exp $
** Initialization of libraries for lua.c
** See Copyright Notice in lua.h
*/


#define linit_c
#define LUA_LIB

#include "lua.h"

#include "luaextlib.h"
#include "lauxlib.h"


static const luaL_Reg luaextlibs[] = {
	{ "", luaopen_base_ext },
//  {LUA_LOADLIBNAME, luaopen_package},
  { LUA_TABLIBNAME, luaopen_table_ext },
//  {LUA_IOLIBNAME, luaopen_io},
//  {LUA_OSLIBNAME, luaopen_os},
//  {LUA_STRLIBNAME, luaopen_string},
//  {LUA_MATHLIBNAME, luaopen_math},
//  {LUA_DBLIBNAME, luaopen_debug},
  {NULL, NULL}
};


LUALIB_API void luaL_openextlibs(lua_State *L) {
	const luaL_Reg *lib = luaextlibs;
  for (; lib->func; lib++) {
    lua_pushcfunction(L, lib->func);
    lua_pushstring(L, lib->name);
    lua_call(L, 1, 0);
  }
}

