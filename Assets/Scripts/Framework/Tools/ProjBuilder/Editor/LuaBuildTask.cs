﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor.Build.Pipeline;
using UnityEditor.Build.Pipeline.Interfaces;
using UnityEngine;

public class LuaBuildTask 
{
    public bool useArm64 = false;
#if UNITY_EDITOR_OSX
    #if UNITY_IPHONE || UNITY_IOS
        private  static readonly string _luajit = "../Lua/tools/luajit/mac/ios64";
    #elif UNITY_ANDROID
        private static readonly string _luajit = "../Lua/tools/luajit/mac/android32";
        private static readonly string _luajit64 = "../Lua/tools/luajit/mac/android64";
    #endif
#else
        private static readonly string _luajit = "../Lua/tools/luajit/windows/android32";
#endif
    public void Run()
    {
        StringBuilder strErr = new StringBuilder();
        bool hasErr = false;
        CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../Lua/src"), Application.dataPath + "/ResourcesAssets/Lua/src", strErr, _luajit);
        CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../Lua/lua_modules"), Application.dataPath + "/ResourcesAssets/Lua/lua_modules", strErr, _luajit);
        CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../../Share/Config/Lua"), Application.dataPath + "/ResourcesAssets/Lua/config", strErr, _luajit);

#if UNITY_EDITOR_OSX && UNITY_ANDROID
        if (useArm64)
        {
            CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../Lua/src"), Application.dataPath + "/ResourcesAssets/Lua64/src", strErr, _luajit64);
            CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../Lua/lua_modules"), Application.dataPath + "/ResourcesAssets/Lua64/lua_modules", strErr, _luajit64);
            CopyLuaBytesFilesJit(ref hasErr, Path.Combine(Application.dataPath, "../../Share/Config/Lua"), Application.dataPath + "/ResourcesAssets/Lua64/config", strErr, _luajit64);
        }
#endif
        if (hasErr)
        {
            throw new System.Exception("buildLuaTask :" + strErr);
        }
    }
    private void CopyLuaBytesFilesJit(ref bool hasErr, string sourceDir, string destDir, StringBuilder strErr, string luajit)
    {
        if (!Directory.Exists(sourceDir))
        {
            hasErr = true;
            strErr.AppendLine("Dont has sourceDir:" + sourceDir);
            return;
        }
        sourceDir = sourceDir.Replace("\\", "/");
        string[] files = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories); //lua文件后缀必须是小写<.lua>
        int len = sourceDir.Length;
        string luajitPath = Path.Combine(Application.dataPath, luajit);
        if (sourceDir[len - 1] == '/')
        {
            sourceDir = sourceDir.Substring(0, len - 1);
            --len;
        }
        for (int i = 0; i < files.Length; i++)
        {
            FileInfo info = new FileInfo(files[i]);
            string src = files[i];
            if (info.Extension == ".lua")
            {
                files[i] = files[i].Replace("\\", "/");
                string str = files[i].Remove(0, len + 1);
                string dest = destDir + "/" + str;
                string dir = Path.GetDirectoryName(dest);
                Directory.CreateDirectory(dir);
                using (System.Diagnostics.Process proc = new System.Diagnostics.Process())
                {
                    proc.StartInfo.FileName = string.Format("{0}/luajit", luajitPath);
                    proc.StartInfo.WorkingDirectory = luajitPath;
                    string tmp = files[i].Remove(0, files[i].IndexOf(sourceDir) + sourceDir.Length + 1);
                    src = string.Format("{0}/luajit_tmp/{1}", luajitPath, tmp);
                    Directory.CreateDirectory(Path.GetDirectoryName(src));
                    proc.StartInfo.Arguments = string.Format("-b {0} {1}", files[i], src);
                    proc.StartInfo.CreateNoWindow = true;

                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.RedirectStandardError = true;
                    proc.StartInfo.RedirectStandardInput = true;
                    proc.EnableRaisingEvents = true;

                    StringBuilder errorSb = new StringBuilder();
                    proc.ErrorDataReceived += (sender, args) => errorSb.Append(args.Data);

                    proc.Start();
                    proc.BeginErrorReadLine();
                    proc.WaitForExit();

                    if (errorSb.Length > 0)
                    {
                        if (null != strErr)
                        {
                            hasErr = true;
                            strErr.Append(errorSb.ToString() + "\n");
                        }
                        Debug.LogError(errorSb.ToString());
                        continue;
                    }
                    dest = dest.Replace(".lua", ".bytes");
                    File.Copy(src, dest, true);
                    UnityEditor.AssetDatabase.ImportAsset(dest.Replace(Application.dataPath, "Assets"));
                }
                //UpdateProgress("拷贝中", n++, files.Length, dest);
            }
            else
            {
                string str = files[i].Remove(0, len);
                if (str.Substring(0, 2) == "/.")
                {
                    continue;
                }
                string dest = destDir + str;
                FileInfo destInfo = new FileInfo(dest);
                if (!destInfo.Directory.Exists)
                {
                    destInfo.Directory.Create();
                }
                File.Copy(src, dest, true);
                UnityEditor.AssetDatabase.ImportAsset(dest.Replace(Application.dataPath, "Assets"));
                //File.SetAttributes(targetPath, FileAttributes.Normal);
            }
        }
        Directory.Delete(string.Format("{0}/luajit_tmp", luajitPath), true);
    }

}