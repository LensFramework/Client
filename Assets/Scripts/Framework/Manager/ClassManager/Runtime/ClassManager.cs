﻿//#define LOG
using System;
using System.Collections;
using System.Collections.Generic;
#if LOG
using System.IO;
using System.Text;
#endif
using UnityEngine;

namespace Lens.Framework.Managers
{
#pragma warning disable 0649, 0067
    using Core;
    using System.Threading.Tasks;

    public class ClassManager:Singleton<ClassManager>
    {
        public const string tag="ClassManager";
        public static DataHandler<Type> storeHandler;
        public static DataHandler<Type> getHandler;
        public static DataHandler<Type> freeHandler;
        public static DataHandler<Type> clearHandler;
        public static Dictionary<int,Pool>[] source { get { return p_instance.m_dic; } }
        public class Pool
        {
            public Type type;
            public int index;
            public int group;
            public string typeName;
            public List<IClass> freeList=new List<IClass>();
    #if UNITY_EDITOR
            public int getCount;
            public int freeCount;
            public int storeCount;
            public List<IClass> allList = new List<IClass>(5);
    #endif
            public Pool()
            {

            }
            public Pool(Type type, int count,int group)
            {
    #if UNITY_EDITOR
                storeCount = count;
    #endif
                this.group = group;
                index = type.MetadataToken;
                this.type = type;
                this.typeName = type.Name;
                freeList = new List<IClass>(count);
                for (int i = 0; i < count; i++)
                {
                    var cla = Activator.CreateInstance(type) as IClass;
                    freeList.Add(cla);
    #if UNITY_EDITOR
                    allList.Add(cla);
    #endif
                }
            }
            public void Store(int count)
            {
                for (int i = 0; i < count; i++)
                {
                    var cla = Activator.CreateInstance(type) as IClass;
                    freeList.Add(cla);
    #if UNITY_EDITOR
                    allList.Add(cla);
    #endif
                }
            }
            public IClass GetObject()
            {
    #if UNITY_EDITOR
                getCount++;
    #endif
                if (freeList.Count <= 0)
                {
                    var cla = Activator.CreateInstance(type) as IClass;
    #if UNITY_EDITOR
                    allList.Add(cla);
    #endif
                    return cla;
                }
                else
                {
                    var l = freeList[0];
                    freeList.RemoveAt(0);
                    return l;
                }
            }
            public void FreeObject(IClass obj)
            {
    #if UNITY_EDITOR
                if (freeList.Contains(obj))
                {
                    Log("Has this Obj=>" + obj);
                    return;
                }
                freeCount++;
    #endif
                obj.OnReset();
                freeList.Add(obj);
            }
    #if UNITY_EDITOR
            public int GetCount()
            {
                return allList.Count;
            }

    #endif
            public void Clear()
            {
                freeList.Clear();
    #if UNITY_EDITOR
                allList.Clear();
    #endif
            }
            public void Reset()
            {
                type = null;
                index = 0;
                typeName = null;
                if (freeList!=null)
                {
                    freeList.Clear();
                }
    #if UNITY_EDITOR
                if (allList!=null)
                {
                    allList.Clear();
                }
    #endif
            }
        }
        private Dictionary<int, Pool>[] m_dic;
        private Action<string,string> m_logHandler;
        public async override Task Initialize(Options options=null)
        {
            m_dic=new Dictionary<int, Pool>[20];
            m_dic[0] = new Dictionary<int, Pool>();
        }
        public override void Dispose()
        {
            storeHandler = null;
            getHandler = null;
            clearHandler = null;
            freeHandler = null;
            p_instance = null;
        }
        public void SetLogHandler(Action<string,string> callback){
            m_logHandler=callback;
        }
        public static void AddGroup(int group){
            p_instance.m_dic[group]=new Dictionary<int, Pool>();
        }
        public static void RemoveGroup(int group){
            p_instance.m_dic[group]=null;
        }
        public static void Store(Type type, int count, int group=0)
        {
            int index = type.MetadataToken;
            var g = p_instance.m_dic[group];
            if (g.ContainsKey(index))
            {
                g[index].Store(count);
            }
            else
            {
                g.Add(index, new Pool(type, count,group));
            }
            if (storeHandler != null)
                storeHandler(type);
        }
        public static void Store<T>( int count,int group = 0)
        {
            Type type = typeof(T);
            Store(type, count,group);
        }
        public static IClass Get(string name,int group=0)
        {
            bool has = false;
            int index = 0;
            IClass cls;
            var g = p_instance.m_dic[group];
            foreach (var item in g)
            {
                if (name == item.Value.typeName)
                {
                    has = true;
                    index = item.Key;
                }
            }
            if (has)
            {
                cls= g[index].GetObject();
            }
            else
            {
                Type type=Type.GetType(name);
                g.Add(index, new Pool(type, 1,group));
                cls= g[index].GetObject();
            }
            if (getHandler != null)
                getHandler(cls.GetType());
            return cls;
        }
        public static T Get<T>(int group=0) where T :IClass
        {
            Type type = typeof(T);
            IClass cls;
            int index = type.MetadataToken;
            var g = p_instance.m_dic[(int)group];
            Pool pool;
            if (g.TryGetValue(index,out pool))
            {
                cls= pool.GetObject();
            }
            else
            {
                g.Add(index, new Pool(type, 1,group));
                cls=g[index].GetObject();
            }
            if (getHandler != null)
                getHandler(cls.GetType());
            return (T)cls;
        }
        public static void Free(IClass obj,int group=0)
        {
            if (obj == null || p_instance == null) return;
            int index = (int)obj.GetType().MetadataToken;
            var g = p_instance.m_dic[(int)group];
            Pool pool;
            if (g.TryGetValue(index,out pool))
            {
                pool.FreeObject(obj);
            }
            else
            {
                Log("LaoHan: freeObject dont has this type =>" + obj.GetType());
            }
            if (freeHandler != null)
                freeHandler(obj.GetType());
        }
        public void Clear<T>(int group=0)
        {
            var g = p_instance.m_dic[group];
            Type type = typeof(T);
            int index = type.MetadataToken;
            g[index].Clear();
            if (clearHandler != null)
                clearHandler(type);
        }
        public void Clear(int group=0)
        {
            var g = p_instance.m_dic[group];
            foreach (var item in g)
            {
                clearHandler(item.Value.type);
                item.Value.Clear();
            }
        }
        private static void Log(string str){
            if(p_instance.m_logHandler!=null){
                p_instance.m_logHandler(tag,str);
            }
            else {
                UnityEngine.Debug.Log(str);
            }
        }
    }
}