return {
    state=require("store/user/state"),
    getters = require("store/user/getters"),
    mutations = require("store/user/mutations"),
    actions = require("store/user/actions")
}
