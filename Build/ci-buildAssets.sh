#!/bin/bash -ilex
branch=$1
platform=$2
major=$3
weekly=$4
revision=$5
user=$5
ip=$6


remoteBuildPath="Dist/ServerData/[BuildTarget]"
remoteLoadPath="http://${ip}:8080/lensframework/versions/${major}_${branch}/[BuildTarget]"


version=${major}.${weekly}.${revision}_${branch}
curDir=$(cd `dirname $0`; pwd)
cd ..
project_path=$(cd `dirname $0`; pwd)
cd $curDir
if [ ! -d "../Dist/ServerData" ]; then
  echo "buildAsset must Build ofter package,ServerData dont exists"
  exit 0
fi
Unity -quit -batchmode -projectPath $project_path  -logFile logs/buildAssets.log -executeMethod Builder.BuildAssets  remoteBuildPath=$remoteBuildPath remoteLoadPath=$remoteLoadPath platform=$platform versoin=$version
cd ../Dist/ServerData
tar zcvf ServerData.tar.gz *
cd ..

scp ServerData/ServerData.tar.gz $user@$ip:/usr/share/nginx/html/lensframework/versions/${major}_${branch}

expect << EOF
  spawn ssh $user@$ip
  expect "*#"
  send "cd /usr/share/nginx/html/lensframework/versions/${major}_${branch}\n"
  expect "*#"
  send "rm -rf ${platform}\n"
  expect "*#"
  send "tar -zxvf ServerData.tar.gz\n"
  expect "*#"
  send "rm -rf ServerData.tar.gz\n"
  expect "*#"
  send "logout\n"
  expect eof
EOF

rm -rf ServerData/ServerData.tar.gz
cd $curDir
echo "SUCCESS!!!!!"