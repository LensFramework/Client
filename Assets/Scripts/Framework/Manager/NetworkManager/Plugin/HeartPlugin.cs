﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lens.Framework.Managers
{
    public class HeartPlugin: NetPluginBase
    {
        public float heartInterval = 2;
        private float m_time;
        public override void Update()
        {
            base.Update();

            if (UnityEngine.Time.time - m_time > heartInterval)
            {
                m_time = UnityEngine.Time.time;
                if (TcpSocket.IsConnected)
                {
                    TcpSocket.Send((int)EMsgType.Heart, new byte[] { });
                }
            }
        }
    }
}
