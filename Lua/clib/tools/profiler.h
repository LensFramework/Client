#pragma once

#if LUA_VERSION_NUM == 501

#define LUAMOD_API LUALIB_API
/* test for pseudo index */
#define ispseudo(i)		((i) <= LUA_REGISTRYINDEX)
/*
** convert an acceptable stack index into an absolute index
*/
LUA_API int lua_absindex(lua_State *L, int idx) {
	return (idx > 0 || ispseudo(idx))
		? idx
		: lua_gettop(L) + 1 + idx;
	//: cast_int(L->top - L->ci->func) + idx;
}

static void
luaL_checkversion(lua_State *L) {
	if (lua_pushthread(L) == 0) {
		luaL_error(L, "Must require in main thread");
	}
	lua_setfield(L, LUA_REGISTRYINDEX, "mainthread");
}

static void
lua_rawsetp(lua_State *L, int idx, const void *p) {
	idx = lua_absindex(L, idx);
	lua_pushlightuserdata(L, (void *)p);
	lua_insert(L, -2);
	lua_rawset(L, idx);
}

static char*
lua_rawgetp(lua_State *L, int idx, const void *p) {
	idx = lua_absindex(L, idx);
	lua_pushlightuserdata(L, (void *)p);
	lua_rawget(L, idx);
	return lua_type(L, -1);
}
/*
** ensure that stack[idx][fname] has a table and push that table
** into the stack
*/
LUALIB_API int luaL_getsubtable(lua_State *L, int idx, const char *fname) {
	lua_getfield(L, idx, fname);
	if (lua_type(L, -1) == LUA_TTABLE)
		return 1;  /* table already there */
	else {
		lua_pop(L, 1);  /* remove previous result */
		idx = lua_absindex(L, idx);
		lua_newtable(L);
		lua_pushvalue(L, -1);  /* copy to be left at top */
		lua_setfield(L, idx, fname);  /* assign new table to field */
		return 0;  /* false, because did not find table there */
	}
}

#define luaL_newlib(L ,reg) luaL_register(L,"profiler",reg)

#else

#define mark_function_env(L,dL,t)

#endif
