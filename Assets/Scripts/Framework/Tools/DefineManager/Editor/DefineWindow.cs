﻿/**
* Editor Wizard for easily managing global defines in Unity
* Place in Assets/Editor folder, or if you choose to place elsewhere
* be sure to also modify the DEF_MANAGER_PATH constant.
* @khenkel
*/

using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Lens.Framework.Tools
{
    public class DefineWindow : EditorWindow
    {
        bool m_repaint = false;
        static DefineWindow m_instance;
        [MenuItem("Tools/Define Manager")]
        static void Init()
        {
            m_instance = EditorWindow.GetWindow<DefineWindow>(true, "Global Define Manager", true);
            m_instance.Show();
        }
        private void OnDestroy()
        {
            m_instance = null;
        }

        private DefineManager.ECompileMode m_current;
        List<DefineManager.ModeData> defs;
        Vector2 scroll = Vector2.zero;
        void OnGUI()
        {
            if (EditorApplication.isPlaying || EditorApplication.isCompiling)
            {
                EditorGUILayout.LabelField("Updating......");
                Repaint();
                return;
            }
            m_repaint = false;
            if (m_instance == null)
            {
                Init();
                return;
            }
            Color oldColor = GUI.backgroundColor;
            GUILayout.BeginHorizontal();
            if (m_current == DefineManager.ECompileMode.Development)
            {
                GUI.backgroundColor = Color.gray;
                defs = DefineManager.defineData.developments;
            }
            if (GUILayout.Button(DefineManager.ECompileMode.Development.ToString(), EditorStyles.miniButtonLeft))
            {
                m_current = DefineManager.ECompileMode.Development;
                EditorUtility.FocusProjectWindow();
            }
            GUI.backgroundColor = oldColor;
            if (m_current == DefineManager.ECompileMode.Debug)
            {
                GUI.backgroundColor = Color.gray;
                defs = DefineManager.defineData.debugs;
            }
            if (GUILayout.Button(DefineManager.ECompileMode.Debug.ToString(), EditorStyles.miniButtonMid))
            {
                EditorUtility.FocusProjectWindow();
                m_current = DefineManager.ECompileMode.Debug;
            }
            GUI.backgroundColor = oldColor;
            if (m_current == DefineManager.ECompileMode.Release)
            {
                GUI.backgroundColor = Color.gray;
                defs = DefineManager.defineData.Releases;
            }
            if (GUILayout.Button(DefineManager.ECompileMode.Release.ToString(), EditorStyles.miniButtonRight))
            {
                m_current = DefineManager.ECompileMode.Release;
                EditorUtility.FocusProjectWindow();
            }
            GUI.backgroundColor = oldColor;
            GUILayout.EndHorizontal();


            GUILayout.Label(m_current.ToString() + " User Defines");

            scroll = GUILayout.BeginScrollView(scroll);
            for (int i = 0; i < defs.Count; i++)
            {
                GUILayout.BeginHorizontal();
                defs[i].enabled = EditorGUILayout.Toggle(defs[i].enabled, GUILayout.MaxWidth(18));
                defs[i].name = EditorGUILayout.TextField(defs[i].name);

                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("x", GUIStyle.none, GUILayout.MaxWidth(18)))
                {
                    if (EditorUtility.DisplayDialog("information", "Are you delete sure?", "Ok", "Cancel"))
                    {
                        defs.RemoveAt(i);
                    }
                }
                GUI.backgroundColor = oldColor;

                GUILayout.EndHorizontal();

            }

            GUILayout.Space(4);

            GUI.backgroundColor = Color.cyan;
            if (GUILayout.Button("Add"))
                defs.Add(new DefineManager.ModeData()
                {
                    name = "NEW_DEFINE",
                    enabled = true
                });

            GUILayout.EndScrollView();


            GUILayout.BeginHorizontal();
            if (m_current == DefineManager.defineData.actived)
            {
                GUI.backgroundColor = Color.green;
            }
            else
            {
                GUI.backgroundColor = Color.white;
            }
            if (GUILayout.Button("Apply"))
            {
                DefineManager.SetDefines(m_current);
                m_repaint = true;
            }
            GUI.backgroundColor = oldColor;
            if (m_repaint)
            {
                Repaint();
            }
        }

    }
}