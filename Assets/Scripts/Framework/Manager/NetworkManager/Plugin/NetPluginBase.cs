﻿/***************************************************************
 * Description: 
 *
 * Documents: https://github.com/hiramtan/HiSocket
 * Author: hiramtan@live.com
***************************************************************/

namespace Lens.Framework.Managers
{
    public abstract class NetPluginBase : INetPlugin
    {
        public TcpSocket TcpSocket { get; set; }
        public virtual void Initialize()
        {

        }
        public virtual void Update()
        {

        }
    }
}
