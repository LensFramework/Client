local page=class("selectserver",G_StackBase)
local tag="page:selectserver"

local function onBack()
    L_UI.back()
end

local function onEnter()
    L_UI.open("loading"):next(function ()
        L_UI.open("main",{isReplace=true})
    end)
end

function page:ctor(...)

end

function page.created(obj,config)
    print(tag,obj,config)
    local loginBtn=obj.transform:Find("SelectHeroForms/ImgReturn")
    local btn=loginBtn:GetComponent(typeof(UnityEngine.UI.Button))
    btn.onClick:AddListener(onBack)

    local okBtn=obj.transform:Find("SelectHeroForms/BtnOK")
    local obtn=okBtn:GetComponent(typeof(UnityEngine.UI.Button))
    obtn.onClick:AddListener(onEnter)
end

function page.open(options)
    print(tag,"open")
end

function page.close(options)
    
end

function page.destroy(options)
    
end

return page