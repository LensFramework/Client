#!/bin/bash
jenkinsWorkspace=$1

curDir=$(cd `dirname $0`; pwd)
cd ..
project_path=$(cd `dirname $0`; pwd)
cd $curDir
Unity -quit -batchmode -projectPath $project_path  -logFile logs/checkAssets.log -executeMethod Builder.CheckAssets maintainer_filePath=${jenkinsWorkspace}/attachments/maintainer.log