﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lens.Framework.Managers
{
    public sealed class ReconnectPlugin : NetPluginBase
    {
        public event Action reconnectFailureHandler;
        public event Action reconnectedHandler;
        public event Action reconnectingHandler;
        private int m_autoResendCount = 3;
        private int m_curCount = 0;
        private bool m_startReconnect;
        private bool m_connected;
        public override void Initialize()
        {
            base.Initialize();
            TcpSocket.sendFailureHandler += OnSendFailure;
            TcpSocket.connectFailureHandler += OnConnectFailure;
        }

        private void OnConnected()
        {
            m_curCount = 0;
            TcpSocket.connectedHandler -= OnConnected;
            m_startReconnect = false;
        }

        private void OnConnectFailure(string obj)
        {
            ToReconnect();
        }

        private void OnSendFailure(string obj)
        {
            ToReconnect();
        }
        private void ToReconnect()
        {
            if (m_curCount < m_autoResendCount)
            {
                m_curCount++;
                if (m_startReconnect)
                {

                }
                else
                {
                    if (reconnectingHandler != null)
                    {
                        reconnectingHandler();
                    }
                    TcpSocket.connectedHandler += OnConnected;
                    m_startReconnect = true;
                }
                TcpSocket.Reconnect();
            }
            else
            {
                if (reconnectFailureHandler != null)
                {
                    reconnectFailureHandler();
                }
            }
        }
        public override void Update()
        {
            base.Update();
            if (TcpSocket.IsConnected)
            {
                m_connected = true;
            }
            else
            {
                if (m_connected)
                {
                    ToReconnect();
                    m_connected = false;
                }
            }
        }
    }
}
