﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public class BlockBuffer<T> : IBlockBuffer<T>, IDisposable
    {
        private enum EState
        {
            ReadAhead,
            WriteAhead,
            ReadWriteAreEqual
        }
        private int _blockSize = 1024;
        public T[] Buffer
        {
            get;
            private set;
        }
        public int Size
        {
            get
            {
                return this.Buffer.Length;
            }
        }
        public int BlockSize
        {
            get
            {
                return this._blockSize;
            }
            set
            {
                this._blockSize = value;
            }
        }
        public int ReadPosition
        {
            get;
            private set;
        }
        public int WritePosition
        {
            get;
            private set;
        }
        private BlockBuffer<T>.EState State
        {
            get
            {
                if (this.ReadPosition > this.WritePosition)
                {
                    return BlockBuffer<T>.EState.ReadAhead;
                }
                if (this.ReadPosition < this.WritePosition)
                {
                    return BlockBuffer<T>.EState.WriteAhead;
                }
                return BlockBuffer<T>.EState.ReadWriteAreEqual;
            }
        }
        public BlockBuffer(int size = 1024)
        {
            this.Buffer = new T[size];
        }
        public BlockBuffer(T[] buffer)
        {
            this.Buffer = buffer;
            this.MoveWritePosition(buffer.Length);
        }
        public void MoveReadPostion(int length)
        {
            this.ReadPosition += length;
        }
        public void MoveWritePosition(int length)
        {
            this.WritePosition += length;
        }
        public void MoveReadPositionTo(int index)
        {
            this.ReadPosition = index;
        }
        public void MoveWritePostionTo(int index)
        {
            this.WritePosition = index;
        }
        public T[] Read(int length)
        {
            T[] array = new T[length];
            this.Read(array, 0, length);
            return array;
        }
        public void Read(T[] destinationArray, int destinationIndex, int length)
        {
            System.Buffer.BlockCopy(this.Buffer, this.ReadPosition, destinationArray, destinationIndex, length);
            this.ReadPosition += length;
        }
        public void Write(T[] sourceArray)
        {
            this.Write(sourceArray, 0, sourceArray.Length);
        }
        public void Write(T[] sourceArray, int sourceIndex, int length)
        {
            int num = this.WritePosition + length;
            if (this.Size >= num)
            {
                System.Buffer.BlockCopy(sourceArray, sourceIndex, this.Buffer, this.WritePosition, length);
                this.WritePosition += length;
                return;
            }
            int num2 = sourceArray.Length / this.BlockSize;
            int num3 = sourceArray.Length % this.BlockSize;
            int num4 = this.WritePosition + 1;
            if (this.Size - num4 < num3)
            {
                num2++;
            }
            int num5 = num2 * this.BlockSize;
            T[] array = new T[this.Size + num5];
            System.Buffer.BlockCopy(this.Buffer, this.ReadPosition, array, 0, this.WritePosition - this.ReadPosition);
            System.Buffer.BlockCopy(sourceArray, sourceIndex, array, this.WritePosition - this.ReadPosition, length);
            this.WritePosition = this.WritePosition - this.ReadPosition + length;
            this.Buffer = array;
        }
        public void ResetIndex()
        {
            switch (this.State)
            {
                case BlockBuffer<T>.EState.ReadAhead:
                    this.ReadPosition = 0;
                    break;
                case BlockBuffer<T>.EState.WriteAhead:
                    {
                        int num = this.WritePosition - this.ReadPosition;
                        for (int i = 0; i < num; i++)
                        {
                            this.Buffer[i] = this.Buffer[this.ReadPosition + i];
                        }
                        this.ReadPosition = 0;
                        this.WritePosition = num;
                        break;
                    }
                case BlockBuffer<T>.EState.ReadWriteAreEqual:
                    this.ReadPosition = (this.WritePosition = 0);
                    return;
                default:
                    return;
            }
        }
        public void Dispose()
        {
            this.Buffer = null;
        }
    }
}
