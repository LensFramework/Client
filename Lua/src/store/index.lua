local store=require('lux.index')
store:initialize({
    modules={
        global=require('store.global.index'),
        csharp=require('store.csharp.index'),
        user=require('store.user.index')
    }
})
return store