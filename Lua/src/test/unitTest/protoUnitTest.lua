L_Proto=require "proto.index"
L_Proto.initialize(UMT.Framework.Managers.LuaManager.ReadByteFiles("proto/protocal.pb"))
require "net"
L_Net.initialize(L_Proto)
function start()
    L_Net.addNetStateHandler(function( netState )
        print(netState)
    end)
    L_Net.addListener("SC_ReadBattleEmailResponse",function( data )
        print(data.email.emailId)
        print(data.email.content.text)
    end)
end
start()