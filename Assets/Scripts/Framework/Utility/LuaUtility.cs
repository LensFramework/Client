﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Lens.Framework.Utility
{
    public partial class LuaUtility 
    {
        public static void InstantiateAsync(string key,Action<GameObject> callback)
        {
            Addressables.InstantiateAsync(key).Completed += delegate (UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> obj)
            {
                callback(obj.Result);
            };
        }
    }
}