﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class MainCity_MainCityProxyWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(MainCity.MainCityProxy), typeof(Lens.Framework.Core.Singleton<MainCity.MainCityProxy>));
		L.RegFunction("New", _CreateMainCity_MainCityProxy);
		L.RegFunction("__tostring", ToLua.op_ToString);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _CreateMainCity_MainCityProxy(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				MainCity.MainCityProxy obj = new MainCity.MainCityProxy();
				ToLua.PushObject(L, obj);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to ctor method: MainCity.MainCityProxy.New");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}

