﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Lens.Framework.Managers
{
    public class NetMessage 
    {
        public Dictionary<int, List<Action<object>>> p_dic = new Dictionary<int, List<Action<object>>>();
        public List<Action<int, object>> p_listenAlls = new List<Action<int, object>>();
        public Queue<byte[]> p_bytes = new Queue<byte[]>();
        public Queue<int> p_msgs = new Queue<int>();
        private INetListener m_listener;
        public NetMessage(INetListener listener)
        {
            m_listener=listener;
        }
        public void AddListener(int msgId, Action<object> callback)
        {
            List<Action<object>> list;
            if (p_dic.TryGetValue(msgId, out list))
            {
                list.Add(callback);
            }
            else
            {
                list = new List<Action<object>>();
                list.Add(callback);
                p_dic.Add(msgId, list);
            }
        }
        public void RemoveListener(int msgId, Action<object> callback)
        {
            List<Action<object>> list;
            if (p_dic.TryGetValue(msgId, out list))
            {
                list.Remove(callback);
            }
        }
        public void ToQueue(int msgId, byte[] bytes)
        {
            UnityEngine.Debug.Log(msgId+"  "+bytes.Length);
            p_msgs.Enqueue(msgId);
            p_bytes.Enqueue(bytes);
        }
        public void AddListenAll(Action<int, object> callback)
        {
            p_listenAlls.Add(callback);
        }
        public void RemoveListenAll(Action<int, object> callback)
        {
            p_listenAlls.Remove(callback);
        }

        public void Update()
        {
            if (p_bytes.Count > 0)
            {
                var bytes = p_bytes.Dequeue();
                var msgId = p_msgs.Dequeue();
                var des = m_listener.WrapUnpack(msgId,bytes);
                List<Action<object>> list;
                if (p_dic.TryGetValue(msgId, out list))
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i](des);
                    }
                    p_dic.Remove(msgId);
                }
                for (int i = 0; i < p_listenAlls.Count; i++)
                {
                    p_listenAlls[i](msgId, des);
                }
            }
        }

        
    }
}
