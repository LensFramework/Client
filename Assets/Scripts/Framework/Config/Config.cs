﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Config
{
    using Framework.Core;
    using System;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading.Tasks;

    public class Config:Singleton<Config>
    {

        public class MyOptions:Options
        {
            public string tableConfigPath = "Assets/ResourcesAssets/Config/Table";
            public string glConfigPath = "Assets/ResourcesAssets/Config/GlConfig.asset";
            public string tableExtension = ".bytes";
            public string jsonConfigPath = "../../Share/Config/Json";
            public string jsonExtension = ".json";
            public Type memConfigType;
            public Type tableConfigType;
        }
        private ITableConfig m_table;
        private IGlConfig m_global;
        private IMemConfig m_mem;
        private PrefConfig m_pref;
        public enum ELoadAssetType
        { 
            Scriptable,
            Bytes
        }
        public delegate Task<UnityEngine.Object> LoadAsset(string assetName);
        public LoadAsset loadAssetHandler;
        private MyOptions m_options;
        public async override Task Initialize(Options options=null)
        {
            m_options = options as MyOptions;
#if DEVELOPMENT
            JsonInitialize();
#else
            m_table = (ITableConfig)Activator.CreateInstance(m_options.tableConfigType);
            var fieldInfos = m_table.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var info in fieldInfos)
            {
                string tableFile = m_options.tableConfigPath+"/" +info.Name + m_options.tableExtension;
                var table = await p_instance.loadAssetHandler(tableFile);
                var textAsset = table as TextAsset;
                var o = p_instance.Deserialize(textAsset.bytes);
                info.SetValue(m_table, o);
            }
#endif
            if(p_instance.loadAssetHandler!=null)
            {
                var o= await p_instance.loadAssetHandler( m_options.glConfigPath);
                if (o != null)
                {
                    m_global = o as IGlConfig;
                }
            }
            m_mem = (IMemConfig)Activator.CreateInstance(m_options.memConfigType);
            m_pref = new PrefConfig();
        }
        public static T GetTableConfig<T>() where T:ITableConfig
        {
            return (T)p_instance.m_table;
        }
        public static T GetMemConfig<T>() where T : IMemConfig
        {
            return (T)p_instance.m_mem;
        }
        public static T GetGlConfig<T>() where T : IGlConfig
        {
            return (T)p_instance.m_global;
        }
        public static object GetMemConfig()
        {
            return p_instance.m_mem;
        }
        public static PrefConfig GetPrefConfig()
        {
            return p_instance.m_pref;
        }
        public override void Dispose()
        {
            m_table = null;
            m_global = null;
            m_mem = null;
            m_pref = null;
            base.Dispose();
        }
        public static void ForceDispose()
        {
            if (p_instance!=null)
            {
                p_instance.Dispose();
            }
        }
        public void SaveTable(MyOptions options,string folderPath,Action<string> callback )
        {
            m_options = options as MyOptions;
            JsonInitialize();
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            var fieldInfos = m_table.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var info in fieldInfos)
            {
                using (FileStream stream = new FileStream(folderPath+"/"+info.Name+m_options.tableExtension, FileMode.Create, FileAccess.Write))
                {
                    var bytes = p_instance.Serialize(info.GetValue(m_table));
                    stream.Write(bytes, 0, bytes.Length);
                }
                if (callback!=null)
                {
                    callback(info.Name + m_options.tableExtension);
                }
            }
        }
        private void JsonInitialize()
        {
            m_table = (ITableConfig)Activator.CreateInstance(m_options.tableConfigType);
            var fieldInfos = m_table.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
            foreach (var info in fieldInfos)
            {
                string jsonFile = Path.Combine(Application.dataPath, m_options.jsonConfigPath, info.Name + m_options.jsonExtension);
                if (!File.Exists(jsonFile))
                {
                    UnityEngine.Debug.LogError("jsonFile is dont exists:  " + jsonFile);
                    continue;
                }
                string jsonStr;
                using (FileStream fs = new FileStream(jsonFile, FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, bytes.Length);
                    jsonStr = System.Text.Encoding.UTF8.GetString(bytes);
                }
                var table = Activator.CreateInstance(info.FieldType);
                var listInfo = table.GetType().GetField("m_array", BindingFlags.NonPublic | BindingFlags.Instance);
                var o = LitJson.JsonMapper.ToObject(jsonStr, listInfo.FieldType);
                listInfo.SetValue(table, o);
                info.SetValue(m_table, table);
            }
        }
        private byte[] Serialize(System.Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memory = new MemoryStream();
            bf.Serialize(memory, obj);
            byte[] bytes = memory.GetBuffer();
            memory.Close();
            return bytes;
        }
        private object Deserialize(byte[] bytes)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memory = new MemoryStream(bytes);
            object ss = bf.Deserialize(memory);
            memory.Close();
            return ss;
        }
        
    }
}