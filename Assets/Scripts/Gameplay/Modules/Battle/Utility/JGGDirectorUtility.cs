﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#pragma warning disable 0414

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class JGGDirectorUtility : MonoBehaviour
{
    private void Awake()
    {
        //Mesh mesh = new Mesh();
        //this.GetComponent<MeshFilter>().mesh = mesh;
        ////Build(0.5f,1.2f, 0.25f, 4 * Mathf.Deg2Rad, 0);
        //CreateCenter(10, 3, 1.3f, 1.3f, 0.51f, 0.49f);
        //CreateRay(5, 1.6f, 1.6f, 0, 0.5f,0.4f, 0);
        //mesh.vertices = Vertices;
        //mesh.triangles = Triangles;
        //mesh.uv = UVs;
    }
    
    void OnDestroy()
    {
        var filter = this.GetComponent<MeshFilter>();
        if (filter!=null)
        {
            if (filter.mesh != null)
                UnityEngine.Object.Destroy(filter.mesh);
        }
    }

    //Fan data
    private float deltaDegSides = 5 * Mathf.Deg2Rad;
    private float outerRadius;
    private float outerSideRadius;
    private float innerSideRadius;
    private float innerRadius;
    private float lineWidth;
    private float degFan;
    private Vector3[] Vertices;
    private int[] Triangles;
    private Vector2[] UVs;

    float degSide = 15 * Mathf.Deg2Rad;
    float degCen = 5 * Mathf.Deg2Rad;
    float deg360 = 2 * Mathf.PI;
    float deg90 = 0.5f * Mathf.PI;
    float deg180 = Mathf.PI;

    public void SetLayer(int layer)
    {
        transform.gameObject.layer = layer;
        foreach (Transform trans in transform)
        {
            trans.gameObject.layer = layer;
        }
    }
    public void Create(float length, float height, float fixedLength, float fixedHeight, float uvLengthPercent, float uvHeightPercent)
    {
        Mesh mesh = new Mesh();
        this.GetComponent<MeshFilter>().mesh = mesh;
        CreateRectBottomCenter(length, height, fixedLength, fixedHeight, uvLengthPercent, uvHeightPercent);
        mesh.vertices = Vertices;
        mesh.triangles = Triangles;
        mesh.uv = UVs;
    }

    public void CreateCenter(float length, float height, float fixedLength, float fixedHeight, float uvLengthPercent, float uvHeightPercent)
    {
        Mesh mesh = new Mesh();
        this.GetComponent<MeshFilter>().mesh = mesh;
        CreateRectCenter(length, height, fixedLength, fixedHeight, uvLengthPercent, uvHeightPercent);
        mesh.vertices = Vertices;
        mesh.triangles = Triangles;
        mesh.uv = UVs;
    }

    public void CreateRay(float length, float height, float fixedLength, float fixedHeight, float uvLeftLengthPercent,float uvRightLengthPercent, float uvHeightPercent)
    {
        Mesh mesh = new Mesh();
        this.GetComponent<MeshFilter>().mesh = mesh;
        CreateRectRay(length, height, fixedLength, fixedHeight, uvLeftLengthPercent, uvRightLengthPercent, uvHeightPercent);
        mesh.vertices = Vertices;
        mesh.triangles = Triangles;
        mesh.uv = UVs;
    }

    public void Build(float innerRadius, float outerRadius, float lineWidth, float deltaDegSides, float degFan, Material meshMatt = null)
    {
        //MeshHelper.CheckMaterial(ref meshMatt);
        name = "ExFanMesh";
        this.deltaDegSides = deltaDegSides;
        this.outerRadius = outerRadius;
        this.innerRadius = innerRadius;
        this.outerSideRadius = Mathf.Max(innerRadius, outerRadius - lineWidth);
        this.innerSideRadius = Mathf.Min(innerRadius, innerRadius - lineWidth);
        this.innerSideRadius = this.innerSideRadius < 0 ? 0 : this.innerSideRadius;
        this.lineWidth = lineWidth;
        this.degFan = degFan;

        degFan = Mathf.Max(2 * (degSide + degCen), degFan);
        bool isFan = Mathf.Min(degFan, deg360) < deg360;
        bool isCircle = !isFan && innerRadius == 0;

        if (isCircle)
        {
            int sides = Mathf.FloorToInt(deg360 / deltaDegSides);
            int outterSides = sides * 2;
            int verticesCount = sides * 4 + 1;
            int trianglesCount = sides * (12 + 6);

            float angleDelta = deg360 / outterSides;

            Vertices = new Vector3[verticesCount];
            Triangles = new int[trianglesCount];
            UVs = new Vector2[verticesCount];

            float curAngle = 0;
            int verIndex = 0;
            int triangleIndex = 0;
            int uvIndex = 0;

            float uidx = 0.3333f;
            float uvDelta = 0.3333f / (float)outterSides;

            Vertices[verIndex++] = Vector3.zero;
            UVs[uvIndex++] = new Vector2(uidx, 0.0f);

            for (int i = 0; i < outterSides; i++)
            {
                Vector3 vcos_sin = new Vector3(Mathf.Cos(curAngle), Mathf.Sin(curAngle));
                Vector3 outerVertPos = vcos_sin * outerRadius;
                Vector3 outSideVertPos = vcos_sin * outerSideRadius;
                curAngle += angleDelta;

                int idx = verIndex;
                Vertices[verIndex++] = outerVertPos;
                Vertices[verIndex++] = outSideVertPos;

                UVs[uvIndex++] = new Vector2(uidx, 1.0f);
                UVs[uvIndex++] = new Vector2(uidx, 0.666f);
                uidx = Mathf.Abs(uidx + uvDelta);

                int a = idx;
                int b = idx + 1;
                int c = (verIndex % verticesCount);
                c = c < verIndex ? c + 1 : c;
                int d = (verIndex + 1) % verticesCount;
                d = d < verIndex ? d + 1 : d;

                //CCW
                Triangles[triangleIndex++] = b;
                Triangles[triangleIndex++] = d;
                Triangles[triangleIndex++] = c;
                Triangles[triangleIndex++] = c;
                Triangles[triangleIndex++] = a;
                Triangles[triangleIndex++] = b;

                Triangles[triangleIndex++] = b;
                Triangles[triangleIndex++] = 0;
                Triangles[triangleIndex++] = d;
            }
        }
        else if(!isFan && !isCircle)
        {
            int cenSides = Mathf.FloorToInt(degCen / deltaDegSides);
            int tiledSides = cenSides;
            int fanSides = cenSides * Mathf.FloorToInt(deg360 / degCen);

            int verticesCount = fanSides * 4;
            int trianglesCount = fanSides * 6 * 3;
            Vertices = new Vector3[verticesCount];
            Triangles = new int[trianglesCount];
            UVs = new Vector2[verticesCount];

            int verIndex = 0;
            int triangleIndex = 0;

            int uvIndex = 0;
            int uvCood = 0;

            float uidx = 0.0f;
            float uvDelta = 0.3333f / (float)cenSides;
            float angleDelta = deg360 / (float)fanSides;

            float curAngle = 0.0f;
            int sidesLoop = fanSides;
            for (int idx = 0; idx < sidesLoop; idx++)
            {
#if false
                if (uvCood == tiledSides)
                {
                    uidx = 0.6666f;
                    uvDelta = 0.3333f / (float)cenSides;
                    angleDelta = degCen / (float)cenSides;
                }
                else if (uvCood == cenSides)
                {
                    uidx = idx == outterSides ? 0.3333f : 0.6666f;
                    uvDelta = 0.3333f / (float)tiledSides;
                    uvDelta = idx == outterSides ? uvDelta : -uvDelta;
                    angleDelta = degTiled / (float)tiledSides;
                }

                UVs[uvIndex++] = new Vector2(uidx, 1.0f);
                UVs[uvIndex++] = new Vector2(uidx, 0.666f);
                UVs[uvIndex++] = new Vector2(uidx, 0.333f);
                UVs[uvIndex++] = new Vector2(uidx, 0.0f);
                uvCood++;
                uidx = Mathf.Abs(uidx + uvDelta);
#endif

                Vector3 vcos_sin = new Vector3(Mathf.Cos(curAngle), Mathf.Sin(curAngle));
                curAngle += angleDelta;

                Vector3 outerVertPos = vcos_sin * outerRadius;
                Vector3 outSideVertPos = vcos_sin * outerSideRadius;
                Vector3 innerVertPos = vcos_sin * innerRadius;
                Vector3 innerSideVertPos = vcos_sin * innerSideRadius;

                int curIdx = verIndex;
                Vertices[verIndex++] = outerVertPos;
                Vertices[verIndex++] = outSideVertPos;
                Vertices[verIndex++] = innerVertPos;
                Vertices[verIndex++] = innerSideVertPos;

                int a = curIdx;
                int b = curIdx + 1;
                int c = verIndex % verticesCount;
                int d = (verIndex + 1) % verticesCount;
                int e = curIdx + 2;
                int f = (verIndex + 2) % verticesCount;
                int g = curIdx + 3;
                int h = (verIndex + 3) % verticesCount;

                //CW
                Triangles[triangleIndex++] = b;
                Triangles[triangleIndex++] = a;
                Triangles[triangleIndex++] = d;
                Triangles[triangleIndex++] = d;
                Triangles[triangleIndex++] = a;
                Triangles[triangleIndex++] = c;

                Triangles[triangleIndex++] = b;
                Triangles[triangleIndex++] = d;
                Triangles[triangleIndex++] = e;
                Triangles[triangleIndex++] = d;
                Triangles[triangleIndex++] = f;
                Triangles[triangleIndex++] = e;

                Triangles[triangleIndex++] = e;
                Triangles[triangleIndex++] = f;
                Triangles[triangleIndex++] = g;
                Triangles[triangleIndex++] = g;
                Triangles[triangleIndex++] = f;
                Triangles[triangleIndex++] = h;
            }
        }
        else
        {
            float degHalf = degFan * 0.5f;
            float degTiled = Mathf.Clamp(degHalf - (degSide + degCen), 0, deg180);

            int outterSides = Mathf.FloorToInt(degSide / deltaDegSides);
            int cenSides = Mathf.FloorToInt(degCen / deltaDegSides);
            int tiledSides = Mathf.FloorToInt(degTiled / deltaDegSides);

            int mid = outterSides + tiledSides + cenSides;
            int fanSides = (mid) << 1;

            int verticesCount = !isFan ? fanSides * 4 : (fanSides + 1) * 4;
            int trianglesCount = fanSides * 6 * 3;
            Vertices = new Vector3[verticesCount];
            Triangles = new int[trianglesCount];
            UVs = new Vector2[verticesCount];

            int verIndex = 0;
            int triangleIndex = 0;

            int uvIndex = 0;

            float uidx = 0.0f;
            float uvDelta = 0.3333f / (float)outterSides;
            float angleDelta = degSide / (float)outterSides;

            float curAngle = degHalf;

            int sidesLoop = fanSides + 1;
            for (int idx = 0; idx < sidesLoop; idx++)
            {
                if (idx == outterSides + tiledSides)
                {
                    uidx = 0.6666f;
                    uvDelta = 0.3333f / (float)cenSides;
                    angleDelta = degCen / (float)cenSides;
                }
                else if (idx == fanSides - outterSides)
                {
                    uidx = 0.3333f;
                    uvDelta = -0.3333f / (float)outterSides;
                    angleDelta = degSide / (float)outterSides;
                }
                else if (/*tiledSides != 0 &&*/
                    (idx == outterSides || idx == (mid + cenSides)))
                {
                    uidx = idx == outterSides ? 0.3333f : 0.6666f;
                    uvDelta = 0.3333f / (float)tiledSides;
                    uvDelta = idx == outterSides ? uvDelta : -uvDelta;
                    angleDelta = degTiled / (float)tiledSides;
                }

                UVs[uvIndex++] = new Vector2(uidx, 1.0f);
                UVs[uvIndex++] = new Vector2(uidx, 0.666f);
                UVs[uvIndex++] = new Vector2(uidx, 0.333f);
                UVs[uvIndex++] = new Vector2(uidx, 0.0f);

                uidx = Mathf.Abs(uidx + uvDelta);

                Vector3 vcos_sin = new Vector3(Mathf.Cos(curAngle), Mathf.Sin(curAngle));
                curAngle -= angleDelta;

                Vector3 outerVertPos = vcos_sin * outerRadius;
                Vector3 outSideVertPos = vcos_sin * outerSideRadius;
                Vector3 innerVertPos = vcos_sin * innerRadius;
                Vector3 innerSideVertPos = vcos_sin * innerSideRadius;

                int curIdx = verIndex;
                Vertices[verIndex++] = outerVertPos;
                Vertices[verIndex++] = outSideVertPos;
                Vertices[verIndex++] = innerVertPos;
                Vertices[verIndex++] = innerSideVertPos;

                if (triangleIndex >= trianglesCount)
                    break;

                int a = curIdx;
                int b = curIdx + 1;
                int c = verIndex;
                int d = (verIndex + 1);
                int e = curIdx + 2;
                int f = (verIndex + 2);
                int g = curIdx + 3;
                int h = (verIndex + 3);
                if (mid > idx)
                {
                    //CW
                    Triangles[triangleIndex++] = b;
                    Triangles[triangleIndex++] = a;
                    Triangles[triangleIndex++] = c;
                    Triangles[triangleIndex++] = c;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = b;

                    Triangles[triangleIndex++] = b;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = e;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = e;

                    Triangles[triangleIndex++] = e;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = g;
                    Triangles[triangleIndex++] = g;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = h;
                }
                else
                {
                    //CW
                    Triangles[triangleIndex++] = b;
                    Triangles[triangleIndex++] = a;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = a;
                    Triangles[triangleIndex++] = c;

                    Triangles[triangleIndex++] = b;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = e;
                    Triangles[triangleIndex++] = d;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = e;

                    Triangles[triangleIndex++] = e;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = g;
                    Triangles[triangleIndex++] = g;
                    Triangles[triangleIndex++] = f;
                    Triangles[triangleIndex++] = h;
                }

            }
        }
    }

    private void CreateRectCenter(float length,float height,float fixedLength,float fixedHeight,float uvLengthPercent,float uvHeightPercent)
    {
        Vertices = new Vector3[16];
        Triangles = new int[18*3];
        UVs = new Vector2[16];
        var verticeIndex = 0;
        Vertices[verticeIndex++] = Vector3.zero+ new Vector3(0 - length/2,0-height/2,0);
        Vertices[verticeIndex++] = new Vector3(fixedLength - length / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - length / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - length / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - length / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - length / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - length / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - length / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - length / 2, height- fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - length / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - length / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - length / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - length / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - length / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - length / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - length / 2, height - height / 2);

        for (int i = 0, vi = 0; i < 18; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i+1] = vi;
            Triangles[i+2] = vi + 4;
            Triangles[i+3] = vi + 5;
            Triangles[i+4] = vi + 1;
            Triangles[i+5] = vi;
        }
        for (int i = 18, vi = 4; i < 36; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        for (int i = 36, vi = 8; i < 54; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        int uvIndex = 0;
        UVs[uvIndex++] = new Vector2(1,0);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 0);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 0);
        UVs[uvIndex++] = new Vector2(1, 0);
        UVs[uvIndex++] = new Vector2(1, uvHeightPercent);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, uvHeightPercent);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, uvHeightPercent);
        UVs[uvIndex++] = new Vector2(1, uvHeightPercent);
        UVs[uvIndex++] = new Vector2(1, 1 - uvHeightPercent);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 1 - uvHeightPercent);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 1 - uvHeightPercent);
        UVs[uvIndex++] = new Vector2(1, 1 - uvHeightPercent);
        UVs[uvIndex++] = new Vector2(1, 1);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(1, 1);

        //UVs[uvIndex++] = new Vector2(uvLengthPercent, 0);
        //UVs[uvIndex++] = new Vector2(1- uvLengthPercent, 0);
        //UVs[uvIndex++] = new Vector2(1, 0);
        //UVs[uvIndex++] = new Vector2(0, uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(uvLengthPercent, uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(1 - uvLengthPercent, uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(1, uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(0, 1-uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(uvLengthPercent, 1-uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(1-uvLengthPercent, 1 - uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(1, 1-uvHeightPercent);
        //UVs[uvIndex++] = new Vector2(0, 1);
        //UVs[uvIndex++] = new Vector2(uvLengthPercent, 1);
        //UVs[uvIndex++] = new Vector2(1-uvLengthPercent, 1);
        //UVs[uvIndex++] = new Vector2(1, 1);
    }

    private void CreateRectBottomCenter(float length, float height, float fixedLength, float fixedHeight, float uvLengthPercent, float uvHeightPercent)
    {
        Vertices = new Vector3[16];
        Triangles = new int[18 * 3];
        UVs = new Vector2[16];
        var verticeIndex = 0;
        Vertices[verticeIndex++] = Vector3.zero + new Vector3(0, 0 - height / 2, 0);
        Vertices[verticeIndex++] = new Vector3(fixedLength, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(0, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length, height - height / 2);

        for (int i = 0, vi = 0; i < 18; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        for (int i = 18, vi = 4; i < 36; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        for (int i = 36, vi = 8; i < 54; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        int uvIndex = 0;
        UVs[uvIndex++] = Vector2.zero;
        UVs[uvIndex++] = new Vector2(1, 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvLengthPercent, 0.5f);
        UVs[uvIndex++] = new Vector2(1, 0.5f);
        UVs[uvIndex++] = new Vector2(0, uvHeightPercent/2+0.5f);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvLengthPercent, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(0, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvLengthPercent, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(0, 1);
        UVs[uvIndex++] = new Vector2(uvLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(1 - uvLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(1, 1);
    }

    private void CreateRectRay(float length, float height, float fixedLength, float fixedHeight, float uvLeftLengthPercent,float uvRightLengthPercent, float uvHeightPercent)
    {
        Vertices = new Vector3[16];
        Triangles = new int[18 * 3];
        UVs = new Vector2[16];
        var verticeIndex = 0;
        Vertices[verticeIndex++] = Vector3.zero + new Vector3(0- height / 2, 0 - height / 2, 0);
        Vertices[verticeIndex++] = new Vector3(fixedLength - height / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - height / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - height / 2, 0 - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - height / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - height / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - height / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - height / 2, fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - height / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - height / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - height / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - height / 2, height - fixedHeight - height / 2);
        Vertices[verticeIndex++] = new Vector3(0 - height / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(fixedLength - height / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - fixedLength - height / 2, height - height / 2);
        Vertices[verticeIndex++] = new Vector3(length - height / 2, height - height / 2);

        for (int i = 0, vi = 0; i < 18; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        for (int i = 18, vi = 4; i < 36; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        for (int i = 36, vi = 8; i < 54; i += 6, vi++)
        {
            Triangles[i] = vi + 5;
            Triangles[i + 1] = vi;
            Triangles[i + 2] = vi + 4;
            Triangles[i + 3] = vi + 5;
            Triangles[i + 4] = vi + 1;
            Triangles[i + 5] = vi;
        }
        int uvIndex = 0;
        UVs[uvIndex++] = Vector2.zero;
        UVs[uvIndex++] = new Vector2(uvLeftLengthPercent, 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvRightLengthPercent, 0.5f);
        UVs[uvIndex++] = new Vector2(1, 0.5f);
        UVs[uvIndex++] = new Vector2(0, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(uvLeftLengthPercent, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvRightLengthPercent, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1, uvHeightPercent / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(0, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(uvLeftLengthPercent, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1 - uvRightLengthPercent, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(1, (1 - uvHeightPercent) / 2 + 0.5f);
        UVs[uvIndex++] = new Vector2(0, 1);
        UVs[uvIndex++] = new Vector2(uvLeftLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(1 - uvRightLengthPercent, 1);
        UVs[uvIndex++] = new Vector2(1, 1);
    }

}
