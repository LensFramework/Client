﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class UEMoblieCPUConfig : ScriptableObject
{
    [Serializable]
    public class VendorCPU
    {
        public VendorCPU(int rank, string vendor, string name)
        {
            m_rank = rank;
            m_vendor = vendor;
            m_name = name;
        }
        public int m_rank;
        public string m_vendor;
        public string m_name;
    }

    [Serializable]
    public class Hardware
    {
        public int androidCpuRank;
        public int coreCount;
        public int frequency;
        public int memorySize;
#if UNITY_IOS || UNITY_EDITOR
        public UnityEngine.iOS.DeviceGeneration iosDevice;
#endif
    }

    [Serializable]
    public class HardwareSetting
    {
        public Hardware[] m_Ranks;
        //public Hardware m_VeryHighRank;
        //public Hardware m_HighRank;
        //public Hardware m_MediumRank;
        //public Hardware m_LowRank;
    }

    //[HideInInspector]
    public HardwareSetting m_settings = new HardwareSetting
    {
        m_Ranks = new Hardware[4] {
            new Hardware()
            {
                coreCount = 8,
                frequency = 2800,
                memorySize = 8192,
        #if UNITY_IOS || UNITY_EDITOR
                iosDevice = UnityEngine.iOS.DeviceGeneration.iPhoneXS,
                androidCpuRank = 0,
        #endif
            },
            new Hardware()
            {
                coreCount = 4,                
                frequency = 2000,
                memorySize = 8192,
        #if UNITY_IOS || UNITY_EDITOR
                iosDevice = UnityEngine.iOS.DeviceGeneration.iPhoneX,
                androidCpuRank = 0,
        #endif        
            },
            new Hardware()
            {
                coreCount = 4,                
                frequency = 2000,
                memorySize = 8192,
        #if UNITY_IOS || UNITY_EDITOR
                iosDevice = UnityEngine.iOS.DeviceGeneration.iPhone7Plus,
                androidCpuRank = 0,
        #endif                
            },
            new Hardware()
            {
                coreCount = 4,                
                frequency = 2000,
                memorySize = 8192,
        #if UNITY_IOS || UNITY_EDITOR
                iosDevice = UnityEngine.iOS.DeviceGeneration.iPhone6S,
                androidCpuRank = 0,
        #endif                
            },
        }       
    };

    //[HideInInspector]
    public List<VendorCPU> m_listvendor = new List<VendorCPU>();

    //[HideInInspector]
    public Dictionary<string, List<VendorCPU>> m_dicvendor = new Dictionary<string, List<VendorCPU>>();

    private bool m_init = false;
    public void Init()
    {
        if (m_init) return;

        foreach (var VendorCPU in m_listvendor)
        {
            if (!m_dicvendor.ContainsKey(VendorCPU.m_vendor))
            {
                m_dicvendor[VendorCPU.m_vendor] = new List<VendorCPU>();
            }
            m_dicvendor[VendorCPU.m_vendor].Add(VendorCPU);
        }
        m_init = true;
    }
}
