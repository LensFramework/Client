#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lua.h"

#include "lauxlib.h"

static int ll_unrequire(lua_State *L) {
	const char *name = luaL_checkstring(L, 1);
	lua_settop(L, 1);  /* _LOADED table will be at index 2 */
	const char* key = luaL_gsub(L, name, "/", ".");
	lua_getfield(L, LUA_REGISTRYINDEX, "_LOADED");
	lua_getfield(L, 3, key);
	if (lua_toboolean(L, -1)) {  /* is it there? */
		lua_pushnil(L);
		lua_setfield(L, 3, key);
		lua_settop(L, 0);
		lua_pushboolean(L, 1);
		return 1;  /* package is already loaded */
	}
	lua_settop(L, 0);
	lua_pushboolean(L, 0);
	return 1;  /* package is already loaded */
}
static const luaL_Reg ll_funcs[] = {
	{ "unrequire", ll_unrequire },
	{ NULL, NULL }
};
LUALIB_API int luaopen_package_ext(lua_State *L) {
	lua_pushvalue(L, LUA_GLOBALSINDEX);
	luaL_register(L, NULL, ll_funcs);  /* open lib into global table */
	lua_pop(L, 1);
	return 1;  /* return 'package' table */
}