G_PageBase=require "ui.base.pageBase"
G_StackBase=require "ui.base.stackBase"
G_FixedBase=require "ui.base.fixedBase"
G_ModalBase=require "ui.base.modalBase"
G_UniqueBase=require "ui.base.uniqueBase"
G_GuideBase=require "ui.base.guideBase"
G_MaskBase=require "ui.base.maskBase"

local L_UI={}
local tag="L_UI"
local rootTrans
local uiConfig=require "ui.config"
local pagesDicType={}
local stackIndexes={}
local uniqueIndexes={}
local curUnique=false
local curMask
local auxiliarys={}
local allPages={}

local function newPage(type,pageName,options,callback)
    print(tag,"newPage:"..pageName)
    local promise=Promise.new()
    local layer=uiConfig[type]["layer"]
    local pageConfig=uiConfig[type]["pages"][pageName]
    local asyncHandler=C_Addressables.InstantiateAsync(uiConfig["prefix"]["asset"]..pageConfig.assetName..".prefab")
    asyncHandler.Completed=asyncHandler.Completed+function(o)
        print(tag,"Completed:"..pageName)
        local obj=o.Result
        local page={}
        page["gameObject"]=obj
        obj.transform:SetParent(rootTrans)
        local cls=require(uiConfig["prefix"]["lua"]..pageConfig["luaPath"]).create()
        page["cls"]=cls
        page["config"]=pageConfig
        page["isOpen"]=true
        local canvas = obj:GetComponent(typeof(UnityEngine.Canvas))
        if not canvas  then
            error(tag,"ui root must has Canvas component")
        else
            if pageConfig.layer then
                canvas.sortingOrder=pageConfig.layer
            else
                canvas.sortingOrder=layer
            end
        end
        cls.created(obj,pageConfig)
        cls.open(options)
        allPages[pageName]=page
        promise:resolve(page)
    end
    return promise
end

local function preload()
    local promise=Promise.new()
    local isNull=true
    local count=0
    for k,v in pairs(uiConfig) do
        local pages=v["pages"]
        if pages then
            for pK,pV in pairs(pages) do
                if pV.preload then
                    isNull=false
                    count=count+1
                    local layer=uiConfig[k]["layer"]
                    local pageConfig=uiConfig[k]["pages"][pK]
                    local asyncHandler=C_Addressables.InstantiateAsync(uiConfig["prefix"]["asset"]..pageConfig.assetName..".prefab")
                    asyncHandler.Completed=asyncHandler.Completed+function(o)
                        count=count-1
                        local obj=o.Result
                        local page={}
                        page["gameObject"]=obj
                        obj.transform:SetParent(rootTrans)
                        local cls=require(uiConfig["prefix"]["lua"]..pageConfig["luaPath"]).create()
                        page["cls"]=cls
                        page["config"]=pageConfig
                        page["isOpen"]=false
                        local canvas = obj:GetComponent(typeof(UnityEngine.Canvas))
                        if not canvas  then
                            error(tag,"ui root must has Canvas component")
                        else
                            if pageConfig.layer then
                                canvas.sortingOrder=pageConfig.layer
                            else
                                canvas.sortingOrder=layer
                            end
                        end
                        cls.created(obj,pageConfig)
                        allPages[pK]=page
                        obj:SetActive(false)
                        if count ==0 then
                            promise:resolve()
                        end
                    end
                end
            end
        end
    end
    if isNull then
        promise:resolve()
    end
    return promise
end

local function initAuxiliary()
    for key, value in pairs(uiConfig["auxiliary"]) do
        local aux={}
        aux["config"]=value
        local cls=require(uiConfig["prefix"]["lua"]..value["luaPath"]).create()
        aux["cls"]=cls
        cls.initialize()
        auxiliarys[key]=aux
    end
end

local function openStack(pageName,options)
    local page=allPages[pageName]
    if page then
        if page["isOpen"]==true then
            print(tag,"ui is opened:",pageName)
            return nil
        end
    end
    if #stackIndexes >0 then
        local p=stackIndexes[1]
        local pa=allPages[p]
        pa["isOpen"]=false
        pa["cls"].close()
        pa["gameObject"]:SetActive(false)
        if options and options.isReplace  then
            table.remove(stackIndexes,1)
        end
    end

    if page then
        table.insert(stackIndexes,1,pageName)
        page["gameObject"]:SetActive(true)
        page["cls"].open(options)
        local promise=Promise.new()
        promise:resolve()
        return promise
    else
        table.insert(stackIndexes,1,pageName)
        return newPage("stack",pageName,options)
    end
end

local function openFixed(pageName,options)
    local page=allPages[pageName]
    if page then
        if page["isOpen"]==true then
            print(tag,"ui is opened:",pageName)
            return nil
        end
        page["isOpen"]=true
        page["gameObject"]:SetActive(true)
        page["cls"].open(options)
        local promise=Promise.new()
        promise:resolve()
        return promise
    else
        return newPage("fixed",pageName,options)
    end
end

local function openModal(pageName,options)
    local page=allPages[pageName]
    if page then
        if page["isOpen"]==true then
            print(tag,"ui is opened:",pageName)
            return nil
        end
        page["isOpen"]=true
        page["gameObject"]:SetActive(true)
        page["cls"].open(options)
        local promise=Promise.new()
        promise:resolve()
        return promise
    else
        return newPage("modal",pageName,options)
    end
end

local function openUnique(pageName,options)
    if curUnique then
        table.insert(uniqueIndexes,#uniqueIndexes,{
            pageName=pageName,
            options=options
        })
        local promise=Promise.new()
        promise:resolve()
        return promise
    else
        local page=allPages[pageName]
        if page then
            page["gameObject"]:SetActive(true)
            page["cls"].open(options)
            curUnique=true
            local promise=Promise.new()
            promise:resolve()
            return promise
        else
            curUnique=true
            return newPage("unique",pageName,options)
        end
    end
end

local function openGuide()
    
end

local function openMask(pageName,options)
    if curMask then
        print(tag,"mask is opened")
        return nil
    end
    local page=allPages[pageName]
    if page then
        page["gameObject"]:SetActive(true)
        page["cls"].open(options)
        curMask=pageName
        local promise=Promise.new()
        promise:resolve()
        return promise
    else
        curMask=pageName
        return newPage("mask",pageName,options)
    end
end

local function closeFixed(pageName,options)
    local page=allPages[pageName]
    if page then
        if page["isOpen"]==false then
            print(tag,"ui is dont opened:",pageName)
            return
        end
        page["isOpen"]=false
        page["gameObject"]:SetActive(false)
        page["cls"].close(options)
    end
end

local function closeModal(pageName,options)
    local page=allPages[pageName]
    if page then
        if page["isOpen"]==false then
            print(tag,"ui is dont opened:",pageName)
            return
        end
        page["isOpen"]=false
        page["gameObject"]:SetActive(false)
        page["cls"].close(options)
    end
end

function L_UI.initialize()
    rootTrans=UnityEngine.GameObject.New("UIRoot").transform
    rootTrans.localScale=Vector3.one
    rootTrans.localPosition=Vector3.zero
    rootTrans.localEulerAngles=Vector3.zero
    UnityEngine.Object.DontDestroyOnLoad(rootTrans.gameObject)
    initAuxiliary()
    for key, value in pairs(uiConfig) do
        if value["pages"] then
            for k,v in pairs(value["pages"]) do
                pagesDicType[k]=key
            end
        end
    end
    return preload()
end

function L_UI.open(pageName,options)
    local pageType=pagesDicType[pageName]
    if not pageType then
        print(tag,"dont has this pageName:",pageName)
        return nil
    end
    if pageType=="stack" then
        return openStack(pageName,options)
    elseif pageType=="fixed" then
        return openFixed(pageName,options)
    elseif pageType=="modal" then
        return openModal(pageName,options)
    elseif pageType=="unique" then
        return openUnique(pageName,options)
    elseif pageType=="guide" then
        return nil
    elseif pageType=="mask" then
        return openMask(pageName,options)
    end
end

function L_UI.back(backCount)
    backCount=backCount or 1
    for i = 1, backCount, 1 do
        if #stackIndexes <2 then
            return
        end
        local pageName=stackIndexes[1]
        local page=allPages[pageName]
        page["isOpen"]=false
        page["cls"].close()
        page["gameObject"]:SetActive(false)
        table.remove(stackIndexes,1)

        local newpageName=stackIndexes[1]
        local newpage=allPages[newpageName]
        newpage["isOpen"]=true
        newpage["cls"].open()
        newpage["gameObject"]:SetActive(true)
    end
end

function L_UI.close(pageName,options)
    local pageType=pagesDicType[pageName]
    if not pageType then
        print(tag,"dont has this pageName:",pageName)
        return
    end
    if pageType=="stack" then
        print(tag,"stack ui dont close you must use stackBack()")
    elseif pageType=="fixed" then
        closeFixed(pageName,options)
    elseif pageType=="modal" then
        closeModal(pageName,options)
    elseif pageType=="unique" then
        print(tag,"unique ui dont close you must use closeUnique()")
    elseif pageType=="guide" then

    elseif pageType=="mask" then
        print(tag,"mask ui dont close you must use closeMask()")
    end
end

function L_UI.closeUnique(pageName,options)
    local page=allPages[pageName]
    if page then
        page["gameObject"]:SetActive(false)
        page["cls"].close(options)
    end
    curUnique=false
    if #uniqueIndexes >0 then
        local newPage=uniqueIndexes[1]
        openUnique(newPage["pageName"],newPage["options"])
    end
end

function L_UI.closeMask(options)
    if curMask then
        local page=allPages[curMask]
        if page then
            page["gameObject"]:SetActive(false)
            page["cls"].close(options)
        end
        curMask=nil
    else
        print(tag,"none mask is open")
    end
end

function L_UI.destroy(pageName,options)
    local pageType=pagesDicType[pageName]
    if not pageType then
        print(tag,"dont has this pageName:",pageName)
        return
    end
    local page=allPages[pageName]
    if not page then
        print(tag,"dont has this page:",pageName)
        return
    end
    if pageType=="stack" then
        if stackIndexes[1] == pageName then
            L_UI.stackBack()
        end
    elseif pageType=="unique" then
        if curUnique==true then
            local curPage=uniqueIndexes[1]
            if pageName==curPage["pageName"] then
                L_UI.closeUnique(curPage["pageName"])
            end
        end
    elseif pageType=="guide" then
    elseif pageType=="mask" then
        if curMask==pageName then
            L_UI.closeMask()
        end
    end
    page["cls"].destroy(options)
    UnityEngine.Object.Destroy(page["gameObject"])
    allPages[pageName]=nil
end

return L_UI