﻿Shader "Mobile/Effect/MixFx" {
Properties {
	[HDR]_TintColor("Main Color", Color) = (1,1,1,1)
	_MainTex("MainTex", 2D) = "white" {}
	_MMultiplier("Layer Multiplier", Float) = 1.0
	_Cutoff("Alpha cutoff", Range(0,1)) = 0

	[IntRange]_IsVertical ("IsVertical", Range(0, 1)) = 0
	_ZMove ("ZMove", Float) = 0

	_DistortTex("DistortTex", 2D) = "white" {}
	_HeatTime  ("Heat Time", range (-1,1)) = 0
	_ForceX  ("Strength X", range (0,1)) = 0.1
	_ForceY  ("Strength Y", range (0,1)) = 0.1

	_UVAngle("UV Rotation Angle", Float) = 0
	_RotationSpeed("RotationSpeed", Range(-200, 200)) = 0

	_DetailTex ("2nd layer (RGB)", 2D) = "white" {}
	_ScrollX ("Base layer Scroll speed X", Float) = 1.0
	_ScrollY ("Base layer Scroll speed Y", Float) = 0.0
	_Scroll2X ("2nd layer Scroll speed X", Float) = 1.0
	_Scroll2Y ("2nd layer Scroll speed Y", Float) = 0.0
	_SineAmplX ("Base layer sine amplitude X",Float) = 0.5 
	_SineAmplY ("Base layer sine amplitude Y",Float) = 0.5
	_SineFreqX ("Base layer sine freq X",Float) = 10 
	_SineFreqY ("Base layer sine freq Y",Float) = 10
	_SineAmplX2 ("2nd layer sine amplitude X",Float) = 0.5 
	_SineAmplY2 ("2nd layer sine amplitude Y",Float) = 0.5
	_SineFreqX2 ("2nd layer sine freq X",Float) = 10 
	_SineFreqY2 ("2nd layer sine freq Y",Float) = 10

	_MaskTex("MaskTex", 2D) = "white" {}
	[HDR]_RimColor("RimColor", Color) = (1,1,1,1)
	_RimFalloff("RimFalloff", Range(0, 10)) = 0.0
	_RimPower("RimPower", Range(0, 10)) = 0.0
    _AlphaPower ("Alpha Rim Power", Range(0.0,8.0)) = 4.0

	_DissolveMap("DissolveMap", 2D) = "white"{}
	[HDR]_DissolveColor("Dissolve Color", Color) = (0,0,0,0)
	[HDR]_DissolveEdgeColor("Dissolve Edge Color", Color) = (1,1,1,1)
	_DissolveThreshold("DissolveThreshold", Range(0,1)) = 0
	_DissolveColorFactor("DissolveColorFactor", Range(0,1)) = 0.7
	_DissolveEdgeFactor("DissolveEdgeFactor", Range(0,1)) = 0.8

	_N_mask("N_mask", Float ) = 0.3
    _DissMask ("Diss Mask Texture", 2D) = "white" {}
    [HDR]_C_BYcolor ("C_BYcolor", Color) = (1,0,0,1)
    _N_BY_QD ("N_BY_QD", Float ) = 3
    _N_BY_KD ("N_BY_KD", Float ) = 0.01

	[HideInInspector] _IsTwoSided("_IsTwoSided", Float) = 0.0
	[HideInInspector] _CullMode("_CullingMode", Float) = 0.0
	[HideInInspector] _Mode("__mode", Float) = 0.0
	[HideInInspector] _SrcBlend("__src", Float) = 1.0
	[HideInInspector] _DstBlend("__dst", Float) = 0.0
	[HideInInspector] _ZWrite("__zw", Float) = 1.0
}

Category{

		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		Blend[_SrcBlend][_DstBlend]
		Cull[_CullMode]
		ZWrite[_ZWrite]
		Lighting Off

	SubShader {

		LOD 100

	Pass {

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest
		//#pragma multi_compile_particles

		#pragma shader_feature _BILLBOARD
		#pragma shader_feature _DISTORT
		//#pragma shader_feature _ROTATIONUV
		//#pragma shader_feature _SCROLL
		#pragma shader_feature _RIMLIGHT
		#pragma shader_feature _MASK
		#pragma shader_feature _DISSOLVE
		//#pragma shader_feature _DISSOLUTION

		//#pragma exclude_renderers opengl d3d11_9x xbox360 xboxone ps3 ps4 psp2
		#pragma only_renderers d3d9 d3d11 glcore gles gles3 metal
		#pragma shader_feature _ALPHATEST_ON _ALPHABLEND_ON
		#include "UnityCG.cginc"

		struct appdata_t {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
#if defined (_RIMLIGHT) || defined (_DISTORTION)
			half3 normal : NORMAL;
#endif
			float4 texcoord: TEXCOORD0;
		};

		struct v2f {
			float4 vertex : POSITION;
			fixed4 color : COLOR;
			float4 uvmain : TEXCOORD0;
			float4 uvsec : TEXCOORD1;
			float4 uvthi : TEXCOORD2;

#if defined (_RIMLIGHT) || defined (_DISTORTION)
			half3 normal : TEXCOORD4;
#endif
#if defined (_RIMLIGHT)
			float4 worldPos : TEXCOORD5;
#endif

		};

		fixed4 _TintColor;
		sampler2D _MainTex;
		float4 _MainTex_ST;
		half _MMultiplier;
		half _Cutoff;
		half _SunIntensityScaleEffect;


		
#ifdef _BILLBOARD
		float _IsVertical;
		float _ZMove;
#endif


#ifdef _DISTORT
		sampler2D _DistortTex;
		half _ForceX;
		half _ForceY;
		half _HeatTime;
#endif

#ifdef _ROTATIONUV
		half _UVAngle;
		half _RotationSpeed;
#endif

#ifdef _SCROLL
		sampler2D _DetailTex;
		float4 _DetailTex_ST;

		half _ScrollX;
		half _ScrollY;
		half _Scroll2X;
		half _Scroll2Y;
		half _SineAmplX;
		half _SineAmplY;
		half _SineFreqX;
		half _SineFreqY;

		half _SineAmplX2;
		half _SineAmplY2;
		half _SineFreqX2;
		half _SineFreqY2;
#endif

#ifdef _MASK
		sampler2D _MaskTex;
		float4 _MaskTex_ST;
#endif

#if defined (_DISSOLUTION)
		uniform sampler2D _DissMask; 
		uniform float4 _DissMask_ST;
		uniform float _N_mask;
		uniform float _N_BY_KD;
		uniform fixed4 _C_BYcolor;
		uniform float _N_BY_QD;
#endif

#if defined(_RIMLIGHT)
		uniform fixed4 _RimColor;
		uniform half _RimFalloff;
		uniform half _RimPower;
		half _AlphaPower;
#endif

#if defined(_DISSOLVE)
		uniform fixed4 _DissolveColor;
		uniform fixed4 _DissolveEdgeColor;
		uniform sampler2D _DissolveMap;
		uniform float4 _DissolveMap_ST;
		uniform fixed _DissolveThreshold;
		uniform fixed _DissolveColorFactor;
		uniform fixed _DissolveEdgeFactor;
#endif


		v2f vert(appdata_t v)
		{
			v2f o = (v2f)0;

					
#ifdef _BILLBOARD
			float3 center = float3(0, 0, 0);
			float3 viewer = mul(unity_WorldToObject,float4(_WorldSpaceCameraPos, 1));
			float3 worldNormalDir = viewer - center;
			float3 normalDir = worldNormalDir;
			normalDir.y = normalDir.y * _IsVertical;
			normalDir = normalize(normalDir);
			float3 upDir = abs(normalDir.y) > 0.999 ? float3(0, 0, 1) : float3(0, 1, 0);
			float3 rightDir = normalize(cross(upDir, normalDir));
			upDir = normalize(cross(normalDir, rightDir));

			float3 centerOffs = v.vertex.xyz - center;
			float3 localPos = center + rightDir * centerOffs.x + upDir * centerOffs.y + normalDir * centerOffs.z;

			localPos += normalize(worldNormalDir) * _ZMove;
			
			v.vertex = (float4(localPos, v.vertex.w));
#endif

			o.vertex = UnityObjectToClipPos(v.vertex);
			o.color = v.color * _MMultiplier * _TintColor;

			o.uvmain.xy = v.texcoord.xy;

#ifdef _ROTATIONUV
			float _Rotation = _UVAngle + _RotationSpeed * _Time.y;
			float s = sin(_Rotation);
			float c = cos(_Rotation);
			float2x2 mat = float2x2(c, -s, s, c);
			float offsetX = .5;
			float offsetY = .5;
			float x = o.uvmain.x - offsetX;
			float y = o.uvmain.y - offsetY;
			o.uvmain.xy = mul(float2(x, y), mat) + float2(offsetX, offsetY);
#endif

			o.uvmain.xy = TRANSFORM_TEX(o.uvmain.xy, _MainTex);

#ifdef _SCROLL
			o.uvmain.xy = o.uvmain.xy + frac(float2(_ScrollX, _ScrollY) * _Time.yy);
			o.uvmain.zw = TRANSFORM_TEX(v.texcoord.xy, _DetailTex) + frac(float2(_Scroll2X, _Scroll2Y) * _Time.yy);

			o.uvmain.x += sin(_Time.yy * _SineFreqX) * _SineAmplX;
			o.uvmain.y += sin(_Time.yy * _SineFreqY) * _SineAmplY;

			o.uvmain.z += sin(_Time.yy * _SineFreqX2) * _SineAmplX2;
			o.uvmain.w += sin(_Time.yy * _SineFreqY2) * _SineAmplY2;
#endif

#if defined (_MASK)
			o.uvsec.xy = TRANSFORM_TEX(v.texcoord, _MaskTex);
#endif
#if defined (_DISSOLUTION)
			o.uvsec.zw = TRANSFORM_TEX(v.texcoord, _DissMask);
#endif

#if defined (_DISSOLVE)
			o.uvthi.xy = TRANSFORM_TEX(v.texcoord, _DissolveMap);
#endif
			float4 worldPos = mul(unity_ObjectToWorld, v.vertex);

#if defined (_RIMLIGHT)
			half3 worldNormal = UnityObjectToWorldNormal(v.normal);
			o.worldPos = worldPos;
			o.normal = worldNormal;
#endif

			return o;
		}

		fixed4 frag(v2f i) : COLOR
		{
			fixed4 col = 1;

#ifdef _SCROLL
		col = tex2D(_DetailTex, i.uvmain.zw);
#endif

#ifdef _DISTORT
		fixed4 offsetColor1 = tex2D(_DistortTex, i.uvmain.xy + _Time.xz*_HeatTime);
		fixed4 offsetColor2 = tex2D(_DistortTex, i.uvmain.xy + _Time.yx*_HeatTime);
		i.uvmain.x += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceX;
		i.uvmain.y += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceY;
#endif

		fixed4 col2 = tex2D(_MainTex, i.uvmain.xy);
		col2.rgb *= i.color.rgb;
#if defined(_ALPHATEST_ON) || defined(_ALPHABLEND_ON)
		col2.a *=i.color.a;
		col2.a = saturate(col2.a);
#endif
		col *=col2;
#if defined (_RIMLIGHT)
		half3 viewDirection = normalize(UnityWorldSpaceViewDir(i.worldPos));
		half3 normalection = normalize(i.normal);
		half rim = 1.0 - saturate(dot(viewDirection, normalection));
		col.rgb += _RimColor.rgb * (pow(rim, _RimPower)* _RimFalloff);
		col.a = pow (rim, _AlphaPower) * _RimFalloff;
#endif

#if defined (_MASK)
		fixed4 cMask = tex2D(_MaskTex, i.uvsec.xy);
		col *= cMask;
#endif

#if defined(_ALPHATEST_ON)
		clip(col.a - _Cutoff);
#elif defined(_ALPHABLEND_ON)

#else
		col.a = 1.0;
#endif

#if defined (_DISSOLVE)

#ifdef _DISTORT
		i.uvthi.x += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceX;
		i.uvthi.y += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceY;
#endif

		fixed4 dissolveValue = tex2D(_DissolveMap, i.uvthi.xy);
		fixed factor = sign(dissolveValue.r - _DissolveThreshold);
		col.a = lerp(0, col.a, saturate(factor));

		half percentage = _DissolveThreshold / dissolveValue.r;
		half lerpEdge = sign(percentage - _DissolveColorFactor - _DissolveEdgeFactor);
		fixed3 edgeColor = lerp(_DissolveEdgeColor.rgb, _DissolveColor.rgb, saturate(lerpEdge));
		half lerpOut = sign(percentage - _DissolveColorFactor);

		col.rgb = lerp(col.rgb, edgeColor, saturate(lerpOut));
#endif

#if defined (_DISSOLUTION)
		half v_mask = i.color.a*_N_mask;
		fixed d_mask = tex2D(_DissMask, i.uvsec.zw);
		half leA = step(v_mask, d_mask.r);
		half leB = step(d_mask.r, v_mask);
		half le = lerp(leB, 1, leA*leB);
		half leC = step(v_mask, (d_mask.r + _N_BY_KD));
		half leD = step((d_mask.r + _N_BY_KD), v_mask);
		half le2 = (le - lerp(leD, 1, leC*leD));
		col.rgb = col.rgb + ((le2*_C_BYcolor.rgb)*_N_BY_QD);
		
		col.a = col.a*(le + le2);
#endif
		col.rgb *= (1 - _SunIntensityScaleEffect);

			return col;
		}
		ENDCG
				}
		}
	}

	Fallback Off
	CustomEditor "MixEffectEditor"
}
