﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Lens.UnitTest
{
    public class StreamUnitTest : MonoBehaviour
    {
        FileStream m_stream;
        // Use this for initialization
        void Start()
        {
            m_stream = new FileStream(Path.Combine(Application.dataPath , "../test.txt"), FileMode.Create, FileAccess.Write);
        }

        // Update is called once per frame
        void OnGUI()
        {
            if (GUILayout.Button("Log"))
            {
                for (int i = 0; i < 1000; i++)
                {
                    string str = UnityEngine.Random.Range(1, 100000.0f).ToString() + "\n";
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(str);
                    m_stream.Write(bytes, 0, bytes.Length);
                }
            }
        }
        void OnDestroy()
        {
            m_stream.Close();
        }
    }
}