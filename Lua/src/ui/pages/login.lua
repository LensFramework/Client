local crypt=require "crypt"
local page=class("login",G_StackBase)
local tag="page:login"
local data
local challenge
local subid

local token={
	user="hello",
	server="sample",
	password="password"
}

local function unpack_line(text)
    local from = text:find("\n",1,true)
    if from then
        return text:sub(1,from-1),text:sub(from+1)
    end
    print(tag,text)
    return nil,text
end

local function eToLogin()
    C_TcpManager.SetRawUnpack(true);
    C_TcpManager.Connect("118.190.206.94",8002)
    while true do
        data=C_TcpManager.ReadUnpack()
        if data then
            break;
        end
        coroutine.step()
    end
    challenge=crypt.base64decode(unpack_line(data))
    print("challenge",crypt.hexencode(challenge))
    local clientKey=crypt.randomkey()
    print("clientKey",crypt.hexencode(clientKey))
    local cKey=crypt.dhexchange(clientKey)
    print("cKey",crypt.hexencode(cKey))
    cKey=crypt.base64encode(cKey)
    C_TcpManager.Send(cKey.."\n")
    data=nil
    while true do
        data=C_TcpManager.ReadUnpack()
        if data then
            break;
        end
        coroutine.step()
    end
    local sKey=crypt.base64decode(unpack_line(data))
    print("sKey",crypt.hexencode(sKey))
    
    local secret=crypt.dhsecret(sKey,clientKey)
    print("secret",crypt.hexencode(secret))
    local CHMac = crypt.hmac64(challenge,secret)
    CHMac=crypt.base64encode(CHMac)
    print(tag,CHMac)
    C_TcpManager.Send(CHMac.."\n")

    local user= crypt.base64encode(token.user)
    local server= crypt.base64encode(token.server)
    local password= crypt.base64encode(token.password)
    
    local tokenStr=user .. "@" .. server .. ":" .. password
    local etoken=crypt.desencode(secret,tokenStr)
    etoken=crypt.base64encode(etoken)
    C_TcpManager.Send(etoken.."\n")
    data=nil
    while true do
        data=C_TcpManager.ReadUnpack()
        if data then
            break;
        end
        coroutine.step()
    end
    local response = unpack_line(data)
    local code=response:sub(1,3)
    print("response:",response:sub(1,3))
    assert(code == "200")
    subid=response:sub(5)
    subid=crypt.base64decode(subid)
    print("subId",subid)
    
    C_TcpManager.Disconnect()
end

local function onClickLogin()
    -- coroutine.start(eToLogin)
    L_UI.open("selectserver")
end

function page:ctor(...)

end

function page.created(obj,config)
    print(tag,obj,config)
    local loginBtn=obj.transform:Find("LogonForms/LogonWindow/Btn_LogonSys")
    local btn=loginBtn:GetComponent(typeof(UnityEngine.UI.Button))
    btn.onClick:AddListener(onClickLogin)
end

function page.open(options)
    print(tag,"open")
	C_Config.GetMemConfig():SetSceneType(E_SceneType.Login)
end

function page.close(options)
    
end

function page.destroy(options)
    
end

return page