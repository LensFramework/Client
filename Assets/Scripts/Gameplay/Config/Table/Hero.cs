using System.Collections.Generic;

namespace Lens.Gameplay.Config
{
    [System.Serializable]
    public class THero
    {
        private Hero[] m_array;
        public Hero GetId(int index)
        {
            for (int i = 0; i < m_array.Length; i++)
            {
                if (m_array[i].id == index)
                {
                    return m_array[i];
                }
            }
            return null;
        }
    }
    [System.Serializable]
    public class Hero
    {
        public int id { get; set; }
        public string name { get; set; }
        public int rare { get; set; }
        public int type { get; set; }
        public int defaultStar { get; set; }
        public bool isOpen { get; set; }
        public LitJson.JsonData attributes { get; set; }
    }
}
