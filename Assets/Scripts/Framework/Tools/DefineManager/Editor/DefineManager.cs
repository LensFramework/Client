﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Lens.Framework.Tools
{
    [InitializeOnLoad]
    public static class DefineManager
    {
        [System.Serializable]
        public class ModeData
        {
            public string name;
            public bool enabled;
        }
        public class DefineData
        {
            public ECompileMode actived = ECompileMode.Development;
            public List<ModeData> developments = new List<ModeData>();
            public List<ModeData> debugs = new List<ModeData>();
            public List<ModeData> Releases = new List<ModeData>();
        }
        public enum ECompileMode
        {
            Development,
            Debug,
            Release
        }
        static string m_definePath;
        public static DefineData defineData;
        static DefineManager()
        {
            Initialize();
        }
        static void Initialize()
        {
            m_definePath = Application.dataPath + "/define";
            if (File.Exists(m_definePath))
            {
                var file = File.ReadAllText(m_definePath);
                defineData = JsonUtility.FromJson<DefineData>(file);
            }
            else
            {
                defineData = new DefineData();
            }
        }
        public static void SetDefines(ECompileMode activeCompileMode)
        {
            if (defineData == null)
            {
                Initialize();
            }
            defineData.actived = activeCompileMode;
            using (FileStream fs = new FileStream(m_definePath, FileMode.Create, FileAccess.Write))
            {
                var json = JsonUtility.ToJson(defineData);
                var bytes = System.Text.Encoding.UTF8.GetBytes(json);
                fs.Write(bytes, 0, bytes.Length);
            }
            string symbal = "";
            List<ModeData> lists;
            switch (activeCompileMode)
            {
                case ECompileMode.Development:
                    lists = defineData.developments;
                    break;
                case ECompileMode.Debug:
                    lists = defineData.debugs;
                    break;
                case ECompileMode.Release:
                    lists = defineData.Releases;
                    break;
                default:
                    lists = null;
                    break;
            }
            if (lists!=null)
            {
                for (int i = 0; i < lists.Count; i++)
                {
                    if (!lists[i].enabled) continue;
                    if (string.IsNullOrEmpty(symbal))
                    {
                        symbal = lists[i].name;
                    }
                    else
                    {
                        symbal += ";" + lists[i].name;
                    }
                }
            }
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, symbal);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, symbal);
        }
    }
}