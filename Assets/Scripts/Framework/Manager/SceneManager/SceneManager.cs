﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using System;
    using System.Threading.Tasks;
    using Core;
    public class SceneManager :Singleton<SceneManager>
    {
        private IScene m_scene;
        public async override Task Initialize(Options options)
        {
#if DEVELOPMENT
            m_scene=new LocalScene();
#else
            m_scene = new BundleScene();
#endif
            m_scene.Initialize();
        }
        public void Load()
        {
            m_scene.Load();
        }
        public void UnLoad()
        {
            m_scene.Unload();
        }
        public override void Dispose()
        {
            m_scene.Dispose();
            m_scene = null;
            base.Dispose();
        }
    }
}