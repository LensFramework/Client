namespace Lens.Gameplay.Config
{
    [System.Serializable]
    public class TProp
    {
        private Prop[] m_array;
        public Prop GetId(int index)
        {
            for (int i = 0; i < m_array.Length; i++)
            {
                if (m_array[i].id == index)
                {
                    return m_array[i];
                }
            }
            return null;
        }
    }
    [System.Serializable]
    public class Prop
    {
        public int id { get; set; }
        public int type { get; set; }
        public int subType { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public int quality { get; set; }
        public string icon { get; set; }
        public int sellPrice { get; set; }
    }
}
