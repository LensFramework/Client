﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Lens.Framework.Console
{
    public class LogWindow:EditorWindow
    {
        private Vector2 m_scrollpos;
        private static LogWindow m_instance;
        [MenuItem("Tools/LogWindow")]
        static void Init()
        {
            m_instance = GetWindow<LogWindow>("LogWindow");
            m_instance.Show();
        }
        void OnDestroy()
        {
            m_instance = null;
        }
        void OnGUI()
        {
            if (!EditorApplication.isPlaying)
            {
                EditorGUILayout.LabelField("log window 需要在运行时启动才行....\n请运行游戏,并保证Log已经初始化");
                return;
            }
            else
            {
                if (!Log.isInited)
                {
                    EditorGUILayout.LabelField("log window 请确保Log已经初始化");
                    return;
                }
            }
            string mKey=null;
            Color mValue=Color.white;
            m_scrollpos=EditorGUILayout.BeginScrollView(m_scrollpos);
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.BeginVertical();
                    {
                        foreach (var item in Log.GetLogs())
                        {
                            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(700));
                            {
                                if (string.IsNullOrEmpty(item.Key) || string.IsNullOrEmpty(item.Value))
                                {
                                    continue;
                                }
                                EditorGUILayout.LabelField(item.Key );
                                string r = item.Value.Substring(1, 2);
                                string g = item.Value.Substring(3, 2);
                                string b = item.Value.Substring(5, 2);
                                var logColor= new Color(System.Convert.ToInt32(r, 16) / 255.0f, System.Convert.ToInt32(g, 16) / 255.0f, System.Convert.ToInt32(b, 16) / 255.0f);
                                var color=EditorGUILayout.ColorField(item.Value,logColor, GUILayout.Width(300));
                                if (color !=logColor)
                                {
                                    mKey = item.Key;
                                    mValue = color;
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.LabelField("", EditorStyles.textField, GUILayout.Width(1),GUILayout.Height(Screen.height-30));
                    EditorGUILayout.BeginVertical(GUILayout.MinWidth(300));
                    {
                        string str = "";
                        foreach (var item in Log.GetLogs())
                        {
                            str += "{\"" + item.Key + "\",\"" + item.Value + "\"},\n";
                        }
                        EditorGUILayout.TextArea(str,GUILayout.Height(Screen.height-30));
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndScrollView();
            if (!string.IsNullOrEmpty(mKey))
            {
                Log.GetLogs()[mKey] = Log.ColorTo((int)(mValue.r * 255), (int)(mValue.g * 255), (int)(mValue.b * 255));
            }
        }
    }
}