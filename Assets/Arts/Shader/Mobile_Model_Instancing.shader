﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Mobile/Model/Instancing"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
        _AmbientColor("AmbientColor",Color) = (0.863,0.863,0.863,1)

        _AnimationTex("Animation Texture", 2D) = "white" {}
		_AnimationTexSize("Animation Texture Size", Vector) = (0, 0, 0, 0)

         _MaskColor("_MaskColor" , Color) = (1,1,1,1)

        _SpecPower("Spec Power", Range(0.0, 40)) = 25
        _SpecStrength("Spec Strength", Range(0.0, 2)) = 0.5
        _EffectColor("EffectColor",Color) = (1,1,1,1)
        _LightDir("LightDir", vector) = (0, 1, 0, 1)
        _LightColor("LightColor", Color) = (1,1,1,1)
        _LightIntensity("LightIntensity",Float) = 1.2
        _FresnelExp("FresnelExp", Float) = 1
    }
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

	Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile MASK_COLOR_OFF MASK_COLOR_ON
#pragma multi_compile FUSION_OFF FUSION_ON
#pragma multi_compile_fog
#pragma skip_variants FOG_EXP FOG_EXP2
#include "UnityCG.cginc"
#include "UnityInstancing.cginc"
#pragma target 3.0
#pragma multi_compile_instancing
		UNITY_INSTANCING_BUFFER_START(Props)
		UNITY_DEFINE_INSTANCED_PROP(float, _FrameIndex)
#if FUSION_ON
			UNITY_DEFINE_INSTANCED_PROP(float, _FrameIndexFusion)
			UNITY_DEFINE_INSTANCED_PROP(float, _FusionProgress)
#endif // FUSION_ON
#define _FrameIndex_arr Props
		UNITY_INSTANCING_BUFFER_END(Props)

	struct appdata
	{
		float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
        float3 uv2 : TEXCOORD1;
		half3 normal : NORMAL;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
        UNITY_FOG_COORDS(1)
        fixed3 diff : TEXCOORD2;
        fixed3 spec : TEXCOORD3;
	};

	sampler2D _MainTex;
	float4 _MainTex_ST;

	sampler2D _AnimationTex;
	float4 _AnimationTex_ST;

	float4 _AnimationTexSize;

	float4x4 QuaternionToMatrix(float4 vec)
	{
		float4x4 ret;
		ret._11 = 2.0 * (vec.x * vec.x + vec.w * vec.w) - 1;
		ret._21 = 2.0 * (vec.x * vec.y + vec.z * vec.w);
		ret._31 = 2.0 * (vec.x * vec.z - vec.y * vec.w);
		ret._41 = 0.0;
		ret._12 = 2.0 * (vec.x * vec.y - vec.z * vec.w);
		ret._22 = 2.0 * (vec.y * vec.y + vec.w * vec.w) - 1;
		ret._32 = 2.0 * (vec.y * vec.z + vec.x * vec.w);
		ret._42 = 0.0;
		ret._13 = 2.0 * (vec.x * vec.z + vec.y * vec.w);
		ret._23 = 2.0 * (vec.y * vec.z - vec.x * vec.w);
		ret._33 = 2.0 * (vec.z * vec.z + vec.w * vec.w) - 1;
		ret._43 = 0.0;
		ret._14 = 0.0;
		ret._24 = 0.0;
		ret._34 = 0.0;
		ret._44 = 1.0;
		return ret;
	}

	float4x4 DualQuaternionToMatrix(float4 dual, float4 real)
	{
		float4x4 rotationMatrix = QuaternionToMatrix(float4(dual.x, dual.y,dual.z, dual.w));
		float4x4 translationMatrix;
		translationMatrix._11_12_13_14 = float4(1, 0, 0, 0);
		translationMatrix._21_22_23_24 = float4(0, 1, 0, 0);
		translationMatrix._31_32_33_34 = float4(0, 0, 1, 0);
		translationMatrix._41_42_43_44 = float4(0, 0, 0, 1);
		translationMatrix._14 = real.x;
		translationMatrix._24 = real.y;
		translationMatrix._34 = real.z;
		float4x4 scaleMatrix;
		scaleMatrix._11_12_13_14 = float4(1, 0, 0, 0);
		scaleMatrix._21_22_23_24 = float4(0, 1, 0, 0);
		scaleMatrix._31_32_33_34 = float4(0, 0, 1, 0);
		scaleMatrix._41_42_43_44 = float4(0, 0, 0, 1);
		scaleMatrix._11 = real.w;
		scaleMatrix._22 = real.w;
		scaleMatrix._33 = real.w;
		scaleMatrix._44 = 1;
		float4x4 M = mul(mul(translationMatrix, rotationMatrix), scaleMatrix);
		return M;
	}

    fixed4 _Color;
    fixed4 _EffectColor;

    fixed _SpecPower;
    fixed _SpecStrength;
    uniform float4 _LightDir;
    uniform fixed4 _LightColor;
    fixed4 _AmbientColor;
    half _LightIntensity;
    //half _FresnelExp;

    fixed4 _MaskColor;
    //half _Opacity;

   // half4 _RimColor;
    //half _RimIntensity;

	v2f vert(appdata v)
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
        float4 _uv2 = float4(v.uv2.xyz, 1 - v.uv2.y);
		half frameIndex = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FrameIndex);
		float4x4 matrices0 = DualQuaternionToMatrix(
			tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.x * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
			, tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.x * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
		);
		float4x4 matrices1 = DualQuaternionToMatrix(
			tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.z * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
			, tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.z * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
		);
#if FUSION_ON
		float progress = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FusionProgress);
		float frameIndexFusion = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FrameIndexFusion);
		float4x4 matrices2 = DualQuaternionToMatrix(
			tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.x * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
			, tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.x * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
			);
		float4x4 matrices3 = DualQuaternionToMatrix(
			tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.z * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
			, tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.z * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
			);
		matrices0 = lerp(matrices0, matrices2, progress);
		matrices1 = lerp(matrices1, matrices3, progress);
#endif // FUSION_ON

		float4 pos = mul(matrices0, v.vertex) * _uv2.y + mul(matrices1, v.vertex) * _uv2.w;
		o.vertex = UnityObjectToClipPos(pos);
		o.uv = TRANSFORM_TEX(v.uv, _MainTex);
		half3 posWorld = mul(pos, unity_ObjectToWorld).xyz;
		half3 viewDirection = normalize(UnityWorldSpaceViewDir(posWorld));
		
        float3 normal = mul((float3x3)matrices0, v.normal) * _uv2.y + mul((float3x3)matrices1, v.normal) * _uv2.w;
        half3 N = mul(normal, (float3x3)unity_WorldToObject);
        half3 L = _LightDir.xyz;
        half3 V = viewDirection;
        half nl = max(0, dot(N, normalize(L)));
        half3 diffuseColor = _LightColor.rgb * nl * _LightIntensity + _AmbientColor;
        o.spec = _LightColor.rgb  * pow(nl, _SpecPower) * _SpecStrength * step(0, nl) * nl;
        o.diff = _Color * diffuseColor;
        o.diff *= _EffectColor;

//#if RIM_COLOR_ON
//		half3 normalDir = normalize(N);
//		half fresnel = 1.0 - max(0, dot(normalDir, viewDirection));
//        half fresnelExp = _FresnelExp;
//        half4 rimColor = _RimColor;
//        half rimIntensity = _RimIntensity;
//        half3 fresnelColor = rimColor.rgb * pow(fresnel, fresnelExp) * rimIntensity;
//        o.spec += fresnelColor;
//#endif
        UNITY_TRANSFER_FOG(o, o.vertex);
        return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
        fixed4 col = tex2Dbias(_MainTex, half4(i.uv, 0.0, -3));
#if MASK_COLOR_ON
        half blendTex = col.a;
        col.rgb = col.rgb * (1 - blendTex) + col.rgb * blendTex * _MaskColor.rgb;//lerp(col, col+blendTex*_MaskColor, _Opacity);
#endif
        col.rgb *= i.diff;
        col.rgb += i.spec;
		col.a = 1;
        UNITY_APPLY_FOG(i.fogCoord, col);
        return col;
	}
		ENDCG
	}
	
	}
}
