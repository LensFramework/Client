﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lens.Framework.Managers
{
    public enum ENetPluginType
    {
        Ping,
        Statistical,
        Reconnect,
        Heart,
        Test
    }
}
