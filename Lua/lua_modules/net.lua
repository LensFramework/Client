require "protobuf"
local C_Net=Lens.Framework.Managers.TcpManager
local L_Net={}
local Proto
local listeners={}
local connectStates={}

local ENetTyoe={
    connectSuccess=1,
    connectFailure=2,
    reconnectSuccess=3,
    reconnectFailure=4,
    startReconnect=5
}

function L_Net.initialize( proto )
    Proto=proto
end

function L_Net.sendMessage(cmd,data,callback)
    local bytes,msgId=Proto:encode("cs",cmd,data)
    if callback == nil then
        C_Net.SendMessage(msgId,bytes)
    else
        C_Net.SendMessage(msgId,bytes,callback)
    end
end

function L_Net.addNetStateHandler(callback,single)
    table.insert( connectStates,1,callback);
end

function L_Net.removeNetStateHandler( callback,single )
    for i= 1,#connectStates,1 do
        if connectStates[i]==callback then
            table.remove(connectStates,i)
        end
    end
end

function L_Net.addListener(cmd,callback)
    listeners[cmd]=listeners[cmd] or {}
    table.insert(listeners[cmd],1,callback)
end

function L_Net.removeListener( cmd,callback )
    local list=listeners[cmd]
    if list==nil then
        return;
    end
    for k,v in pairs(list) do
        if v==callback then
            table.remove(list,k)
        end
    end
end

function L_Net.C_OnReceive(data,msgId)
    local cmd=Proto.getSCCmd(msgId)
    if cmd then
        -- print("C_OnReceive msgId:  "..msgId.." cmd: "..cmd)
        local pro=Proto.decode("sc",cmd,data)
        for k,v in pairs(listeners) do
            if k==cmd then
                for i,m in pairs(v) do
                    m(pro)
                end
            end
        end
    else
        error("msgId is dont exists  "..msgId)
    end
end

function L_Net.C_OnConnectSuccess()
    print(connectStates)
    print(#connectStates)
    if #connectStates < 1 then return end
    for i= 1,#connectStates,1 do
        connectStates[i](ENetTyoe.connectSuccess)
    end
end

function L_Net.C_OnConnectFailure()
    if #connectStates < 1 then return end
    for i=1,#connectStates,1 do
        connectStates[i](ENetTyoe.connectFailure)
    end
end


function L_Net.C_OnReconnectSuccess()
    if #connectStates < 1 then return end
    for i= 1,#connectStates,1 do
        connectStates[i](ENetTyoe.reconnectSuccess)
    end
end

function L_Net.C_OnReconnectFailure()
    if #connectStates < 1 then return end
    for i= 1,#connectStates,1 do
        connectStates[i](ENetTyoe.reconnectFailure)
    end
end

function L_Net.C_OnStartReconnect()
    if #connectStates < 1 then return end
    for i= 1,#connectStates,1 do
        connectStates[i](ENetTyoe.reconnectFailure)
    end
end


function L_Net.C_OnDisconnect()
    if #connectStates < 1 then return end
    for i= 1,#connectStates,1 do
        connectStates[i](ENetTyoe.connectSuccess)
    end
end
return L_Net