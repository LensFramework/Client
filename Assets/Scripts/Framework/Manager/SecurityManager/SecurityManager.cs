﻿using System;
using System.Text;
using UnityEngine;

//加解密使用base64转码方法，ECB模式，PKCS5Padding填充，密码必须是16字节
//模式:Java的ECB对应C#的System.Security.Cryptography.CipherMode.ECB
//填充方法:Java的PKCS5Padding对应C#System.Security.Cryptography.PaddingMode.PKCS7
//编码指定采用UTF-8，有需要其他编码方法的可自行扩展

namespace Lens.Framework.Managers
{
    using Core;
    public class SecurityManager:Singleton<SecurityManager>
    {
        private byte[] m_privateKey;
        private System.Security.Cryptography.ICryptoTransform m_ictEncryptor;

        /// <summary>
        /// SetKey
        /// </summary>
        public static void SetKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                Debug.LogError("AES Key Error! ->" + key);
                return;
            }

            byte[] publicKey = null;
            try
            {
                publicKey = Convert.FromBase64String(key);
            }
            catch
            {
                Debug.LogWarning("SetKey publicKey is invalid.");
                return;
            }
            byte[] randomKey = DiffieHellman.GetSharedSecret(p_instance.m_privateKey, publicKey);
            byte[] subKey = new byte[16];
            Array.Copy(randomKey, 0, subKey, 0, 16); //取高16位最为最终秘钥
            p_instance.m_privateKey = null;

            if (p_instance.m_ictEncryptor != null)
            {
                p_instance.m_ictEncryptor.Dispose();
            }

            System.Security.Cryptography.RijndaelManaged rijndaelManaged = new System.Security.Cryptography.RijndaelManaged
            {
                Key = subKey,
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7
            };

            p_instance.m_ictEncryptor = rijndaelManaged.CreateEncryptor();
        }

        /// <summary>
        /// AESEncrypt
        /// </summary>
        public static byte[] AESEncrypt(byte[] data)
        {
            if (data.Length == 0 || p_instance.m_ictEncryptor == null)
            {
                if (Debug.unityLogger.logEnabled)
                {
                    Debug.LogError("Encrypt Parameter Error,data.Length: " + data.Length);
                }
                return null;
            }
            else
            {
                return p_instance.m_ictEncryptor.TransformFinalBlock(data, 0, data.Length);
            }
        }

        /// <summary>
        /// AESEncrypt
        /// </summary>
        public static string AESEncrypt(string data, string key)
        {
            if (string.IsNullOrEmpty(data) || string.IsNullOrEmpty(key))
            {
                return null;
            }

            Byte[] toEncryptArray = Encoding.UTF8.GetBytes(data);
            System.Security.Cryptography.RijndaelManaged rm = new System.Security.Cryptography.RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7
            };

            System.Security.Cryptography.ICryptoTransform cTransform = rm.CreateEncryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// AESDecrypt
        /// </summary>
        public static byte[] AESDecrypt(byte[] data, string key)
        {
            if (data.Length == 0 || string.IsNullOrEmpty(key))
            {
                if (Debug.unityLogger.logEnabled)
                {
                    Debug.LogError("Encrypt Parameter Error");
                }
                return null;
            }

            System.Security.Cryptography.RijndaelManaged rm = new System.Security.Cryptography.RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7
            };

            System.Security.Cryptography.ICryptoTransform cTransform = rm.CreateDecryptor();
            return cTransform.TransformFinalBlock(data, 0, data.Length);
        }

        /// <summary>
        /// AESDecrypt
        /// </summary>
        public static string AESDecrypt(string data, string key)
        {
            if (string.IsNullOrEmpty(data) || string.IsNullOrEmpty(key))
            {
                return null;
            }

            Byte[] toEncryptArray = Convert.FromBase64String(data);
            System.Security.Cryptography.RijndaelManaged rm = new System.Security.Cryptography.RijndaelManaged
            {
                Key = Encoding.UTF8.GetBytes(key),
                Mode = System.Security.Cryptography.CipherMode.ECB,
                Padding = System.Security.Cryptography.PaddingMode.PKCS7
            };

            System.Security.Cryptography.ICryptoTransform cTransform = rm.CreateDecryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Encoding.UTF8.GetString(resultArray);
        }

        /// <summary>
        /// DiffieHellman
        /// </summary>
        public static string GetDiffieHellmanKey()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
            System.Random random = new System.Random(ts.Milliseconds);
            byte[] rawKey = new byte[32];
            for (int i = 0; i < 32; i++)
            {
                rawKey[i] = (byte)random.Next(256);
            }

            p_instance.m_privateKey = DiffieHellman.ClampPrivateKey(rawKey);
            return Convert.ToBase64String(DiffieHellman.GetPublicKey(p_instance.m_privateKey));
        }
    }
}