﻿using ProtoBuf;
using System;
using System.IO;
using System.Net;

namespace Lens.Framework.Managers
{
    using Core;
    using System.Threading.Tasks;

    public class TcpManager:Singleton<TcpManager>
    {
        private INetListener m_listener;
        private TcpSocket m_tcpSocket;
        public async override Task Initialize(Options options=null)
        {
            p_instance.m_tcpSocket = new TcpSocket(m_listener);
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Ping, new PingPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Statistical, new StatisticalPlugin());
            p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Reconnect, new ReconnectPlugin());
            // p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Heart, new HeartPlugin());
            // p_instance.m_tcpSocket.AddPlugin(ENetPluginType.Test, new TestPlugin());
        }
        public override void Dispose()
        {
            Disconnect();
            base.Dispose();
        }
        public override void Update()
        {
            p_instance.m_tcpSocket.Update();
        }
        public void AddListener(INetListener listener)
        {
            m_listener=listener;
        }
        public static void Connect(string ip,int port)
        {
            p_instance.m_tcpSocket.Connect(new IPEndPoint(IPAddress.Parse(ip),port));
        }
        public static void Reconnect()
        {
            p_instance.m_tcpSocket.Reconnect();
        }
        public static void Disconnect()
        {
            p_instance.m_tcpSocket.Disconnect();
        }
        public static void SetRawUnpack(bool rawUnpack)
        {
            p_instance.m_tcpSocket.SetRawUnpack(rawUnpack);
        }
        public static object ReadUnpack(){
            if(p_instance.m_tcpSocket.unPackedBytes.Count>0){
                var bytes=p_instance.m_tcpSocket.unPackedBytes.Dequeue();
                return p_instance.m_listener.WrapUnpack(0,bytes);
            }
            else{
                return null;
            }
        }
        public static void Send(int msgId,byte[] bytes)
        {
            p_instance.m_tcpSocket.Send(msgId, bytes);
        }
        public static void Send(string str)
        {
            p_instance.m_tcpSocket.Send(0,System.Text.Encoding.UTF8.GetBytes(str));
        }
        public static void Send(int msgId, byte[] bytes,Action<object> callback)
        {
            if (callback!=null)
            {
                p_instance.m_tcpSocket.message.AddListener(msgId, callback);
            }
            p_instance.m_tcpSocket.Send(msgId, bytes);
        }
        public static void Send(int msgId,MemoryStream mm)
        {
            byte[] bytes = new byte[mm.Length];
            mm.Position = 0;
            mm.Read(bytes, 0, (int)mm.Length);
            Send(msgId, bytes);
            mm.Close();
            mm.Dispose();
        }
        public static void Send(int msgId, MemoryStream mm, Action<object> callback)
        {
            byte[] bytes = new byte[mm.Length];
            mm.Position = 0;
            mm.Read(bytes, 0, (int)mm.Length);
            Send(msgId, bytes, callback);
            mm.Close();
            mm.Dispose();
        }
        public static void AddListener(EMsgType msgType,Action<object> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.message.AddListener((int)msgType, callback);
        }
        public static void RemoveListener(EMsgType msgType, Action<object> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.message.RemoveListener((int)msgType, callback);
        }
        public static void AddListenAll(Action<int,object> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.message.AddListenAll(callback);
        }
        public static void RemoveListenAll(Action<int, object> callback)
        {
            if (p_instance == null) return;
            p_instance.m_tcpSocket.message.RemoveListenAll(callback);
        }
        public static void AddListener(ENetState netState,Action callback)
        {
            if (p_instance == null) return;
            switch (netState)
            {
                case ENetState.Connected:
                    p_instance.m_tcpSocket.connectedHandler += callback;
                    break;
                case ENetState.Connecting:
                    p_instance.m_tcpSocket.connectingHandler += callback;
                    break;
                case ENetState.Disconnected:
                    p_instance.m_tcpSocket.disconnectedHandler += callback;
                    break;
                case ENetState.ReconnectFailure:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectFailureHandler += callback;
                    break;
                case ENetState.Reconnecting:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectingHandler += callback;
                    break;
                case ENetState.Reconnected:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectedHandler += callback;
                    break;
                default:
                    break;
            }
        }
        public static void RemoveListener(ENetState netState, Action callback)
        {
            if (p_instance == null) return;
            switch (netState)
            {
                case ENetState.Connected:
                    p_instance.m_tcpSocket.connectedHandler -= callback;
                    break;
                case ENetState.Connecting:
                    p_instance.m_tcpSocket.connectingHandler -= callback;
                    break;
                case ENetState.Disconnected:
                    p_instance.m_tcpSocket.disconnectedHandler -= callback;
                    break;
                case ENetState.ReconnectFailure:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectFailureHandler -= callback;
                    break;
                case ENetState.Reconnecting:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectingHandler -= callback;
                    break;
                case ENetState.Reconnected:
                    ((ReconnectPlugin)(p_instance.m_tcpSocket.GetPlugin(ENetPluginType.Reconnect))).reconnectedHandler -= callback;
                    break;
                default:
                    break;
            }
        }
    }
}
