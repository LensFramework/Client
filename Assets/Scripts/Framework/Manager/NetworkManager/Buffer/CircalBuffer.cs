﻿using System;

namespace Lens.Framework.Managers
{
    /// <summary>
    /// This is a circular buffer class for reuse memory
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public class CircleBuffer<T> : ICircleBuffer<T>, IDisposable
    {
        public T[] Buffer
        {
            get;
            private set;
        }
        public int Capacity
        {
            get
            {
                return this.Buffer.Length;
            }
        }
        public int ReadPosition
        {
            get;
            private set;
        }
        public int WritePosition
        {
            get;
            private set;
        }
        public CircleBuffer(int capacity = 1024)
        {
            this.Buffer = new T[capacity];
        }
        public void MoveReadPostion(int length)
        {
            this.ReadPosition += length;
            int num = this.ReadPosition / this.Capacity;
            this.ReadPosition -= num * this.Capacity;
        }
        public void MoveWritePosition(int length)
        {
            this.WritePosition += length;
            int num = this.WritePosition / this.Capacity;
            this.WritePosition -= num * this.Capacity;
        }
        public void MoveReadPositionTo(int index)
        {
            this.ReadPosition = index;
        }
        public void MoveWritePostionTo(int index)
        {
            this.WritePosition = index;
        }
        public T[] Read(int length)
        {
            T[] array = new T[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = this.Buffer[this.ReadPosition];
                this.MoveReadPostion(1);
            }
            return array;
        }
        public void Read(T[] destinationArray, int destinationIndex, int length)
        {
            for (int i = destinationIndex; i < length; i++)
            {
                destinationArray[i] = this.Buffer[this.ReadPosition];
                this.MoveReadPostion(1);
            }
        }
        public void Write(T[] sourceArray)
        {
            for (int i = 0; i < sourceArray.Length; i++)
            {
                this.Buffer[this.WritePosition] = sourceArray[i];
                this.MoveWritePosition(1);
            }
        }
        public void Write(T[] sourceArray, int sourceIndex, int length)
        {
            for (int i = sourceIndex; i < length; i++)
            {
                this.Buffer[this.WritePosition] = sourceArray[i];
                this.MoveWritePosition(1);
            }
        }
        public void Dispose()
        {
            this.Buffer = null;
        }
    }
}
