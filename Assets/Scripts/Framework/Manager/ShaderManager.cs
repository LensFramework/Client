﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using Console;
    using Core;
    public class ShaderManager:Singleton<ShaderManager>
    {
        private Dictionary<string, Shader> m_dic = new Dictionary<string, Shader>();
        public static void Store(Shader[] shaders)
        {
#if RELEASE
            for (int i = 0; i < shaders.Length; i++)
            {
                p_instance.m_dic.Add(shaders[i].name, shaders[i]);
            }
#endif
        }
        public static Shader Get(string name)
        {
#if RELEASE
            if (p_instance.m_dic.ContainsKey(name))
            {
                return p_instance.m_dic[name];
            }
            else
            {
                Log.Info( "dont has this shader: " + name);
            }
            return p_instance.m_dic[name];
#else
            return Shader.Find(name);
#endif
        }
    }
}
