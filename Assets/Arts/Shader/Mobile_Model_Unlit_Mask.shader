// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Unlit shader. Simplest possible textured shader.
// - no lighting
// - no lightmap support
// - no per-material color

Shader "Mobile/Model/TextureMask" {
Properties {
	_MainTex ("MainTex", 2D) = "white" {}
	_Color("Color",Color) = (1,1,1,1)
	_AmbientColor ("AmbientColor",Color) = (0.863,0.863,0.863,1)
 
	// _MaskTex("MaskTex",2D) = "black"{} 
    [Toggle(MASK_COLOR_ON)] _bMaskColor("_MaskColor_On",Float) = 0
    _MaskColor("_MaskColor" , Color) = (1,1,1,1)
    _Opacity("_Opacity", Float) = 1.0

    _SpecPower("Spec Power", Range(0.0, 40))= 24 
	_SpecStrength("Spec Strength", Range(0.0, 2))= 0.5
	_EffectColor("EffectColor",Color) = (1,1,1,1)
	_LightDir ("LightDir", vector) = (-400, 1000, 200, 1)
	_LightColor ("LightColor", Color) = (0.64,0.64,0.64,1)
	 _LightIntensity ("LightIntensity",Float) = 1.2
	_RimColor ("RimColor", Color) = (1,0.4632353,0.4632353,1)
	_FresnelExp ("FresnelExp", Float ) = 1
	_RimIntensity ("RimIntensity", Float ) = 0
}
 
SubShader {
	//pass to render object
	  
    LOD 100
		Pass {  
		Tags { "RenderType"="Opaque"} 
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#pragma multi_compile_fog
			#pragma skip_variants FOG_EXP FOG_EXP2
			//#include "Lighting.cginc"
            #pragma multi_compile MASK_COLOR_OFF MASK_COLOR_ON

			struct appdata_t {
				half4 vertex : POSITION;
				half2 texcoord : TEXCOORD0;
				half3 normal:NORMAL;
			};

			struct v2f {
				half4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				fixed3 diff: TEXCOORD2;  
				fixed3 spec: TEXCOORD3;  
			};

			sampler2D _MainTex;
			half4 _MainTex_ST;
				fixed4 _AmbientColor;
			// sampler2D _MaskTex;
			fixed4 _MaskColor;
			fixed4 _Color;
			fixed4 _EffectColor;
			fixed _Opacity;
			fixed _SpecPower;
			fixed _LightPower;
			fixed _SpecStrength;
			uniform float4 _LightDir;
			half _LightIntensity;
			uniform fixed4 _LightColor; 

			half _FresnelExp;
            half4 _RimColor;
            half _RimIntensity;
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				half3 N = UnityObjectToWorldNormal(v.normal);  
				half3 L = _LightDir.xyz;
				half3 V = WorldSpaceViewDir(v.vertex);
				half nl = max(0,dot(N, normalize(L))) ;
				half3 diffuseColor = _LightColor.rgb * nl * _LightIntensity + _AmbientColor;
				o.spec = _LightColor.rgb  * pow(nl, _SpecPower) * _SpecStrength * step(0, nl) * nl;
				o.diff = _Color * (diffuseColor );
				o.diff *= _EffectColor;

				half3 posWorld = mul(unity_ObjectToWorld, v.vertex).xyz;
				half3 viewDirection = normalize(UnityWorldSpaceViewDir(posWorld));
				half3 normalDir = normalize(N);
				half fresnel = 1.0-max(0,dot(normalDir, viewDirection));
				half3 fresnelColor = _RimColor.rgb * pow(fresnel,_FresnelExp) *_RimIntensity ;
				o.spec = o.spec + fresnelColor;

				UNITY_TRANSFER_FOG(o, o.vertex); 
				return o;
			}

			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2Dbias(_MainTex,  half4(i.texcoord, 0.0, -3) ); 

				// fixed4 blendTex = tex2D(_MaskTex, i.texcoord);
#if MASK_COLOR_ON
                half blendTex = col.a;
                col = col* (1- blendTex) +   col * blendTex*fixed4(_MaskColor.rgb,1)*_Opacity;//lerp(col, col+blendTex*_MaskColor, _Opacity);
#endif
                col.rgb *= i.diff;
				col.rgb += i.spec;
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}

	  
	}

}
