﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Security.Cryptography;
using System;

namespace LuaReload
{
    [InitializeOnLoad]
    public static class LuaReload
    {
        private static List<string> modifys = new List<string>();
        private static LuaReloadConfig m_config;
        private static double m_time;
        private static double m_interval =2;
        private static IInternal m_internal;
        private static bool m_injected;
        private static AutoReload m_autoReload;
        private static FileSystemWatcher watcher;
        private static string m_luaFolder;
        [MenuItem("Tools/LuaToReload %q")]
        static void ToolsToReload()
        {
            if (!EditorApplication.isPlaying)
            {
                EditorUtility.DisplayDialog("Infomation", "luaReload must start game", "Ok");
            }
            else
            {
                ToReload();
            }
        }
        static LuaReload()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }
        private static void OnPlayModeStateChanged(PlayModeStateChange obj)
        {
            if (obj == PlayModeStateChange.EnteredPlayMode)
            {
                m_injected = false;
                var assemblys = AppDomain.CurrentDomain.GetAssemblies();
                for (int i = 0; i < assemblys.Length; i++)
                {
                    var assembly = assemblys[i];
                    if (assembly.GetName().Name == "Assembly-CSharp")
                    {
                        var types = assembly.GetTypes();
                        for (int j = 0; j < types.Length; j++)
                        {
                            var type = types[j];
                            var s = type.GetInterface("LuaReload.IInternal");
                            if (s != null)
                            {
                                m_internal = (IInternal)Activator.CreateInstance(type);
                            }
                        }
                    }
                }

                m_autoReload = new GameObject().AddComponent<AutoReload>();
                m_autoReload.gameObject.hideFlags = HideFlags.HideInHierarchy;
                UnityEngine.Object.DontDestroyOnLoad(m_autoReload.gameObject);

                var files = Directory.GetFiles(Application.dataPath, "LuaReloadSettings.asset", SearchOption.AllDirectories);
                if (files.Length <= 0)
                {
                    UnityEngine.Debug.LogError("LuaReloadSettings.asset is not exsts");
                }
                else
                {
                    string file = files[0];
                    file = file.Replace("\\", "/");
                    string configPath = "Assets" + file.Replace(Application.dataPath, "");
                    m_config = (LuaReloadConfig)AssetDatabase.LoadAssetAtPath(configPath, typeof(LuaReloadConfig));
                    m_luaFolder = Path.Combine(Application.dataPath, m_config.luaFolder).Replace("\\","/");
                    GetWatchers();
                }
                if (m_config.autoReload)
                {
                    m_autoReload.StartCoroutine(EToReload());
                }
            }
            else if (obj == PlayModeStateChange.ExitingPlayMode)
            {
                UnityEngine.Object.Destroy(m_autoReload.gameObject);
                m_autoReload = null;
                if (watcher!=null)
                {
                    watcher.Dispose();
                }
                EditorUtility.ClearProgressBar();
            }
        }
        private static void GetWatchers()
        {
            if (watcher!=null)
            {
                watcher.Dispose();
                watcher = null;
            }
            watcher = new FileSystemWatcher();
            try
            {
                watcher.Path = m_luaFolder;
            }
            catch (ArgumentException e)
            {
                UnityEngine.Debug.LogError(e);
                return;
            }

            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.IncludeSubdirectories = true;
            watcher.Filter = "*.lua";

            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }
        static void ToReload()
        {
            if (m_internal != null)
            {
                if (m_internal.IsLuaStarted())
                {
                    if (!m_injected)
                    {
                        m_internal.Inject();
                        UnityEngine.Debug.Log("Lua Reload Init Success");
                        m_injected = true;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                //UnityEngine.Debug.LogError("Lua IInternal is Dont exists");
                return;
            }
            if (modifys.Count>0)
            {
                EditorUtility.DisplayProgressBar("Lua Reloading", "LuaReloading......", 0.5f);
                if (m_internal != null)
                {
                    try
                    {
                        m_internal.ToReload("reload", "Reload",m_luaFolder, modifys,m_config.luaVersion);
                    }
                    catch(Exception e)
                    {
                        UnityEngine.Debug.LogError(e);
                    }
                }
                modifys.Clear();
                EditorUtility.ClearProgressBar();
            }
        }
        static IEnumerator EToReload()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                ToReload();
            }
        }
        static void OnChanged(object sender,FileSystemEventArgs args)
        {
            string luaFolder = m_luaFolder.Replace("\\", "/").Replace("Assets/../","");
            var item = args.FullPath;
            var s = item.Replace("\\", "/").Replace(luaFolder + "/", "");
            s = s.Replace(m_config.extension, "");
            UnityEngine.Debug.Log(s);
            modifys.Add(s);
        }
    }
}