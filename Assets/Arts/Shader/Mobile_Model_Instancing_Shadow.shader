﻿Shader "Mobile/Model/InstancingShadow"
{
	Properties
	{
		_AnimationTex("Animation Texture", 2D) = "white" {}
		_AnimationTexSize("Animation Texture Size", Vector) = (0, 0, 0, 0)
		_ShadowOffset("Shadow Offset", Vector) = (0.91, 0.51, 0, 0)
		_ShadowHeight("Shadow Height", float) = 0.1
	}
	SubShader
	{
		Tags{
			"Queue" = "Geometry"
			"RenderType" = "Opaque" }
		LOD 100
		Pass
		{
			Blend One OneMinusSrcAlpha
			Stencil
			{
				Ref 2
				Comp NotEqual
				Pass Replace
			}
			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag

	#include "UnityCG.cginc"
	#include "UnityInstancing.cginc"
	#pragma target 3.0
	#pragma multi_compile FUSION_OFF FUSION_ON
	#pragma multi_compile_instancing
			UNITY_INSTANCING_BUFFER_START(Props)
			UNITY_DEFINE_INSTANCED_PROP(float, _FrameIndex)
#if FUSION_ON
			UNITY_DEFINE_INSTANCED_PROP(float, _FrameIndexFusion)
			UNITY_DEFINE_INSTANCED_PROP(float, _FusionProgress)
#endif // FUSION_ON
	#define _FrameIndex_arr Props
			UNITY_INSTANCING_BUFFER_END(Props)

			struct appdata
			{
				float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 uv2 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};


			sampler2D _AnimationTex;
			float4 _AnimationTex_ST;

			float4 _AnimationTexSize;

			float4 _ShadowOffset;
			float _ShadowHeight;
			uniform fixed4 _ShadowColor;

			float4x4 QuaternionToMatrix(float4 vec)
			{
				float4x4 ret;
				ret._11 = 2.0 * (vec.x * vec.x + vec.w * vec.w) - 1;
				ret._21 = 2.0 * (vec.x * vec.y + vec.z * vec.w);
				ret._31 = 2.0 * (vec.x * vec.z - vec.y * vec.w);
				ret._41 = 0.0;
				ret._12 = 2.0 * (vec.x * vec.y - vec.z * vec.w);
				ret._22 = 2.0 * (vec.y * vec.y + vec.w * vec.w) - 1;
				ret._32 = 2.0 * (vec.y * vec.z + vec.x * vec.w);
				ret._42 = 0.0;
				ret._13 = 2.0 * (vec.x * vec.z + vec.y * vec.w);
				ret._23 = 2.0 * (vec.y * vec.z - vec.x * vec.w);
				ret._33 = 2.0 * (vec.z * vec.z + vec.w * vec.w) - 1;
				ret._43 = 0.0;
				ret._14 = 0.0;
				ret._24 = 0.0;
				ret._34 = 0.0;
				ret._44 = 1.0;
				return ret;
			}

			float4x4 DualQuaternionToMatrix(float4 m_dual, float4 m_real)
			{
				float4x4 rotationMatrix = QuaternionToMatrix(float4(m_dual.x, m_dual.y, m_dual.z, m_dual.w));
				float4x4 translationMatrix;
				translationMatrix._11_12_13_14 = float4(1, 0, 0, 0);
				translationMatrix._21_22_23_24 = float4(0, 1, 0, 0);
				translationMatrix._31_32_33_34 = float4(0, 0, 1, 0);
				translationMatrix._41_42_43_44 = float4(0, 0, 0, 1);
				translationMatrix._14 = m_real.x;
				translationMatrix._24 = m_real.y;
				translationMatrix._34 = m_real.z;
				float4x4 scaleMatrix;
				scaleMatrix._11_12_13_14 = float4(1, 0, 0, 0);
				scaleMatrix._21_22_23_24 = float4(0, 1, 0, 0);
				scaleMatrix._31_32_33_34 = float4(0, 0, 1, 0);
				scaleMatrix._41_42_43_44 = float4(0, 0, 0, 1);
				scaleMatrix._11 = m_real.w;
				scaleMatrix._22 = m_real.w;
				scaleMatrix._33 = m_real.w;
				scaleMatrix._44 = 1;
				float4x4 M = mul(mul(translationMatrix, rotationMatrix), scaleMatrix);
				return M;
			}

			v2f vert(appdata v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				half frameIndex = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FrameIndex);
                float4 _uv2 = float4(v.uv2.xyz, 1 - v.uv2.y);
                float4x4 matrices0 = DualQuaternionToMatrix(
					tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.x * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
					, tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.x * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
				);
				float4x4 matrices1 = DualQuaternionToMatrix(
					tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.z * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
					, tex2Dlod(_AnimationTex, float4(frameIndex, _uv2.z * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
				);

#if FUSION_ON
				float progress = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FusionProgress);
				float frameIndexFusion = UNITY_ACCESS_INSTANCED_PROP(_FrameIndex_arr, _FrameIndexFusion);
				float4x4 matrices2 = DualQuaternionToMatrix(
					tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.x * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
					, tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.x * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
				);
				float4x4 matrices3 = DualQuaternionToMatrix(
					tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.z * 2.0 / _AnimationTexSize.y + 0.5 / _AnimationTexSize.y, 0, 0))
					, tex2Dlod(_AnimationTex, float4(frameIndexFusion, _uv2.z * 2.0 / _AnimationTexSize.y + 1.5 / _AnimationTexSize.y, 0, 0))
				);
				matrices0 = lerp(matrices0, matrices2, progress);
				matrices1 = lerp(matrices1, matrices3, progress);
#endif // FUSION_ON
				float4 pos = mul(matrices0, v.vertex) * _uv2.y + mul(matrices1, v.vertex) * _uv2.w;

				pos = mul(unity_ObjectToWorld, pos);

				pos.x += _ShadowOffset.x * pos.y + _ShadowOffset.z;
				pos.z += _ShadowOffset.y * pos.y + _ShadowOffset.w;
				pos.y = _ShadowHeight;
				
				o.vertex = mul(UNITY_MATRIX_VP, pos);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				return _ShadowColor;
			}
			ENDCG
		}
	}
}
