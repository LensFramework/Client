﻿/***************************************************************
 * Description: 
 *
 * Documents: https://github.com/hiramtan/HiSocket
 * Author: hiramtan@live.com
***************************************************************/

namespace Lens.Framework.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class StatisticalPlugin : NetPluginBase
    {
        /// <summary>
        /// How many bytes total send
        /// </summary>
        public long HowManyBytesSend;
        public override void Initialize()
        {
            base.Initialize();
            TcpSocket.sendedHandler += OnSended;
        }
        void OnSended(byte[] bytes)
        {
            HowManyBytesSend += bytes.Length;
        }
    }
}