﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lens.Framework.Managers
{
    using Core;
    using System.Threading.Tasks;

    public class TimeManager:Singleton<TimeManager>
    {
        public static int deltaTime { get; private set; }
        public static float fDeltaTime { get;private set; }
        public static int time { get; private set; }
        public async override Task Initialize(Options options){
            SetDeltaTime(30);
        }
        public override void Dispose()
        {
            time = 0;
            deltaTime = 0;
            base.Dispose();
        }
        public override void Update()
        {
            time += deltaTime;
        }
        public static void ForceTime(int t)
        {
            time = t;
        }
        public void SetDeltaTime(int time)
        {
            deltaTime = time;
            fDeltaTime = time / 1000.0f;
        }
    }
}
