﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Libs
{
    [ExecuteInEditMode]
    public class InstancingComponent : MonoBehaviour
    {
        #region public member
        public Material material;
        public Material shadowMaterial;
        public InstancingData animationInfo;
        public bool useApiDraw;
        public bool useShadow;
        public List<Mesh> meshLods;
        public int spawnId;
        public bool shownShadow;
        public float fusionTime = 0.20f;
        public float progress = 0;
        public float frameIndex;
        public float frameIndexFusion;
        public MeshFilter filter;
        public MeshRenderer render;

        #endregion

        #region private member

        private float m_timeScale = 1.0f;
        private InstancingAnimation m_sourceAnimation = null;
        private InstancingAnimation m_destinationAnimation = null;
        private float m_frameStartIndex = 0;
        private float m_frameEndIndex = 0;
        private float m_currentTime = 0;

        private bool m_isFusioning;
        private float m_frameStartIndexFusion = 0;
        private float m_frameEndIndexFusion = 0;
        private float m_fusionCurrentTime = 0;
        private int m_instancingId=-1;
        private bool m_initial;
        #endregion

        #region Unity
        void Awake()
        {
            if (Application.isPlaying)
            {
                if (!useApiDraw)
                {
                    render = GetComponent<MeshRenderer>();
                    filter = GetComponent<MeshFilter>();
                    if (render == null)
                    {
                        render = gameObject.AddComponent<MeshRenderer>();
                    }
                    if (filter == null)
                    {
                        filter = gameObject.AddComponent<MeshFilter>();
                    }
                }
            }
        }
#if UNITY_EDITOR
        void Update()
        {
            if (m_initial) return;
            if (render == null)
            {
                render = GetComponent<MeshRenderer>();
                if (render == null)
                {
                    render = gameObject.AddComponent<MeshRenderer>();
                }
            }
            if (filter == null)
            {
                filter = GetComponent<MeshFilter>();
                if (filter == null)
                {
                    filter = gameObject.AddComponent<MeshFilter>();
                }
            }
            if (Application.isPlaying)
            {
                if (useShadow)
                {
                    if (render.sharedMaterials == null || render.sharedMaterials.Length != 2)
                    {
                        render.sharedMaterials = new Material[2] { material, shadowMaterial };
                        var m_tex2D = animationInfo.GetTexture();
                        render.materials[0].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                        render.materials[0].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                        render.materials[1].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                        render.materials[1].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    }
                }
                else
                {
                    if (render.sharedMaterials == null || render.sharedMaterials.Length != 1 || render.sharedMaterials[0] == null)
                    {
                        render.sharedMaterials = new Material[] { material };
                        var m_tex2D = animationInfo.GetTexture();
                        render.materials[0].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                        render.materials[0].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    }
                }
                if (m_sourceAnimation == null || m_sourceAnimation.frames.Length == 0)
                {
                    var m_tex2D = animationInfo.GetTexture();
                    render.materials[0].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                    render.materials[0].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    if (useShadow)
                    {
                        render.materials[1].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                        render.materials[1].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    }
                    Play(2);
                }
            }
            else
            {
                if (useShadow)
                {
                    if (render.sharedMaterials == null || render.sharedMaterials.Length != 2)
                    {
                        render.sharedMaterials = new Material[2] { material, shadowMaterial };
                    }
                }
                else
                {
                    if (render.sharedMaterials == null || render.sharedMaterials.Length != 1 || render.sharedMaterials[0] == null)
                    {
                        render.sharedMaterials = new Material[] { material };
                    }
                }
                if (m_sourceAnimation == null || m_sourceAnimation.frames.Length == 0)
                {
                    var m_tex2D = animationInfo.GetTexture();
                    render.sharedMaterials[0].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                    render.sharedMaterials[0].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    if (useShadow)
                    {
                        render.sharedMaterials[1].SetTexture(Core.Uniforms._AnimationTex, m_tex2D);
                        render.sharedMaterials[1].SetVector(Core.Uniforms._AnimationTexSize, new Vector4(m_tex2D.width, m_tex2D.height, 0, 0));
                    }
                    Play(2);
                }
            }

            float dTime = Time.deltaTime;

            Mesh lodMesh = meshLods[0];
            ToUpdate(dTime);
            MaterialPropertyBlock properties = new MaterialPropertyBlock();
            properties.SetFloat(Core.Uniforms._FrameIndex, frameIndex);
            properties.SetFloat(Core.Uniforms._FrameIndexFusion, frameIndexFusion);
            properties.SetFloat(Core.Uniforms._FusionProgress, progress);

            filter.sharedMesh = lodMesh;
            render.SetPropertyBlock(properties);
        }
        void OnDestroy()
        {
            if (m_initial) return;
            animationInfo.Dispose();
        }
#endif
        #endregion

        #region public methods
        public void Initialize(int configId,Color campColor)
        {
            m_initial = true;
            shownShadow = useShadow;
            m_instancingId = InstancingCluster.AddInstancingConfig(configId, animationInfo, material, shadowMaterial, campColor,true,true);
            spawnId = InstancingCluster.AddSpawnObject(m_instancingId, this);
            Play(2);
        }
        public void ShowShadow()
        {
            if (!shownShadow)
            {
                shownShadow = true;
            }
        }
        public void HideShadow()
        {
            if (shownShadow)
            {
                shownShadow = false;
            }
        }
        public void Play(int index)
        {
            m_currentTime = 0;
            m_sourceAnimation = animationInfo.boneAnimations[index];
            m_frameStartIndex = (m_sourceAnimation.frameStartIndex + 0.5f) / animationInfo.totalAnimationLength;
            m_frameEndIndex = (float)(m_sourceAnimation.frameStartIndex + m_sourceAnimation.framesCount - 1) / animationInfo.totalAnimationLength;
        }
        public void PlayFusion(int index)
        {
            m_isFusioning = true;
            m_fusionCurrentTime = 0;
            m_destinationAnimation = animationInfo.boneAnimations[index]; 
            m_frameStartIndexFusion = (m_destinationAnimation.frameStartIndex + 0.5f) / animationInfo.totalAnimationLength;
            m_frameEndIndexFusion = (float)(m_destinationAnimation.frameStartIndex + m_destinationAnimation.framesCount - 1) / animationInfo.totalAnimationLength;
        }
        public void ToUpdate(float deltaTime)
        {
            if (m_isFusioning)
            {
                m_fusionCurrentTime += deltaTime * m_timeScale;
                var normalizedTime = m_fusionCurrentTime / m_destinationAnimation.length;
                if (normalizedTime > 1)
                {
                    normalizedTime = 1;
                }
                progress = m_fusionCurrentTime / fusionTime;
                frameIndexFusion = m_frameStartIndexFusion + normalizedTime * (m_frameEndIndexFusion - m_frameStartIndexFusion);
                if (progress >= 1)
                {
                    m_sourceAnimation = m_destinationAnimation;
                    m_frameStartIndex = (m_sourceAnimation.frameStartIndex + 0.5f) / animationInfo.totalAnimationLength;
                    m_frameEndIndex = (float)(m_sourceAnimation.frameStartIndex + m_sourceAnimation.framesCount - 1) / animationInfo.totalAnimationLength;
                    m_currentTime = m_fusionCurrentTime;
                    progress = 0;
                    frameIndex = frameIndexFusion;
                    m_isFusioning = false;
                }
            }
            else
            {
                m_currentTime += deltaTime * m_timeScale;
                var normalizedTime = m_currentTime / m_sourceAnimation.length;// ( - Mathf.Floor(m_currentTime / m_sourceAnimation.length));
                if (normalizedTime > 1)
                {
                    normalizedTime = 1;
                }
                frameIndex = m_frameStartIndex + normalizedTime * (m_frameEndIndex - m_frameStartIndex);
                if (normalizedTime == 1 && m_sourceAnimation.isLooping)
                {
                    m_currentTime = 0;
                }
            }
        }
        #endregion

        #region private methods
        #endregion
    }
}