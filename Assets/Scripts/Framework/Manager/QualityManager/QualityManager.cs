﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using Core;
    using System.Threading.Tasks;

    public class QualityManager:Singleton<QualityManager>
    {
        public static UEDevicesGrading.DeviceGrade grade
        {
            get { return p_instance.m_grade; }
            set
            {
                p_instance.SetGrade(value);
            }
        }

        public delegate UnityEngine.Object LoadAsset(string path);
        public LoadAsset loadAssetHandler;

        UEDevicesGrading.DeviceGrade m_grade = UEDevicesGrading.DeviceGrade.LOW;
        UEMoblieCPUConfig m_config;
        public async override Task Initialize(Options options = null)
        {
            var o=loadAssetHandler("config/devicegradingconfig");
            m_config = o as UEMoblieCPUConfig;
            if (m_config == null)
            {
                Debug.LogWarning("config/devicegradingconfig not found");
                return;
            }

            UEDevicesGrading grading = new UEDevicesGrading();
            grading.Init(m_config, OnGradeChange, Debug.Log, true);
        }

        void OnGradeChange(UEDevicesGrading.DeviceGrade grade)
        {
            SetGrade(grade);
        }

        void SetGrade(UEDevicesGrading.DeviceGrade grade)
        {
            m_grade = grade;
            PlayerPrefs.SetInt(UEDevicesGrading.KEY, (int)p_instance.m_grade);
            

            int lvl = 0;
            switch (grade)
            {
                case UEDevicesGrading.DeviceGrade.LOW:
                    lvl = 1;
                    break;
                case UEDevicesGrading.DeviceGrade.MEDIUM:
                    lvl = 2;
                    break;
                case UEDevicesGrading.DeviceGrade.HIGH:
                    lvl = 3;
                    break;
                case UEDevicesGrading.DeviceGrade.VERY_HIGH:
                    lvl = 4;
                    break;
            }
            UnityEngine.QualitySettings.SetQualityLevel(lvl);
        }

        public override void Update()
        {
            
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}