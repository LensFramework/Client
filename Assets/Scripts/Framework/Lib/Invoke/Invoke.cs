﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{
    public class Invoke
    {
        private class InvokePool
        {
            public enum Status
            {
                start = 0,
                delay = 1,
                interval = 2,
                end = 3,
            }
            public Status status
            {
                get
                {
                    return m_status;
                }
            }
            public Action m_invoke1 { get; private set; }
            public Action<object> m_invoke2 { get; private set; }
            private object m_params;
            public Action<object> m_endInvoke { get; private set; }
            private object m_endparams;
            private int m_count;
            private int m_delay;
            private int m_interval;
            private int m_delayTime;
            private int m_intervalTime;
            private Status m_status;
            public int m_invokeIndex;
            public void Create(Action callback, int delay, int invokeIndex, int interval = 0, int count = 0)
            {
                this.m_invoke1 = callback;
                Create(delay, invokeIndex, interval, count);
            }
            public void Create(Action<object> callback, object param, int delay, int invokeIndex, int interval = 0, int count = 0)
            {
                this.m_invoke2 = callback;
                this.m_params = param;
                Create(delay, invokeIndex, interval, count);
            }

            public void Create(Action<object> callback, object param, int delay, int invokeIndex, int interval, int count, Action<object> endcallback, object param1)
            {
                this.m_invoke2 = callback;
                this.m_params = param;
                this.m_endInvoke = endcallback;
                this.m_endparams = param1;
                Create(delay, invokeIndex, interval, count);
            }

            private void Create(int delay, int invokeIndex, int interval, int count)
            {
                this.m_delay = delay;
                this.m_invokeIndex = invokeIndex;
                this.m_interval = interval;
                this.m_count = count;
                this.m_delayTime = (int)(UnityEngine.Time.time*1000);
                this.m_status = Status.start;
            }

            private void RunAction()
            {
                if (m_invoke1 != null)
                {
                    m_invoke1();
                }
                if (m_invoke2 != null)
                {
                    m_invoke2(m_params);
                }
                m_intervalTime = (int)(UnityEngine.Time.time * 1000);
            }

            public void Update()
            {
                if (this.m_status == Status.end) return;
                if (this.m_status==Status.start && (int)(UnityEngine.Time.time * 1000) - m_delayTime >= m_delay)
                {
                    this.m_status = Status.delay;
                }
                if (this.m_status == Status.delay)
                {
                    RunAction();
                    m_count--;
                    this.m_status = Status.interval;
                }
                if (this.m_status == Status.interval)
                {
                    if (m_interval == 0)
                    {
                        this.m_status = Status.end;
                        return;
                    }
                    if ((int)(UnityEngine.Time.time * 1000) - m_intervalTime >= m_interval)
                    {
                        if (m_count > 0)
                        {
                            RunAction();
                            if (--m_count < 1)
                            {
                                if(m_endInvoke != null)
                                {
                                    m_endInvoke(m_endparams);
                                }
                                this.m_status = Status.end;
                            }
                        }
                        else
                        {
                            RunAction();
                        }
                    }
                }
            }
            public void Release()
            {
                this.m_invoke1 = null;
                this.m_invoke2 = null;
                this.m_params = null;
                this.m_delay = 0;
                this.m_interval = 0;
                this.m_delayTime = 0;
                this.m_intervalTime = 0;
                this.m_status = Status.end;
            }
        }
        private List<InvokePool> m_usingList;
        private List<InvokePool> m_freeList;
        private int m_invokeIndex;
        public Invoke(int poolCount)
        {
            m_invokeIndex = 0;
            m_usingList = new List<InvokePool>(poolCount);
            m_freeList = new List<InvokePool>(poolCount);
            for (int i = 0; i < poolCount; i++)
            {
                InvokePool invokePool = new InvokePool();
                m_freeList.Add(invokePool);
            }
        }
        public void LateUpdate()
        {
            if (m_usingList.Count <= 0)
            {
                return;
            }
            for (int i = m_usingList.Count - 1; i >= 0; i--)
            {
                if (i >= m_usingList.Count) break;
                var l = m_usingList[i];
                if (l.status == InvokePool.Status.end)
                {
                    if (i >= m_usingList.Count) break;
                    m_usingList.RemoveAt(i);
                    l.Release();
                    m_freeList.Add(l);
                    continue;
                }
                l.Update();
            }
        }
        /// <summary>
        /// 单次执行
        /// <param name="callback">无参数回调方法</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// </summary>
        public int Call(Action callback, int delay)
        {
            return CallFunction(callback, delay, 0, 0);
        }

        /// <summary>
        /// 永久执行
        /// <param name="callback">无参数回调方法</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// <param name="interval">间隔（单位为秒）</param>
        /// </summary>
        public int Call(Action callback, int delay, int interval)
        {
            return CallFunction(callback, delay, interval, 0);
        }

        /// <summary>
        /// 多次执行
        /// <param name="callback">无参数回调方法</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// <param name="interval">间隔（单位为秒）</param>
        /// <param name="count">重复次数，为1则延时后执行回调方法再过间隔再执行1次回调方法</param>
        /// </summary>
        public int Call(Action callback, int delay, int interval, int count)
        {
            return CallFunction(callback, delay, interval, count);
        }

        /// <summary>
        /// 单次执行
        /// <param name="callback">带参数回调方法</param>
        /// <param name="param">参数</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// </summary>
        public int Call(Action<object> callback, object param, int delay)
        {
            return CallFunction(callback, param, delay, 0, 0);
        }

        /// <summary>
        /// 永久执行
        /// <param name="callback">带参数回调方法</param>
        /// <param name="param">参数</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// <param name="interval">间隔（单位为秒）</param>
        /// </summary>
        public int Call(Action<object> callback, object param, int delay, int interval)
        {
            return CallFunction(callback, param, delay, interval, 0);
        }

        /// <summary>
        /// 多次执行
        /// <param name="callback">带参数回调方法</param>
        /// <param name="param">参数</param>
        /// <param name="delay">延时（单位为秒）</param>
        /// <param name="interval">间隔（单位为秒）</param>
        /// <param name="count">重复次数，为1则延时后执行回调方法再过间隔再执行1次回调方法</param>
        /// </summary>
        public int Call(Action<object> callback, object param, int delay, int interval, int count)
        {
            return CallFunction(callback, param, delay, interval, count);
        }

        public int Call(Action<object> callback, object param, int delay, int interval, int count, Action<object> endcallback, object param1)
        {
            return CallFunction(callback, param, delay, interval, count, endcallback, param1);
        }

        public void Cancel(int invokeIndex)
        {
            for (int i = 0; i < m_usingList.Count; i++)
            {
                if (m_usingList[i].m_invokeIndex == invokeIndex)
                {
                    CancelOneInvoke(i);
                    break;
                }
            }
        }

        public void Cancel(Action callback)
        {
            for (int i = 0; i < m_usingList.Count; i++)
            {
                if (m_usingList[i].m_invoke1 == callback)
                {
                    CancelOneInvoke(i);
                    break;
                }
            }
        }

        public void Cancel(Action<object> callback)
        {
            for (int i = 0; i < m_usingList.Count; i++)
            {
                if (m_usingList[i].m_invoke2 == callback)
                {
                    CancelOneInvoke(i);
                    break;
                }
            }
        }

        public void CancelAll()
        {
            for (int i = m_usingList.Count - 1; i >= 0; i--)
            {
                CancelOneInvoke(i);
            }
        }

        private void CancelOneInvoke(int index)
        {
            m_usingList[index].Release();
            m_freeList.Add(m_usingList[index]);
            m_usingList.RemoveAt(index);
        }


        private int CallFunction(Action callback, int delay, int interval, int count)
        {
            if (delay < 0 || interval < 0 || count < 0)
            {
                return 0;
            }
            m_invokeIndex++;
            if (m_freeList.Count <= 0)
            {
                InvokePool invokePool = new InvokePool();
                m_freeList.Add(invokePool);
            }
            m_freeList[0].Create(callback, delay, m_invokeIndex, interval, count);
            m_usingList.Add(m_freeList[0]);
            m_freeList.RemoveAt(0);
            return m_invokeIndex;
        }

        private int CallFunction(Action<object> callback, object param, int delay, int interval, int count)
        {
            if (delay < 0 || interval < 0 || count < 0)
            {
                return 0;
            }
            m_invokeIndex++;
            if (m_freeList.Count <= 0)
            {
                InvokePool invokePool = new InvokePool();
                m_freeList.Add(invokePool);
            }
            m_freeList[0].Create(callback, param, delay, m_invokeIndex, interval, count);
            m_usingList.Add(m_freeList[0]);
            m_freeList.RemoveAt(0);
            return m_invokeIndex;
        }

        private int CallFunction(Action<object> callback, object param, int delay, int interval, int count, Action<object> endcallback, object param1)
        {
            if (delay < 0 || interval < 0 || count < 0)
            {
                return 0;
            }
            m_invokeIndex++;
            if (m_freeList.Count <= 0)
            {
                InvokePool invokePool = new InvokePool();
                m_freeList.Add(invokePool);
            }
            m_freeList[0].Create(callback, param, delay, m_invokeIndex, interval, count, endcallback, param1);
            m_usingList.Add(m_freeList[0]);
            m_freeList.RemoveAt(0);
            return m_invokeIndex;
        }
    }
}
