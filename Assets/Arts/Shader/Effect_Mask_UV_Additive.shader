
Shader "Effect/Mask_UV_Additive" {
    Properties {
        _color ("color", Color) = (0.5,0.5,0.5,0.5) 
         _MainTex ("MainTex", 2D) = "white" {}
      
        _MaskTex ("MaskTex", 2D) = "white" {} 
        _SpeedU ("SpeedU", Float ) = 0.1
        _SpeedV ("SpeedV", Float ) = 0.1 
        _Power ("Power",Float) = 1
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
       
            Blend SrcAlpha One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
            #include "UnityCG.cginc"
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal  
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST; 
            uniform fixed4 _color; 
            uniform half _SpeedU;
            uniform half _SpeedV;
            uniform sampler2D _MaskTex; uniform float4 _MaskTex_ST;
           
            half  _Power ;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                half4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                half4 vertexColor : COLOR;
                float2 uv0 : TEXCOORD0;
                float2 movingUV :TEXCOORD1;
                half4 color:TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                float4 time = _Time;
                    o.movingUV =TRANSFORM_TEX(v.texcoord0, _MainTex);
                o.movingUV = half2(((_SpeedU*time.g)+o.movingUV.r),(o.movingUV.g+(time.g*_SpeedV)));
                o.uv0 = TRANSFORM_TEX(o.uv0, _MaskTex);
                o.color = _color*v.vertexColor;


                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            fixed4 frag(VertexOutput i, half facing : VFACE) : SV_Target {
                half isFrontFace = ( facing >= 0 ? 1 : 0 );
                half faceSign = ( facing >= 0 ? 1 : -1 );
                fixed4 _MainTexVar = tex2D(_MainTex,i.movingUV);
                
                fixed4 _MaskTex_var = tex2D(_MaskTex,i.uv0);
                fixed3 emissive = (_MainTexVar.rgb*i.color.rgb *_MaskTex_var.rgb);
                fixed3 finalColor =   _Power  *emissive; 
         
                return fixed4(finalColor,(_MainTexVar.a*i.color.a*_MaskTex_var.r));
            }
            ENDCG
        }
        
    }
}
