return {
    state=require("store/csharp/state"),
    getters = require("store/csharp/getters"),
    mutations = require("store/csharp/mutations"),
    actions = require("store/csharp/actions")
}
