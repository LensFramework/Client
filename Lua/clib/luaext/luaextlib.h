/*
** $Id: lualib.h,v 1.36.1.1 2007/12/27 13:02:25 roberto Exp $
** Lua standard libraries
** See Copyright Notice in lua.h
*/


#ifndef luaextlib_h
#define luaextlib_h

#include "lua.h"
#include "lualib.h"


/* Key to file-handle type */
/*
#define LUA_FILEHANDLE		"FILE*"


#define LUA_COLIBNAME	"coroutine"
*/
LUALIB_API int (luaopen_base_ext) (lua_State *L);

LUALIB_API int (luaopen_table_ext) (lua_State *L);

/*
#define LUA_IOLIBNAME	"io"
LUALIB_API int (luaopen_io) (lua_State *L);

#define LUA_OSLIBNAME	"os"
LUALIB_API int (luaopen_os) (lua_State *L);

#define LUA_STRLIBNAME	"string"
LUALIB_API int (luaopen_string) (lua_State *L);

#define LUA_MATHLIBNAME	"math"
LUALIB_API int (luaopen_math) (lua_State *L);

#define LUA_DBLIBNAME	"debug"
LUALIB_API int (luaopen_debug) (lua_State *L);

#define LUA_LOADLIBNAME	"package"
LUALIB_API int (luaopen_package) (lua_State *L);
*/

/* open all previous libraries */
LUALIB_API void (luaL_openextlibs) (lua_State *L); 



#endif
