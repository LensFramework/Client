local page=class("package",G_MaskBase)
local tag="page:package"

local function onBack()
    L_UI.close("package")
end

function page:ctor(...)
    
end

function page.created(obj,config)
    print(tag,obj,config)
    local backBtn=obj.transform:Find("MarketForms/Btn_Return")
    local btn=backBtn:GetComponent(typeof(UnityEngine.UI.Button))
    btn.onClick:AddListener(onBack)

end

function page.open(options)
    print(tag,"open")
end

function page.close(options)
    
end

function page.destroy(options)
    
end

return page