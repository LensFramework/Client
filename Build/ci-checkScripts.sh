#!/bin/bash
jenkinsWorkspace=$1

curDir=$(cd `dirname $0`; pwd)
cd ..
project_path=$(cd `dirname $0`; pwd)
echo ${project_path}
cd $curDir
./tools/tsc_tools_for_mac/tscancode --xml --quiet ${project_path} 2>${jenkinsWorkspace}/attachments/tscancode.xml
