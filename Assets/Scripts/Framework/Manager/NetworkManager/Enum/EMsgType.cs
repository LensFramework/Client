﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public enum EMsgType
    {
        Login=60001,
        /// <summary>
        /// 心跳包
        /// </summary>
        Heart=60002,
        /// <summary>
        /// 用于接受服务器的没有id的数据
        /// </summary>
        Raw=0
    }
}
