return {
  prefix={
    asset="Assets/ResourcesAssets/UI/",
    lua="ui/"
  },
  -- 辅助，ui管理器初始化后会初始化辅助类代码，并持续存在，辅助类代码可以绑定UI也可以不绑定UI
  auxiliary={
    -- 用于处理网络的问题，比如，网络菊花，网络重连确认窗口
    ["network"]={assetName="network",luaPath="auxiliary/network",layer=900},
    -- 用于接收服务发送的奖励推送，收到推送则打开对应unique界面并传入参数
    ["reward"]={luaPath="auxiliary/reward"},
  },
  stack={
    layers=0,
    pages={
      ["login"]={assetName="login",luaPath="pages/login",layer=0},
      ["selectserver"]={assetName="selectserver",luaPath="pages/selectserver",layer=0},
      ["main"]={assetName="main",luaPath="pages/main",layer=0}
    }
  },
  fixed={
    -- 界面上固定不变的UI如主城的top,滚动广播等
    layers=10,
    pages={
      ["heroinfo"]={assetName="heroinfo",luaPath="pages/heroinfo",layer=10}
    }
  },
  modal={
    layers=20,
    pages={
      ["package"]={assetName="package",luaPath="pages/package",layer=20},
      ["confirm"]={assetName="confirm",luaPath="pages/confirm",layer=20}
    }
  },
  unique={
    -- 队列弹框，比如：奖励领取，只能出现一次,后出现的自动排队，等待第一个消失
    layers=30,
    pages={
      
    }
  },
  guide={
    -- 新手引导
    layers=40,
    pages={

    }
  },
  -- 全屏遮挡类型的界面，比如loading。章节切换等，只提供简单的显示和隐藏功能，只能显示一个
  mask={
    -- 新手引导
    layers=50,
    pages={
      ["loading"]={assetName="loading",luaPath="pages/loading",layer=50,preload=true}
    }
  }
}