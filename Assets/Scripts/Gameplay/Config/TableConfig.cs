﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization;
using LitJson;

namespace Lens.Gameplay.Config
{
    using Framework.Config;
    public class TableConfig:ITableConfig
    {
        public THero hero;
        public TEquipSystem equipSystem;
    }
}