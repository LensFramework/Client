﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UnityEngine;
namespace Lens.Framework.Console
{
    using Core;
    using System.IO;
    using System.Threading.Tasks;
    using UnityEngine.Networking;

    public class Log:Singleton<Log>
    {
        public class MyOptions : Options
        {
            public float maxSizeOfSave = 1;//M
            public string uploadUrl = "";
            public string logPath;
            public int intervalSave = 1000;
        }
        public static bool isInited { get; private set; }
        public static bool useLog=true;
        private Dictionary<string,string> m_dics=new Dictionary<string,string>();
        private List<string> m_marks=new List<string>();
        private Thread m_thread;
        private StringBuilder m_sb;
        private FileStream m_fs;
        private UnityWebRequest m_www;
        private MyOptions m_options;
        public async override Task Initialize(Options options = null)
        {
            m_options = options as MyOptions;
            m_dics.Add("Info", "#FFFFFF");
#if DEBUGGER || RELEASE
            m_sb = new StringBuilder();
            Application.logMessageReceivedThreaded += OnLogMessage;
#endif
            isInited = true;
        }
        public override void Dispose()
        {
            if (m_thread!=null)
            {
                m_thread.Abort();
                m_thread = null;
            }
            if (m_fs!=null)
            {
                m_fs.Close();
                m_fs.Dispose();
                m_fs = null;
            }
            if (m_www!=null)
            {
                m_www.Abort();
                m_www.Dispose();
                m_www = null;
            }
            base.Dispose();
        }
        public static void SetLogs(Dictionary<string,string> dics)
        {
            p_instance.m_dics = dics;
            if (!p_instance.m_dics.ContainsKey("Info"))
            {
                p_instance.m_dics.Add("Info", "#FFFFFF");
            }
        }
        public static void StartThreadSave()
        {
            p_instance.m_thread = new Thread(OnSave) { Name = "BeginConnent", IsBackground = true };

            p_instance.m_thread.Start();
        }
        public static Dictionary<string,string> GetLogs()
        {
            return p_instance.m_dics;
        }
        public static void SetTags(Dictionary<string, string> dic)
        {
            foreach (var item in dic)
            {
                if (p_instance.m_dics.ContainsKey(item.Key))
                {
                    p_instance.m_dics[item.Key] = item.Value;
                }
                else
                {
                    p_instance.m_dics.Add(item.Key, item.Value);
                }
            }
        }
        public static void SetMarks(List<string> marks)
        {
            for (int i = 0; i < marks.Count; i++)
            {
                p_instance.m_marks.Add(marks[i]);
            }
        }
        public static void Info(string value, string type="Info")
        {
            if (useLog)
            {
                string result;
                if(!p_instance.m_dics.TryGetValue(type,out result))
                {
                    result = RandomColor();
                    p_instance.m_dics.Add(type, result);
                }
#if UNITY_EDITOR
                UnityEngine.Debug.Log("<color=" + result + "> [" + type + "] </color> " + value);
#else
                UnityEngine.Debug.Log("[" + type + "]  " + value);
#endif
                if (p_instance.m_thread !=null)
                {
                    p_instance.m_sb.AppendLine("[" + type + "]  [" + value);
                }
            }
        }
        public static void Warning(string value, string type = "Info")
        {
            if (useLog)
            {
                string result;
                if (!p_instance.m_dics.TryGetValue(type, out result))
                {
                    result = RandomColor();
                    p_instance.m_dics.Add(type, result);
                }
#if UNITY_EDITOR
                UnityEngine.Debug.LogWarning("<color=" + result + "> [" + type + "] </color> [" + System.DateTime.Now.ToString("hh:mm:fff") + "]  " + value);
#else
                UnityEngine.Debug.LogWarning("[" + type + "]  [" + System.DateTime.Now.ToString("hh:mm:fff") + "]  " + value);
#endif
                if (p_instance.m_thread != null)
                {
                    p_instance.m_sb.AppendLine("Warning "+"[" + type + "]  [" + System.DateTime.Now.ToString("hh:mm:fff") + "]  " + value);
                }
            }
        }
        public static void Filter(string value,string mark, string type="Info")
        {
            if(p_instance.m_marks.Contains(mark)){
                Info(type,value);
            }
        }
        private static string RandomColor()
        {
            int r = UnityEngine.Random.Range(0, 255);
            int g = UnityEngine.Random.Range(0, 255);
            int b = UnityEngine.Random.Range(0, 255);
            return ColorTo(r, g, b);
        }
        public static string ColorTo(int r,int g,int b)
        {
            string rs = r.ToString("X");
            string gs = g.ToString("X");
            string bs = b.ToString("X");
            if (rs.Length == 1)
            {
                rs = "0" + rs;
            }
            if (gs.Length == 1)
            {
                gs = "0" + gs;
            }
            if (bs.Length == 1)
            {
                bs = "0" + bs;
            }
            return "#" + rs + gs + bs;
        }
        private static void OnLogMessage(string condition, string stackTrace, LogType type)
        {
            if (type==LogType.Error || type==LogType.Exception || type==LogType.Assert)
            {
                p_instance.m_sb.AppendLine(condition + " => " + stackTrace);
            }
        }
        private static void OnSave()
        {
            p_instance.m_fs = new FileStream(p_instance.m_options.logPath, FileMode.Append, FileAccess.Write);
            while (true)
            {
                if (p_instance.m_fs ==null)
                {
                    continue;
                }
                if (p_instance.m_fs.Length > 1024 * 1024* p_instance.m_options.maxSizeOfSave)
                {
                    p_instance.m_fs.Close();
                    p_instance.m_fs.Dispose();
                    p_instance.m_fs = new FileStream(p_instance.m_options.logPath, FileMode.Append, FileAccess.Write);
                }
                var bytes = System.Text.Encoding.UTF8.GetBytes(p_instance.m_sb.ToString());
                p_instance.m_fs.Write(bytes, 0, bytes.Length);
                p_instance.m_sb.Length = 0;
                Thread.Sleep(p_instance.m_options.intervalSave);
            }   
        }
        public static void Upload(Action<bool> onComplete)
        {
            if (p_instance.m_thread !=null && p_instance.m_thread.IsAlive)
            {
                p_instance.m_fs.Close();
                p_instance.m_fs.Dispose();
                p_instance.m_fs = null;

                List<IMultipartFormSection> list = new List<IMultipartFormSection>();
                list.Add(new MultipartFormDataSection("projectid", "2"));
                p_instance.m_www = UnityWebRequest.Post(p_instance.m_options.uploadUrl, list);
                p_instance.m_www.timeout = 6000;
                var request = p_instance.m_www.SendWebRequest();
                request.completed += (obj) =>
                {
                    if (obj == null)
                    {
                        onComplete(false);
                        return;
                    }
                    UnityWebRequestAsyncOperation async = obj as UnityWebRequestAsyncOperation;
                    if (async == null)
                    {
                        Debug.LogError("battleReportResponse is async: null");
                        p_instance.m_www = null;
                        if (p_instance.m_www != null)
                        {
                            try
                            {
                                p_instance.m_www.Abort();
                                p_instance.m_www.Dispose();
                            }
                            catch { }
                            p_instance.m_www = null;
                        }
                        onComplete(false);
                        return;
                    }
                    UnityWebRequest www = async.webRequest;
                    if (www == null)
                    {
                        Debug.LogError("Log Upload is www: null");
                        p_instance.m_www = null;
                        if (p_instance.m_www != null)
                        {
                            try
                            {
                                p_instance.m_www.Abort();
                                p_instance.m_www.Dispose();
                            }
                            catch { }
                            p_instance.m_www = null;
                        }
                        onComplete(false);
                        return;
                    }
                    if (www.isNetworkError)
                    {
                        Debug.LogError("Log Upload is isNetworkError: " + www.error + "    " + www.url);
                        p_instance.m_www = null;
                        onComplete(false);
                        return;
                    }
                    Debug.Log("Reponse:" + www.downloadHandler.text);
                    if (p_instance.m_www != null)
                    {
                        try
                        {
                            p_instance.m_www.Abort();
                            p_instance.m_www.Dispose();
                        }
                        catch { }
                        p_instance.m_www = null;
                    }
                    onComplete(true);
                };

                p_instance.m_fs = new FileStream(p_instance.m_options.logPath, FileMode.OpenOrCreate, FileAccess.Write);
            }
        }
    }
}
