﻿

namespace Lens.Gameplay.Managers
{
    public enum ETimelineEventType
    {
        Event,
        AnimatorTrigger,
        StartAudio,
        StopAudio
    }
}