#include <stdio.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <err.h>

extern "C" {	
    char * IPv6_getIpAddr( const char * ipv4 , const char * port )
    {
        struct addrinfo hints , *res0 ;
        int error ;
        memset(&hints,0,sizeof(addrinfo));
        hints.ai_family = PF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_DEFAULT;
        error = getaddrinfo(ipv4, port, &hints ,&res0);
        if(error)
        {
            NSLog(@"%s",gai_strerror(error));
            return NULL;
        }
        if( NULL == res0 ) return NULL ;
        char ipstr[128];
        if( AF_INET6 == res0->ai_family)
        {
            const char* ip_str = inet_ntop(AF_INET6, &(((struct sockaddr_in6*)res0->ai_addr)->sin6_addr), ipstr, sizeof(ipstr));
            NSLog(@"%s --> %s \n",ipv4 , ip_str);
            char* ret = (char*)malloc(strlen(ipstr)+1);
            strcpy(ret,ipstr);
            freeaddrinfo(res0);
            return ret;
        }
        freeaddrinfo(res0);
        return NULL;
    }
}