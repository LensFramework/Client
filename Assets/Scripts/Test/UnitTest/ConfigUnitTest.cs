﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;


namespace Lens.UnitTest
{
    using Lens.Gameplay.Config;
    using Lens.Framework.Config;
    public class ConfigUnitTest : MonoBehaviour
    {
        public bool isBaModel;
        Config m_config;
        // Start is called before the first frame update
        async void Start()
        {
            m_config = Config.GetInstance();
            m_config.loadAssetHandler = OnLoad;
            await m_config.Initialize(new Config.MyOptions()
            {
                tableConfigType = typeof(TableConfig),
                memConfigType = typeof(MemConfig)
            });
            var hero = Config.GetTableConfig<TableConfig>().equipSystem.GetId(3);
            UnityEngine.Debug.Log(hero.help);
        }

        // Update is called once per frame
        void OnGUI()
        {
            if (GUILayout.Button("Save"))
            {
                m_config.Dispose();
                m_config = Config.GetInstance();
                m_config.SaveTable(new Config.MyOptions()
                {
                    tableConfigType = typeof(TableConfig),
                    memConfigType = typeof(MemConfig)
                }, Application.dataPath + "/ResourcesAssets/Config/Table",null);
                m_config.Dispose();
            }
        }
        private async Task<UnityEngine.Object> OnLoad(string assetName)
        {
            return await UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<Object>(assetName).Task;
        }
        private void OnDestroy()
        {
            m_config.Dispose();
        }
    }
}
