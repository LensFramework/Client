﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEngine;

public class AddressablesImport : AssetPostprocessor
{
    public enum EVariantType
    {
        High,
        Middle,
        Low
    }
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        var setting = AssetDatabase.LoadAssetAtPath<AddressableAssetSettings>("Assets/AddressableAssetsData/AddressableAssetSettings.asset");
        foreach (string str in importedAssets)
        {
            ResetEntry(setting, str);
        }
        foreach (var str in movedAssets)
        {
            ResetEntry(setting, str);
        }
    }
    static void ResetEntry(AddressableAssetSettings setting,string str)
    {
        var guid = AssetDatabase.AssetPathToGUID(str); //获得GUID
        if (AssetDatabase.IsValidFolder(str)) return;
        string rootPath = "Assets/ResourcesAssets/";
        var strNor = str.Replace("\\", "/");
        if (strNor.Contains(rootPath))
        {
            var s = strNor.Replace(rootPath, "");
            string classifyStr = s.Substring(0, s.IndexOf("/"));
            string groupStr="Default Remote Group";
            var group = setting.FindGroup(groupStr);
            if (group == null)
            {
                group = setting.CreateGroup(groupStr, false, false, true, null, typeof(BundledAssetGroupSchema),typeof(ContentUpdateGroupSchema));
                var bundleAssetGroupSchema = group.GetSchema<BundledAssetGroupSchema>();
                bundleAssetGroupSchema.BundleMode = BundledAssetGroupSchema.BundlePackingMode.PackTogether;
                bundleAssetGroupSchema.BuildPath.SetVariableByName(group.Settings, AddressableAssetSettings.kRemoteBuildPath);
                bundleAssetGroupSchema.LoadPath.SetVariableByName(group.Settings, AddressableAssetSettings.kRemoteLoadPath);
                group.GetSchema<ContentUpdateGroupSchema>().StaticContent = true;
            }
            var entry = setting.CreateOrMoveEntry(guid, group); //通过GUID创建entry
            if (strNor.Contains("/Lua/"))
            {
                entry.SetAddress(strNor);
                setting.AddLabel(classifyStr);
                entry.SetLabel(classifyStr, true);
            }
            else
            {
                s = s.Replace(classifyStr + "/", "");
                var len = System.Enum.GetNames(typeof(EVariantType));
                for (int i = 0; i < len.Length; i++)
                {
                    setting.AddLabel(len[i]);
                    entry.SetLabel(len[i], false);
                }
                if (s.StartsWith("-"))
                {
                    string variant = s.Substring(1, s.IndexOf("/") - 1);
                    entry.SetAddress(str.Replace("-" + variant + "/", ""));
                    entry.SetLabel(variant, true);
                }
                else
                {
                    entry.SetAddress(str);
                }
            }
        }
        else
        {
            setting.RemoveAssetEntry(guid);
        }
    }
}
