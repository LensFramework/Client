/*
** $Id: ltablib.c,v 1.38.1.3 2008/02/14 16:46:58 roberto Exp $
** Library for Table Manipulation
** See Copyright Notice in lua.h
*/


#include <stddef.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "string.h"


static int clone(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	void *pSrc = lua_topointer(L, 1);
	lua_createtable(L, 0, 0);
	void *pDst = lua_topointer(L, 1);
	memcpy(pDst, pSrc, )
	return 1;
}

/* }====================================================== */


static const luaL_Reg tab_funcs[] = {
	{ "clone", clone },
  {NULL, NULL}
};


LUALIB_API int luaopen_table_ext(lua_State *L) {
  luaL_register(L, LUA_TABLIBNAME, tab_funcs);
  return 1;
}

