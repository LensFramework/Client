﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{

    public enum EAnimationType
    {
        Idle = 0,
        Dead = 1,
        Attack = 2,
        Cheer = 3,
        Run = 4,
        Shoot = 5,
        Skill = 6,
        Stun = 7,
        Walk = 8,
        Storage = 9,
        KnockBack = 10,
        Gethit = 11,
        Getup = 12,
        Attack2 = 13,
        Attacksp = 14,
        Exclaim = 15,
        Shoot2 = 16,
    }
    public class EAnimationTypeMapped
    {

        public static int[] TriggerHashCode = new int[] {
            "Idle".GetHashCode(),
            "Dead".GetHashCode(),
            "Attack".GetHashCode(),
            "Cheer".GetHashCode(),
            "Run".GetHashCode(),
            "Shoot".GetHashCode(),
            "Skill".GetHashCode(),
            "Stun".GetHashCode(),
            "Walk".GetHashCode(),
            "Storage".GetHashCode(),
            "Knockback".GetHashCode(),
            "Gethit".GetHashCode(),
            "Getup".GetHashCode(),
            "Attack2".GetHashCode(),
            "Attacksp".GetHashCode(),
            "Shoot2".GetHashCode(),
        };

        public static string[] AnimClip = new string[] {
            "Idle",
            "Dead",
            "Attack",
            "Attack1",
            "Cheer",
            "Run",
            "Shoot",
            "Skill",
            "Stun",
            "Walk",
            "Storage",
            "Knockback",
            "Gethit",
            "Getup",
            "Attack2",
            "Attacksp",
            "Shoot2",
        };

        public static int[] AnimStateHashCode = new int[] {
            "Idle".GetHashCode(),
            "Dead".GetHashCode(),
            "Attack".GetHashCode(),
            "Attack1".GetHashCode(),
            "Cheer".GetHashCode(),
            "Run".GetHashCode(),
            "Shoot".GetHashCode(),
            "Skill".GetHashCode(),
            "Stun".GetHashCode(),
            "Walk".GetHashCode(),
            "Storage".GetHashCode(),
            "Knockback".GetHashCode(),
            "Gethit".GetHashCode(),
            "Getup".GetHashCode(),
            "Attack2".GetHashCode(),
            "Attacksp".GetHashCode(),
            "Shoot2".GetHashCode(),
        };
    }
}
