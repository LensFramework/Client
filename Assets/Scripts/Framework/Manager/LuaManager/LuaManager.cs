﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Managers
{
    using Core;
    using LuaInterface;
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using UnityEngine.AddressableAssets;

    public class LuaManager : Singleton<LuaManager>
    {
        public static LuaState mainState { get { return p_instance.m_luaState; } }
        private LuaState m_luaState = null;
        private bool m_isLuaStarted;

        public async override Task Initialize(Options options=null )
        {
            var loader = LuaFileUtils.Instance;
            loader.Dispose();
            m_luaState = new LuaState();
            //可根据需要初始化  
            m_luaState.OpenLibs(LuaDLL.luaopen_lpeg);
            m_luaState.OpenLibs(LuaDLL.luaopen_protobuf_c);
            m_luaState.OpenLibs(LuaDLL.luaopen_protobuf_c_ext);
            m_luaState.OpenLibs(LuaDLL.luaopen_profiler);
            m_luaState.OpenLibs(LuaDLL.luaopen_snapshot);
            m_luaState.OpenLibs(LuaDLL.luaopen_package_ext);
            m_luaState.OpenLibs(LuaDLL.luaopen_crypt);
            m_luaState.OpenLibs(LuaDLL.luaopen_pb);
            m_luaState.OpenLibs(LuaDLL.luaopen_struct);
            m_luaState.LuaSetTop(0);
    #if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            m_luaState.OpenLibs(LuaDLL.luaopen_bit);
    #endif
            AddInt64Libs();
            RemoveCompatLibs();
#if UNITY_EDITOR
            if (LuaConst.openLuaDebugger)
            {
                OpenZbsDebugger();
            }
#endif
            OpenCJson();
            m_luaState.LuaSetTop(0);
            LuaBinder.Bind(m_luaState);
            DelegateFactory.Init();
#if !UNITY_EDITOR && RELEASE
            var allLocations=await Addressables.LoadResourceLocationsAsync("Lua").Task;
            for (int i = 0; i < allLocations.Count; i++)
            {
                var result = await Addressables.LoadAssetAsync<TextAsset>(allLocations[i].PrimaryKey).Task;
                var str = allLocations[i].PrimaryKey.Replace("Assets/ResourcesAssets/Lua/", "");
                str = str.Substring(str.IndexOf("/")+1);
                str = str.Replace(".bytes", "");
                LuaFileUtils.Instance.Preload(str, result.bytes);
            }
#else
            m_luaState.AddSearchPath(Path.Combine(Application.dataPath, "../Lua/src"));
            m_luaState.AddSearchPath(Path.Combine(Application.dataPath, "../Lua/lua_modules"));
            m_luaState.AddSearchPath(Path.Combine(Application.dataPath, "../../Share/Config/Lua"));
#endif
            await base.Initialize(options);
        }
        
        public override void Dispose()
        {
            if (m_luaState != null)
            {
                m_luaState.Dispose();
            }
            base.Dispose();
        }
        public override void Update()
        {
            base.Update();
        }
        public void Start(string main="main.lua")
        {
            m_luaState.Start();
            m_luaState.DoFile(main);
            m_isLuaStarted = true;
        }
        public static void DoString(string str)
        {
            p_instance.m_luaState.DoString(str);
        }
        public static bool IsLuaStarted()
        {
            if (p_instance == null) return false;
            return p_instance.m_isLuaStarted;
        }
        public static void ReadByteFiles(string filePath,Action<LuaByteBuffer> callback)
        {
#if !UNITY_EDITOR && RELEASE
            Addressables.LoadAssetAsync<TextAsset>("Assets/ResourcesAssets/Lua/src/" + filePath).Completed+=(handler)=> {
                callback(new LuaByteBuffer(handler.Result.bytes));
            };
#else
            var bytes = File.ReadAllBytes(Path.Combine(Application.dataPath, "../Lua/src/" + filePath));
            if (bytes != null)
            {
                callback(new LuaByteBuffer(bytes));
            }
            else
            {
                UnityEngine.Debug.LogError("dont has this bytes file:" + filePath);
                callback(null);
            }
#endif
        }
        public static LuaTable GetTable(string fullPath)
        {
            return p_instance.m_luaState.GetTable(fullPath);
        }

        [LuaInterface.MonoPInvokeCallback(typeof(LuaCSFunction))]
        public static int _toint64(IntPtr luaState)
        {
            long n = LuaDLL.tolua_toint64(luaState, 1);
            LuaDLL.tolua_pushint64(luaState, n);
            return 1;
        }

        [LuaInterface.MonoPInvokeCallback(typeof(LuaCSFunction))]
        public static int _int64ToString(IntPtr luaState)
        {
            long n = LuaDLL.tolua_toint64(luaState, 1);
            LuaDLL.lua_pushstring(luaState, n.ToString());
            return 1;
        }

        [LuaInterface.MonoPInvokeCallback(typeof(LuaCSFunction))]
        public static int _isint64(IntPtr luaState)
        {
            LuaDLL.lua_pushboolean(luaState, LuaDLL.tolua_isint64(luaState, 1));
            return 1;
        }

        public void OpenZbsDebugger(string ip = "localhost")
        {
            if (!Directory.Exists(LuaConst.zbsDir))
            {
                Debugger.LogWarning("ZeroBraneStudio not install or LuaConst.zbsDir not right");
                return;
            }

            if (!LuaConst.openLuaSocket)
            {                            
                OpenLuaSocket();
            }

            if (!string.IsNullOrEmpty(LuaConst.zbsDir))
            {
                m_luaState.AddSearchPath(LuaConst.zbsDir);
            }

            m_luaState.LuaDoString(string.Format("DebugServerIp = '{0}'", ip), "@LuaClient.cs");
        }
        void AddInt64Libs()
        {
            m_luaState.LuaGetGlobal("tolua");
            m_luaState.LuaPushString("toint64");
            m_luaState.LuaPushFunction(_toint64);
            m_luaState.LuaRawSet(-3);
            m_luaState.LuaGetGlobal("tolua");
            m_luaState.LuaPushString("isint64");
            m_luaState.LuaPushFunction(_isint64);
            m_luaState.LuaRawSet(-3);
            m_luaState.LuaGetGlobal("tolua");
            m_luaState.LuaPushString("int64ToString");
            m_luaState.LuaPushFunction(_int64ToString);
            m_luaState.LuaRawSet(-3);
        }
        void RemoveCompatLibs()
        {
            m_luaState.LuaGetGlobal("math");
            m_luaState.LuaPushString("mod");
            m_luaState.LuaPushNil();
            m_luaState.LuaRawSet(-3);

            m_luaState.LuaGetGlobal("string");
            m_luaState.LuaPushString("gfind");
            m_luaState.LuaPushNil();
            m_luaState.LuaRawSet(-3);
        }
        void OpenLuaSocket()
        {
            m_luaState.BeginPreLoad();
            m_luaState.RegFunction("socket", LuaDLL.luaopen_socket_core);
            m_luaState.RegFunction("mime.core", LuaDLL.luaopen_mime_core);
            m_luaState.EndPreLoad();
        }
        //cjson 比较特殊，只new了一个table，没有注册库，这里注册一下
        void OpenCJson()
        {
            m_luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
            m_luaState.OpenLibs(LuaDLL.luaopen_cjson);
            m_luaState.LuaSetField(-2, "cjson");

            m_luaState.OpenLibs(LuaDLL.luaopen_cjson_safe);
            m_luaState.LuaSetField(-2, "cjson.safe");
        }
    }

}