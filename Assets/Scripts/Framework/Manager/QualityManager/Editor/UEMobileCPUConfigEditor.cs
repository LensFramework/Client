﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine.iOS;

[CustomEditor(typeof(UEMoblieCPUConfig))]
public class UEMoblieCPUConfigEditor : Editor
{    
    private UEMoblieCPUConfig moblieCPU;

    void OnEnable()
    {
        moblieCPU = target as UEMoblieCPUConfig;        
        for (int i = 0; i < moblieCPU.m_settings.m_Ranks.Length; i++)
            m_ranks.Add(false);
        SortRank(moblieCPU);
    }

    Dictionary<int, string> m_dicRank = new Dictionary<int, string>();
    

    List<int> m_keys = new List<int>();
    List<bool> m_ranks = new List<bool>();

    List<string> m_keyStrs = new List<string>();
    private bool m_preview = false;

    private bool m_previewIOS = false;
    private bool m_previewAndroid = false;
    public void SortRank(UEMoblieCPUConfig moblieCPU)
    {
        m_keys.Clear();
        m_keyStrs.Clear();
        m_dicRank.Clear();
        foreach (var VendorCPU in moblieCPU.m_listvendor)
        {
            var s = VendorCPU.m_vendor + "_" + VendorCPU.m_name;
            if (m_dicRank.ContainsKey(VendorCPU.m_rank))
            {
                m_dicRank[VendorCPU.m_rank] += "\n\t" + s;
            }
            else
            {
                m_dicRank[VendorCPU.m_rank] = s;
            }
        }
        m_dicRank = m_dicRank.OrderBy(o => o.Key).ToDictionary(o => o.Key, p => p.Value);
        foreach (var key in m_dicRank.Keys)
        {
            m_keys.Add(key);
            m_keyStrs.Add(key.ToString());
        }
    }

    private void OnRankDraw()
    {
        string rank = string.Empty;
        for (int i = 0;i < moblieCPU.m_settings.m_Ranks.Length;i++)
        {
            var hardware = moblieCPU.m_settings.m_Ranks[i];
            if (i == 0)
                rank = "GRADE:VERY HIGH";
            else if (i == 1)
                rank = "GRADE:HIGH";
            else if (i == 2)
                rank = "GRADE:MEDIUM";
            else if (i == 3)
                rank = "GRADE:LOW";
            else
                rank = "GRADE:UNKNOWN";
            m_ranks[i] = EditorGUILayout.Foldout(m_ranks[i], rank);
            if (m_ranks[i] == true)
            {
                //GUILayout.BeginHorizontal();               
                EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);
                hardware.iosDevice =
                (DeviceGeneration)EditorGUILayout.EnumPopup("IOS device rank",
                    hardware.iosDevice);
                m_previewIOS = EditorGUILayout.Foldout(m_previewIOS, "IOS Detail:");
                if (m_previewIOS)
                {
                    GUILayout.Label("\tContains:\n\t" + hardware.iosDevice);
                }
                EditorGUILayout.TextArea("Android", GUI.skin.horizontalSlider);
                hardware.androidCpuRank =
                    EditorGUILayout.IntPopup("Android cpu device rank", hardware.androidCpuRank, m_keyStrs.ToArray(),
                        m_keys.ToArray());
                m_previewAndroid = EditorGUILayout.Foldout(m_previewAndroid, "Android Detail:");
                if (m_previewAndroid)
                {
                    if (m_dicRank.ContainsKey(hardware.androidCpuRank))
                        GUILayout.Label("\tContains:\n\t" + m_dicRank[hardware.androidCpuRank]);
                }
                EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);
                GUILayout.Label("Other rank info");
                hardware.coreCount =
                    EditorGUILayout.IntSlider("\tCore count:", hardware.coreCount, 1, 16);
                hardware.frequency =
                    EditorGUILayout.IntSlider("\tCpu frequency:", hardware.frequency, 1500, 3000);
                hardware.memorySize =
                    EditorGUILayout.IntSlider("\tMemory size:", hardware.memorySize, 2048, 8192);
                EditorGUILayout.TextArea("", GUI.skin.horizontalSlider);
                //GUILayout.EndVertical();                
            }                                    
        }
    }
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        serializedObject.Update();



        moblieCPU = target as UEMoblieCPUConfig;

        if (moblieCPU != null)
        {
            if (m_dicRank.Keys.Count == 0 || m_keys.Count == 0)
                SortRank(moblieCPU);

            GUI.skin.label.wordWrap = true;
            OnRankDraw();
            m_preview = EditorGUILayout.Foldout(m_preview, "Show Android CPU Rank References");
            if (m_preview)
            {
                foreach (var pair in m_dicRank)
                {
                    string label = pair.Key.ToString() + "\t" + pair.Value;
                    GUILayout.Label(label);
                }
            }
        }

        EditorUtility.SetDirty(moblieCPU);
        serializedObject.ApplyModifiedProperties();
    }
}

public static class MoblieCPU
{
    [MenuItem("Tools/Analysis Mobile Hardware")]
    private static void AnalysisMobile()
    {
        var path = EditorUtility.OpenFilePanel("Find Analysis File", Application.dataPath, "");
        if (path != null && path != "")
        {
            using (var f = File.OpenRead(path))
            {
                if (f.CanRead)
                {
                    using (StreamReader sr = new StreamReader(f))
                    {
                        var allContext = sr.ReadToEnd();
                        allContext = allContext.Replace("<td>&nbsp;</td>", "");
                        allContext = allContext.Replace(" ", "");
                        allContext = allContext.Replace("target=\"_blank\"", "");
                        allContext = allContext.Replace("<tr>", "*");
                        var results = allContext.Split('*');

                        var asset = ScriptableObject.CreateInstance<UEMoblieCPUConfig>();
                        int index = 0;
                        foreach (var ret in results)
                        {
                            var replaceRet = ret.Replace("<br />", "");
                            replaceRet = replaceRet.Replace("<br>", "");
                            replaceRet = replaceRet.Replace("\n", "");
                            replaceRet = replaceRet.Replace("<td>", "*");

                            List<string> listStr = new List<string>();
                            var subResults = replaceRet.Split('*');
                            foreach (var subret in subResults)
                            {
                                var matchRet = Regex.Match(subret, "html#(.*)\">([^<]+)</a>");
                                if (matchRet.Success)
                                {
                                    if (matchRet.Groups.Count > 2)
                                    {
                                        string vendor = matchRet.Groups[1].ToString();
                                        if (vendor == "apple")
                                            continue;

                                        string vendorName = matchRet.Groups[1].ToString().ToLower();
                                        string deviceName = matchRet.Groups[2].ToString();
                                        deviceName = deviceName.Replace("麒麟", "ql");
                                        deviceName = deviceName.Replace("骁龙", "xl");
                                        deviceName = deviceName.ToLower();
                                        if (vendorName.Contains("xl"))
                                            vendorName = "qualcomm";

                                        asset.m_listvendor.Add(new UEMoblieCPUConfig.VendorCPU(index, vendorName, deviceName));
                                    }
                                }
                            }

                            ++index;
                            if (index > 30) break;
                        }

                        AssetDatabase.CreateAsset(asset, "Assets/UEMoblieCPUConfig.asset");
                        AssetDatabase.SaveAssets();

                        Selection.activeObject = asset;

                    }
                    f.Close();
                }
            }
        }
    }
}
