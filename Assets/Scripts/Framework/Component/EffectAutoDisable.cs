﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Components
{
#pragma warning disable 0618
    [ExecuteInEditMode]
    public class EffectAutoDisable : MonoBehaviour
    {

        public ParticleSystem[] particleSystemArr;

        void OnEnable()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                particleSystemArr = gameObject.GetComponentsInChildren<ParticleSystem>();
            }
#endif
        }

        public void Reset()
        {
            if (particleSystemArr != null)
            {
                for (int i = 0; i < particleSystemArr.Length; i++)
                {
                    var item = particleSystemArr[i];
                    ParticleSystem.EmissionModule psemit = item.emission;
                    psemit.enabled = true;
                    item.Simulate(0.0f, true, true);
                    item.Clear();
                    item.Play();
                    item.enableEmission = true;
                }
            }
        }
    }
}