﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using Framework.Managers;
    using ProtoBuf;
    using System.Net;

    public class TcpManagerUnitTest : MonoBehaviour
    {
        public string mainLua= "test/unitTest/tcpUnitTest";
        private TcpManager m_tcpManager;
        private string m_challenge;
        private LuaManager m_luaManager;
        private LuaInterface.LuaTable m_luaNetTable;
        private LuaInterface.LuaFunction m_luaResNetFunction;
        // Use this for initialization
        async void Start()
        {
            m_tcpManager = TcpManager.GetInstance();
            await m_tcpManager.Initialize();
            // TcpManager.AddListener(EMsgType.Login, OnLogin);
            m_luaManager = LuaManager.GetInstance();
            await m_luaManager.Initialize();
            // m_luaManager.Preload();
            m_luaManager.Start(mainLua);
            m_luaNetTable = LuaManager.GetTable("L_Net");
            m_luaResNetFunction=m_luaNetTable.GetLuaFunction("C_OnDisconnect");
            TcpManager.AddListener(ENetState.Connected, ()=>{
                var f=m_luaNetTable.GetLuaFunction("C_OnConnectSuccess");
                f.Call();
                f.Dispose();
            });
            TcpManager.AddListener(ENetState.Disconnected, ()=>{
                var f=m_luaNetTable.GetLuaFunction("C_OnDisconnect");
                f.Call();
                f.Dispose();
            });
            // TcpManager.AddListenAll(EPackType.LuaProto, (data,msgId)=>{
            //     m_luaResNetFunction.Call();
            // });
            // TcpManager.AddListenAll(EPackType.Bytes, (data,msgId)=>{
            //     m_luaResNetFunction.Call();
            // });
            // TcpManager.AddListener(EMsgType.Raw,(o)=>{
            //     UnityEngine.Debug.Log(o);
            //     var rawExtensible=o as RawExtensible;
            //     m_challenge=System.Convert.ToBase64String(rawExtensible.bytes);
            //     UnityEngine.Debug.Log(m_challenge);
            // });
        }

        // Update is called once per frame
        void Update()
        {
            m_luaManager.Update();
            m_tcpManager.Update();
        }
        void OnGUI()
        {
            if (GUILayout.Button("login"))
            {
                TcpManager.Connect("118.190.206.94", 8002);
            }
            if (GUILayout.Button("Connect"))
            {
                TcpManager.Disconnect();
                TcpManager.Connect("118.190.206.94", 8003);
            }
            if (GUILayout.Button("ReConnect"))
            {
                TcpManager.Reconnect();
            }
            if (GUILayout.Button("Send"))
            {
                TcpManager.Send(0, System.Text.Encoding.UTF8.GetBytes(""));
            }
            if (GUILayout.Button("Disconnect"))
            {
                TcpManager.Disconnect();
            }
        }
        void OnDestroy()
        {
            m_luaManager.Dispose();
            m_tcpManager.Dispose();
        }
        void OnLogin(IExtensible obj)
        {
            UnityEngine.Debug.Log("Login:" + obj);
        }
    }
}