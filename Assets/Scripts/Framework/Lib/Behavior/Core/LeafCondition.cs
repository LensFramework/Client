﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{
    public abstract class LeafCondition : LeafTask
    {
        public override void Ready()
        {
        }
        public override void End()
        {
        }
        public override void OnReset()
        {
        }
    }
}
