﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Lens.Framework.Utility
{
    using Core;
    public class SignatureUtility
    {
        private static RSACryptoServiceProvider m_rsa;
        private static SHA1 m_sha;
        public static byte[] SignatureFile(byte[] bytes)
        {
            Init();
            var sig = m_rsa.SignData(bytes, m_sha);
            byte[] newBytes = new byte[bytes.Length + sig.Length];
            Array.Copy(sig, 0, newBytes, 0, sig.Length);
            Array.Copy(bytes, 0, newBytes, sig.Length, bytes.Length);
            return newBytes;
        }
        public static byte[] Unsignature(byte[] data)
        {
            Init();
            if (data == null)
            {
                return null;
            }
            if (data.Length < 128)
            {
                throw new InvalidProgramException(" length less than 128!");
            }

            byte[] sig = new byte[128];
            byte[] filecontent = new byte[data.Length - sig.Length];
            Array.Copy(data, sig, sig.Length);
            Array.Copy(data, sig.Length, filecontent, 0, filecontent.Length);
            
            if (m_rsa.VerifyData(filecontent, m_sha, sig))
            {
                return filecontent;
            }
            else
            {
                UnityEngine.Debug.LogError(" has invalid signature!");
                return data;
            }
        }
        public static void ResetKey()
        {
            m_rsa = null;
            Init();
        }
        public static void ResetKey(string publicKey)
        {
            m_rsa = new RSACryptoServiceProvider();
            m_rsa.ImportCspBlob(Convert.FromBase64String(publicKey));
            m_sha = new SHA1CryptoServiceProvider();
        }
        private static void Init()
        {
            if (m_rsa==null)
            {
                m_rsa = new RSACryptoServiceProvider();
                m_rsa.ImportCspBlob(Convert.FromBase64String(Macro.publicKey));
                m_sha = new SHA1CryptoServiceProvider();
            }
        }
    }
}
