local storeModu={}
storeModu.cls={}
storeModu.data={}
local store={
    data={
        dic={}
    }
}
local mapedStates={}
local function wraState (module,mapedStates,state)
    local mt={}
    mt._handlers={}
    mt._values={}
    mt.module=module
    for key, value in pairs(state) do
        mt._values[key]=value
        mt._handlers[key] = {
            set = function(v)
                if mapedStates[mt.module] then
                    for i = 1,#mapedStates[mt.module] , 1 do
                        for ke, val in pairs(mapedStates[mt.module][i]) do
                            if key==ke then
                                val(mt._values[key],v)
                            end
                        end
                    end
                end
                mt._values[key] = v
            end,
            get = function()
                return mt._values[key]
            end
        }
    end
    mt.__index = function(t, k)
        local h = mt._handlers[k]
        -- print("get", k)
        return h and h.get()
    end
    mt.__newindex = function(t, k, v)
        local h = mt._handlers[k]
        -- print("set", k,v)
        if h then
            return h.set(v)
        else
            mt._values[k] = v
            mt._handlers[k] = {
                set =  function(v)
                    if mapedStates[mt.module] then
                        for i = 1,#mapedStates[mt.module] , 1 do
                            for ke, val in mapedStates[mt.module][i] do
                                if k==ke then
                                    val(mt._values[k],v)
                                end
                            end
                        end
                    end
                     mt._values[k] = v
                end,
                get = function()
                    return mt._values[k] 
                end
            }
        end
    end
    return setmetatable({},mt)
end
function storeModu:ctor(module,req)
    self.module=module
    self.cls.state = wraState(module,mapedStates,req.state)
    self.cls.getters = req.getters
    self.cls.mutations = req.mutations
    self.cls.actions =req.actions
end

function storeModu:dispatch( act, params)
    for k, v in pairs(self.cls) do
        if k == "actions" then
            for kc, vc in pairs(v) do
                if kc == act then
                    return vc(self, params)
                end
            end
        end
    end
    return nil
end

function storeModu:commit( com, params)
    for k, v in pairs(self.cls) do
        if k == "mutations" then
            for kc, vc in pairs(v) do
                if kc == com then
                    return vc(self.cls.state, params)
                end
            end
        end
    end
    return nil
end

function storeModu:getter( get)
    for k, v in pairs(self.cls) do
        if k == "getters" then
            for kc, vc in pairs(v) do
                if kc == get then
                    return vc(self.cls.state)
                end
            end
        end
    end
end

function store:initialize( options )
    if options.modules ~=nil then
        for k,v in pairs(options.modules) do
            self.data.dic[k] = clone(storeModu)
            self.data.dic[k]:ctor(k,v)
        end
    end
end

function store.dispatch( module,actions,params )
    for k, v in pairs(store.data.dic) do
        if k == module then
            return v:dispatch(actions, params)
        end
    end
    error("dont has this module dispatch  " .. module)
    return nil
end

function store.commit( module,commits,params )
    for k, v in pairs(store.data.dic) do
        if k == module then
            return v:commit(commits, params)
        end
    end
    error("dont has this module dispatch" .. module)
    return nil
end

function store.getter( module,params )
    for k, v in pairs(store.data.dic) do
        if k == module then
            return v:getter(params)
        end
    end
    error("dont has this module getter" .. module)
end

function store.mapStates(module,maps )
    if not mapedStates[module] then
        mapedStates[module]={}
    end
    table.insert(mapedStates[module],1,maps )
end

function store.unmapStates(module,maps)
    if mapedStates[module] then
        for i = 1, #mapedStates[module], 2 do
            if mapedStates[module]==maps then
                table.remove(mapedStates[module],i )
                break
            end
        end
    end
end

return store
--[[ test:

  L_Store:dispatch("global","testActions",2);
  L_Store:commit("global","SET_PLAYERID",3);
  local playerId=L_Store:getter("global","playerId");
  print(playerId);

--]]
