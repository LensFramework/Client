local page=class("confirm",G_MaskBase)
local tag="page:confirm"
local m_options
local m_title

local function onCancel()
    L_UI.close("confirm")
end
local function onConfirm()
    if m_options.ok then
        m_options.ok()
    end
    if m_options.cancel then
        m_options.cancel()
    end
    L_UI.close("confirm")
end

function page:ctor(...)
    
end

function page.created(obj,config)
    print(tag,obj,config)
    local cancel=obj.transform:Find("ConfirmForms/BtnCancel")
    local cancelbtn=cancel:GetComponent(typeof(UnityEngine.UI.Button))
    cancelbtn.onClick:AddListener(onCancel)

    local ok=obj.transform:Find("ConfirmForms/BtnConfirm")
    local okbtn=ok:GetComponent(typeof(UnityEngine.UI.Button))
    okbtn.onClick:AddListener(onConfirm)

    local m_titleObj=obj.transform:Find("ConfirmForms/Title")
    m_title=m_titleObj:GetComponent(typeof(UnityEngine.UI.Text))
end

function page.open(options)
    print(tag,"open")
    if not options then
        error(tag,"confirm must has options")
    end
    m_options=options
    if m_options.title then
        m_title.text=m_options.title
    end
end

function page.close(options)
    m_options=nil
end

function page.destroy(options)
    
end

return page