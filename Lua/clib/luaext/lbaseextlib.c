/*
** $Id: lbaselib.c,v 1.191.1.6 2008/02/14 16:46:22 roberto Exp $
** Basic library
** See Copyright Notice in lua.h
*/



#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define lbaselib_c
#define LUA_LIB

#include "lua.h"

#include "lauxlib.h"
#include "lualib.h"
#include "luaextlib.h"



static int luaB_rawgeti (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  int n = lua_tointeger(L, 2);
  lua_settop(L, 1);
  lua_rawgeti(L, 1, n);
  return 1;
}


static int luaB_rawseti (lua_State *L) {
  luaL_checktype(L, 1, LUA_TTABLE);
  int n = lua_tointeger(L, 2);
  luaL_checkany(L, 3);
  lua_settop(L, 3);
  lua_rawseti(L, 1, n);
  lua_pop(L, 1);
  return 1;
}

static const luaL_Reg base_ext_funcs[] = {
  {"rawgeti", luaB_rawgeti},
  {"rawseti", luaB_rawseti},
  {NULL, NULL}
};

LUALIB_API int luaopen_base_ext(lua_State *L) {
	/* set global _G */
	lua_pushvalue(L, LUA_GLOBALSINDEX);
	lua_setglobal(L, "_G");
	luaL_register(L, "_G", base_ext_funcs);
  return 2;
}

