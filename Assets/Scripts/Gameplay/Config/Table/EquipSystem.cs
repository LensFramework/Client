using System.Collections.Generic;

namespace Lens.Gameplay.Config
{
    [System.Serializable]
    public class TEquipSystem
    {
        private EquipSystem[] m_array;
        public EquipSystem GetId(int index)
        {
            for (int i = 0; i < m_array.Length; i++)
            {
                if (m_array[i].id == index)
                {
                    return m_array[i];
                }
            }
            return null;
        }
    }
    [System.Serializable]
    public class EquipSystem
    {
        public int id { get; set; }
        public string systemName { get; set; }
        public string help { get; set; }
        public Dictionary<string, int> openCondition { get; set; }
        public List<Dictionary<string, int>> openRewards { get; set; }
    }
}
