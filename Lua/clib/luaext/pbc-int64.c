/*
The MIT License (MIT)

Copyright (c) 2015-2016 topameng(topameng@qq.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "pbc.h"

#if LUA_VERSION_NUM == 501
#define lua_rawlen lua_objlen
#endif
#define PBC_LUA_COMPAT_52
#ifdef PBC_LUA_COMPAT_52
#define INT52_MAX (0x1fffffffffffff)
#endif

typedef union number64
{
	volatile double d;
	volatile int64_t i;
} number64;

static inline void *
checkuserdata(lua_State *L, int index) {
	void * ud = lua_touserdata(L, index);
	if (ud == NULL) {
		luaL_error(L, "userdata %d is nil", index);
	}
	return ud;
}


LUALIB_API int64_t toint64(lua_Number n)
{
	int64_t v64;
#ifdef PBC_LUA_COMPAT_52
	if (n < 0)
	{
		number64 n64;
		n64.d = n;
		v64 = -n64.i;
	}
	else
	{
		v64 = (int64_t)n;
	}
#else
	number64 n64;
	n64.d = n;
	v64 = n64.i;
#endif
	return v64;
}

LUALIB_API int64_t
checkint64(lua_State *L) {
    lua_Number n = luaL_checknumber(L, 1);
    return toint64(n);
}

static int
_wmessage_int64(lua_State *L) {
	struct pbc_wmessage * m = (struct pbc_wmessage *)checkuserdata(L, 1);
	const char * key = luaL_checkstring(L, 2);
	switch (lua_type(L, 3)) {
	case LUA_TSTRING: {
		size_t len = 0;
		const char * number = lua_tolstring(L, 3, &len);
		if (len != 8) {
			return luaL_error(L, "Need an 8 length string for int64");
		}
		const uint32_t * v = (const uint32_t *)number;
		pbc_wmessage_integer(m, key, v[0], v[1]);
		break;
	}
	case LUA_TLIGHTUSERDATA: {
		void * v = lua_touserdata(L, 3);
		uint64_t v64 = (uintptr_t)v;
		pbc_wmessage_integer(m, key, (uint32_t)v64, (uint32_t)(v64 >> 32));
		break;
	}
	case LUA_TUSERDATA: {
		void * v = lua_touserdata(L, 3);
		uint64_t v64 = *(uint64_t*)v;
		pbc_wmessage_integer(m, key, (uint32_t)v64, (uint32_t)(v64 >> 32));
		break;
	}
	case LUA_TNUMBER:
	{
		lua_Number n = lua_tonumber(L, 3);
		int64_t v64 = toint64(n);
		pbc_wmessage_integer(m, key, (uint32_t)v64, (uint32_t)(v64 >> 32));
		break;
	}
	default:
		return luaL_error(L, "Need an int64 type");
	}
	return 0;
}

LUALIB_API int push_int64(lua_State *L, int64_t v64)
{
#ifdef PBC_LUA_COMPAT_52
	if (v64 <= INT52_MAX)
	{
		lua_pushnumber(L, v64);
	}
	else
#endif
	{
		number64 n64;
#ifdef PBC_LUA_COMPAT_52
		n64.i = -v64;
#else
		n64.i = v64;
#endif
		lua_pushnumber(L, n64.d);
	}
	return 1;
}

static void
push_value(lua_State *L, int type, const char * type_name, union pbc_value *v) {
	switch (type) {
	case PBC_INT:
		lua_pushinteger(L, (int)v->i.low);
		break;
	case PBC_REAL:
		lua_pushnumber(L, v->f);
		break;
	case PBC_BOOL:
		lua_pushboolean(L, v->i.low);
		break;
	case PBC_ENUM:
		lua_pushstring(L, v->e.name);
		break;
	case PBC_BYTES:
	case PBC_STRING:
		lua_pushlstring(L, (const char *)v->s.buffer, v->s.len);
		break;
	case PBC_MESSAGE:
		lua_pushvalue(L, -3);
		lua_pushstring(L, type_name);
		lua_pushlstring(L, (const char *)v->s.buffer, v->s.len);
		lua_call(L, 2, 1);
		break;
	case PBC_FIXED64:
		lua_pushlstring(L, (const char *)&(v->i), 8);
		break;
	case PBC_FIXED32:
		lua_pushlightuserdata(L, (void *)(intptr_t)v->i.low);
		break;
	case PBC_INT64: {
		int64_t v64 = (int64_t)(v->i.hi) << 32 | (int64_t)(v->i.low);
		push_int64(L, v64);
		break;
	}
	case PBC_UINT: {
		uint64_t v64 = (uint64_t)(v->i.hi) << 32 | (uint64_t)(v->i.low);
		lua_pushnumber(L, (lua_Number)v64);
		break;
	}
	default:
		luaL_error(L, "Unknown type %s", type_name);
		break;
	}
}

/*
-3 table key
-2 table id
-1 value
*/
static void
new_array(lua_State *L, int id, const char *key) {
	lua_rawgeti(L, -2, id);
	if (lua_isnil(L, -1)) {
		lua_pop(L, 1);
		lua_newtable(L);  // table.key table.id value array
		lua_pushvalue(L, -1);
		lua_pushvalue(L, -1); // table.key table.id value array array array
		lua_setfield(L, -6, key);
		lua_rawseti(L, -4, id);
	}
}
/*
-3: function decode
-2: table key
-1:	table id
*/
static void
decode_cb(void *ud, int type, const char * type_name, union pbc_value *v, int id, const char *key) {
	lua_State *L = (lua_State *)ud;
	if (key == NULL) {
		// undefined field
		return;
	}
	if (type & PBC_REPEATED) {
		push_value(L, type & ~PBC_REPEATED, type_name, v);
		new_array(L, id, key);	// func.decode table.key table.id value array
		int n = lua_rawlen(L, -1);
		lua_insert(L, -2);	// func.decode table.key table.id array value
		lua_rawseti(L, -2, n + 1);	// func.decode table.key table.id array
		lua_pop(L, 1);
	}
	else {
		push_value(L, type, type_name, v);
		lua_setfield(L, -3, key);
	}
}

/*
:1 lightuserdata env
:2 function decode_message
:3 table target
:4 string type
:5 string data
:5 lightuserdata pointer
:6 integer len

table
*/
static int
_decode(lua_State *L) {
	struct pbc_env * env = (struct pbc_env *)checkuserdata(L, 1);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	luaL_checktype(L, 3, LUA_TTABLE);
	const char * type = luaL_checkstring(L, 4);
	struct pbc_slice slice;
	if (lua_type(L, 5) == LUA_TSTRING) {
		size_t len;
		slice.buffer = (void *)luaL_checklstring(L, 5, &len);
		slice.len = (int)len;
	}
	else {
		slice.buffer = checkuserdata(L, 5);
		slice.len = luaL_checkinteger(L, 6);
	}
	lua_pushvalue(L, 2);
	lua_pushvalue(L, 3);
	lua_newtable(L);

	int n = pbc_decode(env, type, &slice, decode_cb, L);
	if (n<0) {
		lua_pushboolean(L, 0);
	}
	else {
		lua_pushboolean(L, 1);
	}
	return 1;
}


static int
int64ToString(lua_State *L) {
	lua_Number n = luaL_checknumber(L, 1);
	int64_t v64 = toint64(n);
	char s[32];
	sprintf(s, "%" PRIu64, v64);
	lua_pushstring(L, s);
	return 1;
}



bool str2long(const char *s, int64_t* result)
{
	char *endptr;
	*result = strtoll(s, &endptr, 10);

	if (endptr == s)
	{
		return false;
	}

	if (*endptr == 'x' || *endptr == 'X')
	{
		*result = (int64_t)strtoull(s, &endptr, 16);
	}

	if (*endptr == '\0')
	{
		return true;
	}

	while (isspace((unsigned char)*endptr)) endptr++;
	return *endptr == '\0';
}

//ת��һ���ַ���Ϊ int64
static int64_t _long(lua_State* L, int pos)
{
	int64_t n = 0;
	int old = errno;
	errno = 0;
	const char* str = lua_tostring(L, pos);

	if (!str2long(str, &n))
	{
		luaL_typerror(L, pos, "long");
	}

	if (errno == ERANGE)
	{
		errno = old;
		return luaL_error(L, "integral is too large: %s", str);
	}

	errno = old;
	return n;
}
static int
int64ToNumber(lua_State *L)
{
	int64_t n = _long(L, 1);
	return push_int64(L, n);
}

int
LUALIB_API luaopen_protobuf_c_ext(lua_State *L) {
	luaL_Reg reg[] = {
		{ "_wmessage_int64", _wmessage_int64 },
		{ "_decode", _decode },
		{"int64ToString", int64ToString },
		{ "int64ToNumber", int64ToNumber },
		{ NULL,NULL },
	};


#if LUA_VERSION_NUM != 501
#error "Lua version != 5.1"
#endif
	luaL_register(L, "protobuf.c.ext", reg);

	return 1;
}
