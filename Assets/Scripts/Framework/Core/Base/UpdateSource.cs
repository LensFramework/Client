﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Framework.Core
{
    public class UpdateSource:SingletonMono<UpdateSource>
    {
        public bool openOnGUI;
        public bool openOnGizmos;

        public static MonoBehaviour mono { get { return p_instance; } }
        public static event Action updateEventHandler;
        public static event Action lateUpdateEventHandler;
        public static event Action onGizmosEventHandler;
        public static event Action onGUIEventHandler;
        void Awake(){
            updateEventHandler = null;
            lateUpdateEventHandler = null;
            onGizmosEventHandler = null;
            onGUIEventHandler = null;
        }
        void Start()
        {
            hideFlags = HideFlags.HideInHierarchy;
            DontDestroyOnLoad(gameObject);
        }
        // Update is called once per frame
        void Update()
        {
            if (updateEventHandler != null)
                updateEventHandler();
        }
        void LateUpdate()
        {
            if (lateUpdateEventHandler != null)
                lateUpdateEventHandler();
        }
#if !RELEASE
        void OnGUI()
        {
            if (openOnGUI)
            {
                if (onGUIEventHandler != null)
                    onGUIEventHandler();
            }
        }
#endif

#if UNITY_EDITOR
        void OnGizmos()
        {
            if (openOnGizmos)
            {
                if (onGizmosEventHandler != null)
                    onGizmosEventHandler();
            }
        }
#endif
        void OnDestroy()
        {
            updateEventHandler = null;
            lateUpdateEventHandler = null;
            onGizmosEventHandler = null;
            onGUIEventHandler = null;
            if (p_instance != null)
            {
                p_instance = null;
            }
        }
    }
}