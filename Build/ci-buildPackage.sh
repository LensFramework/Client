#!/bin/bash -ilex
branch=$1
platform=$2
major=$3
weekly=$4
revision=$5
user=$6
ip=$7
jenkinsWorkspace=$8

productName=`awk -F "=" '/productName/ {print $2}' conf/${branch}.ini`
development=`awk -F "=" '/development/ {print $2}' conf/${branch}.ini`
remoteBuildPath="Dist/ServerData/[BuildTarget]"
remoteLoadPath="http://${ip}:8080/lensframework/versions/${major}_${branch}/[BuildTarget]"

version=${major}.${weekly}.${revision}_${branch}
config=`cat conf/${branch}.ini`
curDir=$(cd `dirname $0`; pwd)
cd ..
project_path=$(cd `dirname $0`; pwd)
rm -rf Dist/*
cd $curDir
mkdir -p ${jenkinsWorkspace}/attachments
Unity -quit -batchmode -projectPath $project_path  -logFile ${jenkinsWorkspace}/attachments/buildPackage.log -executeMethod Builder.BuildPackage outputPath=./Dist/${platform} remoteBuildPath=${remoteBuildPath} remoteLoadPath=${remoteLoadPath} platform=$platform $config
sleep 1
cd ../Dist
if [ ! -d ServerData ]; then
  echo "serverdata is dont exists"
  exit -1
fi
cd ServerData
tar zcvf ServerData.tar.gz *
cd ..

cd ${platform}/${productName}
if [ ${development} == true ]; then
  apkPath=build/outputs/apk/debug/${productName}-debug.apk
  if [ -f ${apkPath} ]; then
    rm -rf ${apkPath}
  fi
  gradle assembledebug
else
  apkPath=build/outputs/apk/release/${productName}-release.apk
  if [ -f ${apkPath} ]; then
    rm -rf ${apkPath}
  fi
  gradle assemblerelease
fi
if [ ! -f ${apkPath} ]; then
  echo "${apkPath} is dont exists"
  exit -1
fi
expect << EOF
  spawn ssh $user@$ip
  expect "*#"
  send "cd /usr/share/nginx/html/lensframework\n"
  expect "*#"
  send "mkdir -p versions/${major}_${branch}\n"
  expect "*#"
  send "mkdir -p packages\n"
  expect "*#"
  send "logout\n"
  expect eof
EOF

scp ${apkPath} $user@$ip:/usr/share/nginx/html/lensframework/packages/${version}.apk
cd ../../
scp ServerData/ServerData.tar.gz $user@$ip:/usr/share/nginx/html/lensframework/versions/${major}_${branch}

expect << EOF
  spawn ssh $user@$ip
  expect "*#"
  send "cd /usr/share/nginx/html/lensframework/versions/${major}_${branch}\n"
  expect "*#"
  send "rm -rf ${platform}\n"
  expect "*#"
  send "tar -zxvf ServerData.tar.gz\n"
  expect "*#"
  send "rm -rf ServerData.tar.gz\n"
  expect "*#"
  send "logout\n"
  expect eof
EOF

rm -rf ServerData/ServerData.tar.gz
cd $curDir
cd tools
mkdir -p ${jenkinsWorkspace}/attachments
java -jar QRCode.jar url=http://${ip}:8080/lensframework/packages/${version}.apk image=QRCode.png save=${jenkinsWorkspace}/attachments/

cd $curDir
