﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LuaReload;
using System.IO;

public class LuaReloadInternal : IInternal
{
    void IInternal.Inject()
    {
        byte[] bytes = null;
        using (FileStream fs = new FileStream(Path.Combine(Application.dataPath, "Scripts/Tools/LuaReload/Lua/reload.lua"), FileMode.Open, FileAccess.Read))
        {
            bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)fs.Length);
        }
        Lens.Framework.Managers.LuaManager.DoString(System.Text.Encoding.UTF8.GetString(bytes));
    }

    bool IInternal.IsLuaStarted()
    {
        return Lens.Framework.Managers.LuaManager.IsLuaStarted();
    }

    void IInternal.ToReload(string tableName, string funcName,string luaFolder, List<string> modifys,string luaVersion)
    {
        var f = Lens.Framework.Managers.LuaManager.GetTable(tableName);
        var t=f.GetLuaFunction(funcName);
        t.Call(luaFolder, modifys, luaVersion);
    }
}
