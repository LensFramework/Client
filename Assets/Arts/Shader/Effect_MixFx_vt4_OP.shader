﻿Shader "Mobile/Effect/MixFx_vt4_OP" {
Properties {
    [NoScaleOffset]_MainTex("MainTex", 2D) = "white" {}
    [NoScaleOffset]_MaskTex("MaskTex", 2D) = "white" {}
    [NoScaleOffset]_DistortTex("DistortTex", 2D) = "white" {}
    scaleMul("Mul", Float) = 20
    scaleAdd("Add", Float) = -10
    offsetMul("Mul", Float) = 20
    offsetAdd("Add", Float) = -10

    //[HDR]_TintColor("Main Color", Color) = (1,1,1,1)
    //_MMultiplier("Layer Multiplier", Float) = 1.0

	[HideInInspector] _IsTwoSided("_IsTwoSided", Float) = 0.0
	[HideInInspector] _CullMode("_CullingMode", Float) = 0.0
	[HideInInspector] _Mode("__mode", Float) = 0.0
	[Enum(UnityEngine.Rendering.BlendMode)]_SrcBlend("__src", Float) = 1.0
    [Enum(UnityEngine.Rendering.BlendMode)]_DstBlend("__dst", Float) = 0.0
	_ZWrite("__zw", Float) = 1.0
}

Category{

		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		Blend[_SrcBlend][_DstBlend]
		Cull[_CullMode]
		ZWrite[_ZWrite]
		Lighting Off

	SubShader {

		LOD 100

	Pass {

		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest

        #pragma multi_compile _ VERTEX_PACKED_OFF _PS_VERTEX
        #pragma shader_feature _MASK 
        #pragma shader_feature _RIMLIGHT
        #pragma shader_feature _DISTORT
        #pragma shader_feature _ALPHABLEND_ON

		#pragma exclude_renderers opengl d3d11_9x xbox360 xboxone ps3 ps4 psp2 
		#include "UnityCG.cginc"


#if defined(_PS_VERTEX)
#undef _RIMLIGHT
#undef _DISTORT
#endif

        
        inline half2 Decode_f2(float value)
        {
            half c_precision = 1024;

            half4 dec;
            dec.x = frac(value);
            dec.y = floor(value) / c_precision;

            return dec;
        }

        inline half4 Decode_f4(float2 value)
        {
            return half4(Decode_f2(value.x), Decode_f2(value.y));
        }

        float4 pack3;//_Rimlight 
        float4 pack4;//_DISTORT

#if VERTEX_PACKED_OFF
        float2 pack0;
        float4 pack1;//_MMultiplier * _TintColor
        float4 pack2;

        struct appdata_t {
            float4 vertex : POSITION;
            float2 texcoord: TEXCOORD0;
            fixed4 color : COLOR;
            half3 normal : NORMAL;
        };

#elif _PS_VERTEX

        struct appdata_t {
            float4 vertex : POSITION;
            fixed4 color : COLOR;
            float4 texcoord_pack0: TEXCOORD0;
            #if defined (_MASK)
                float4 pack1  :TEXCOORD1;
            #else
                float2 pack1  :TEXCOORD1;
            #endif
        };
#else
		struct appdata_t {
			float4 vertex : POSITION;
            fixed4 color : COLOR;
            float4 texcoord_pack0: TEXCOORD0;
            float4 pack1  :TEXCOORD1;
            float4 pack2  :TEXCOORD2;
            half3 normal : NORMAL;
        };
#endif

		struct v2f {
			float4 vertex : POSITION;
            //fixed4 color: COLOR;
            half4 colorM : TEXCOORD0;
            half4 pack0 :TEXCOORD1;
        #if defined (_MASK)
            half4 uvmain_uvsec : TEXCOORD2;
        #else
            half2 uvmain_uvsec : TEXCOORD2;
        #endif

        #if defined (_MASK)
            half4 pack1 :TEXCOORD3;
        #endif
        
        #if defined (_DISTORT) && defined (_RIMLIGHT) 
            half4 pack2 :TEXCOORD4;
            half4 distort :TEXCOORD5;
            half4 rimlight :TEXCOORD6;//_Rimlight
        #elif defined (_DISTORT)
            half4 pack2 :TEXCOORD4;
            half4 distort :TEXCOORD5;
        #elif defined (_RIMLIGHT) 
            half4 rimlight :TEXCOORD4;//_Rimlight
        #endif
        };

		sampler2D _MainTex;

#ifdef _MASK
		sampler2D _MaskTex;
#endif

#ifdef _DISTORT
        sampler2D _DistortTex;
#endif

        half scaleMul;
        half scaleAdd;
        half offsetMul;
        half offsetAdd;

		v2f vert(appdata_t v)
		{
			v2f o = (v2f)0;

			o.vertex = UnityObjectToClipPos(v.vertex);
           
    #if defined(_DISTORT) || defined(_RIMLIGHT)
            float4 p3 = pack3;
            float4 p4 = pack4;
    #endif

#if VERTEX_PACKED_OFF
            float2 uvmain = v.texcoord.xy;
            float2 p0 = pack1.xy;
            float4 p1 = float4(pack0.xy, pack1.zw);
           
            #if defined(_MASK)
                float4 p2 = pack2;
            #endif

#elif _PS_VERTEX
            float2 uvmain = v.texcoord_pack0.xy;
            float2 p0 = v.pack1.xy;
            float2 p1 = v.texcoord_pack0.zw;

            #if defined(_MASK)
            float2 p2 = v.pack1.zw;
            #endif

#else
            float2 uvmain = v.texcoord_pack0.xy;
            float2 p0 = v.pack1.xy;
            float4 p1 = float4(v.texcoord_pack0.zw, v.pack1.zw);

            #if defined(_MASK)
                float4 p2 = v.pack2;
            #endif
#endif

            fixed4 color = v.color;
            o.colorM = Decode_f4(abs(p0.xy)) * 10;
#if defined(_ALPHABLEND_ON)
            o.colorM = color * o.colorM;
#else
            o.colorM.rgb = color.rgb * o.colorM.rgb;
#endif
            half4 scaleOffsetMul = half4(scaleMul, scaleMul, offsetMul, offsetMul);
            half4 scaleOffsetAdd = half4(scaleAdd, scaleAdd, offsetAdd, offsetAdd);

            o.pack0 = half4(sign(p0.x) * Decode_f2(p1.x), Decode_f2(p1.y));

        #if defined (_PS_VERTEX)
            o.uvmain_uvsec.xy = uvmain;
            #if defined (_MASK)
                o.uvmain_uvsec.zw = uvmain;
            #endif
        #else   
            half4 _MainTex_ST = half4(Decode_f2(p1.z), Decode_f2(p1.w)) * scaleOffsetMul + scaleOffsetAdd;
            o.uvmain_uvsec.xy = uvmain.xy * _MainTex_ST.xy + _MainTex_ST.zw;
            #if defined (_MASK)
                o.pack1 = half4(sign(p0.y) * Decode_f2(p2.x), Decode_f2(p2.y));
                half4 _SecTex_ST = half4(Decode_f2(p2.z), Decode_f2(p2.w)) * scaleOffsetMul + scaleOffsetAdd;
                o.uvmain_uvsec.zw = uvmain.xy * _SecTex_ST.xy + _SecTex_ST.zw;
            #endif
        #endif

        #if defined (_DISTORT)
            o.pack2 = half4(Decode_f2(p4.x), Decode_f2(p4.y));
            o.distort.xyz = half3(Decode_f2(p4.z), p4.w);
        #endif
    
        #if defined (_RIMLIGHT)
            float4 worldPos = mul(unity_ObjectToWorld, v.vertex);

            fixed4 _RimColor = Decode_f4(p3.xy);
            half2 r2 = Decode_f2(p3.z) * 10;
            half _RimFalloff = r2.x;
            half _RimPower = r2.y;
            half _AlphaPower = p3.w;

            half3 viewDirection = normalize(UnityWorldSpaceViewDir(worldPos));
            half3 normalection = UnityObjectToWorldNormal(v.normal);
            half rim = 1.0 - saturate(dot(viewDirection, normalection));
            o.rimlight.rgb = _RimColor.rgb * (pow(rim, _RimPower)* _RimFalloff);
            o.rimlight.a = pow(rim, _AlphaPower) * _RimFalloff;
        #endif
			return o;
		}

        half when_zero_gt(half x)
        {
            return step(0, x);
        }
       
        half2 get_frac(half2 xy)
        {
            return xy - floor(xy);
        }

		fixed4 frag(v2f i) : COLOR
		{
            fixed4 col = 1;
            //return i.colorM;

#if defined(_DISTORT)
            half _ForceX = i.distort.x;
            half _ForceY = i.distort.y;
            half _HeatTime = i.distort.z;

            half2 uv0 = i.uvmain_uvsec.xy +_Time.xz*_HeatTime;
            uv0 = get_frac(uv0.xy) * abs(i.pack2.xy) + i.pack2.zw;
            half2 uv1 = i.uvmain_uvsec.xy +_Time.yx*_HeatTime;
            uv1 = get_frac(uv1.xy) * abs(i.pack2.xy) + i.pack2.zw;

            fixed4 offsetColor1 = tex2D(_DistortTex, uv0.xy);
            fixed4 offsetColor2 = tex2D(_DistortTex, uv1.xy);
            i.uvmain_uvsec.x += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceX;
            i.uvmain_uvsec.y += ((offsetColor1.r + offsetColor2.r) - 1) * _ForceY;
#endif

            half2 uvmain = lerp(saturate(i.uvmain_uvsec.xy), get_frac(i.uvmain_uvsec.xy), when_zero_gt(i.pack0.x));
            uvmain = uvmain * abs(i.pack0.xy) + i.pack0.zw;

            fixed4 col2 = tex2D(_MainTex, uvmain.xy);
            //return col2;

#if defined(_ALPHABLEND_ON)
            col2 *= i.colorM;
            col2.a = saturate(col2.a);
#else
            col2.rgb *= i.colorM.rgb;
#endif
            
            col = col2;

#if defined (_RIMLIGHT)
            col.rgb += i.rimlight.rgb;
            col.a = i.rimlight.a;
            //return i.rimlight;
#endif

#if defined (_MASK)
            half2 uvsec = lerp(saturate(i.uvmain_uvsec.zw), get_frac(i.uvmain_uvsec.zw), when_zero_gt(i.pack1.x));
            uvsec = uvsec * abs(i.pack1.xy) + i.pack1.zw;

            fixed4 cMask = tex2D(_MaskTex, uvsec.xy);
            //return cMask;
			col *= cMask;
#endif
			return col;
		}
		ENDCG
		}
		}
	}

	Fallback Off
	CustomEditor "MixVt4OPEffectEditor"
}
