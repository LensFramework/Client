﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace Lens
{
    using Framework.Core;
    using Framework.Config;
    using Framework.Managers;
    using Framework.Console;
    using System.Threading.Tasks;

    public class GameStart : MonoBehaviour
    {
        #region Listener class
        private class NetListener:INetListener{
            ProtobufSerializer m_serialize=new ProtobufSerializer();
            object INetListener.WrapUnpack(int msgId,byte[] bytes){
                if(msgId>0 && msgId<10000)
                {
                    object ob=null;
                    using(MemoryStream memoryStream=new MemoryStream(bytes))
                    {
                        ob= m_serialize.Deserialize(memoryStream,null,typeof(object));
                    }
                    return ob;
                }
                else if(msgId>10000)
                {
                    return new LuaInterface.LuaByteBuffer(bytes);
                }
                else
                {
                    return System.Text.Encoding.UTF8.GetString(bytes);
                }
            }
            byte[] INetListener.Encrypt(byte[] bytes){
                return bytes;
            }
            byte[] INetListener.Decrypt(byte[] bytes){
                return bytes;
            }
        }
        #endregion

        #region public member
        public bool openOnGUI;
        public bool openOnGizmos;
        public string luaMainFunc = "Main";
        #endregion

        #region private member
        LuaManager m_luaManager;
        TcpManager m_tcpanager;
        SecurityManager m_securityManager;
        QualityManager m_qualityManager;
        Config m_config;
        Log m_log;
        UpdateSource m_updateSource;
        Gameplay.Modules.Update.UpdateProxy m_updateProxy;
        #endregion


        #region unity methods
        void Awake()
        {
            m_updateSource = UpdateSource.GetInstance("UpdateSource");
            UpdateSource.updateEventHandler += OnUpdate;
        }
        async void Start()
        {
            m_log = Log.GetInstance();
            await m_log.Initialize(new Log.MyOptions()
            {
                uploadUrl="http://",
                logPath=Path.Combine(Application.persistentDataPath,"log.txt")
            });
            Log.SetLogs(new Dictionary<string, string>(){
                {"Info","#FFFFFF"},
                {"main","#859289"},
                {"store:global","#988DC2"},
                {"L_UI","#459454"},
                {"page:login","#E66933"},
                {"page:selectserver","#430AF5"},
                {"page:main","#4CF38D"},
                {"page:heroinfo","#2ECF6C"},
                {"page:package","#65CE60"},
            });
#if RELEASE
            Log.StartThreadSave();
#endif
            // 调试模式 || 发布模式
#if DEBUGGER || RELEASE
#endif
            m_updateProxy = GameObject.FindObjectOfType<Gameplay.Modules.Update.UpdateProxy>();
            if (m_updateProxy != null)
            {
                await m_updateProxy.Initialize();
            }
            await OnUpdateComplete();
        }
        void OnUpdate()
        {
            if (m_tcpanager != null)
            {
                m_tcpanager.Update();
            }
            if (m_luaManager != null)
            {
                m_luaManager.Update();
            }
        }
        void OnDestroy()
        {
            //m_securityManager.Dispose();
            m_tcpanager.Dispose();
            m_luaManager.Dispose();
            //m_qualityManager.Dispose();
        }
        /// <summary>
        /// 前后台切换判断
        /// </summary>
        /// <param name="focus"></param>
        void OnApplicationFocus(bool focus)
        {
            //if (m_networkManager!=null)
            //{
            //    m_networkManager.Reconnect();
            //}
        }
        /// <summary>
        /// 前后台切换判断
        /// </summary>
        /// <param name="focus"></param>
        void OnApplicationPause(bool pause)
        {
            //if (m_networkManager!=null)
            //{
            //    m_networkManager.Reconnect();
            //}
        }
        #endregion

        #region private methods
        private async Task OnUpdateComplete()
        {
            m_config = Config.GetInstance();
            m_config.loadAssetHandler =async delegate(string assetName)
             {
                 return await UnityEngine.AddressableAssets.Addressables.LoadAssetAsync<UnityEngine.Object>(assetName).Task;
             };
            await m_config.Initialize(new Config.MyOptions()
            {
                memConfigType=typeof(Gameplay.Config.MemConfig),
                tableConfigType=typeof(Gameplay.Config.TableConfig)
            });
            //m_securityManager = SecurityManager.GetInstance();
            m_tcpanager = TcpManager.GetInstance();
            m_tcpanager.AddListener(new NetListener());
            await m_tcpanager.Initialize();
            //m_tcpanager.AddLuaSupport(new NetLuaSupport());
            //var publicKey=SecurityManager.GetDiffieHellmanKey();
            //SecurityManager.SetKey(publicKey);// "AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQE=");
            //m_qualityManager = QualityManager.GetInstance();
            //m_qualityManager.loadAssetHandler = delegate (string path)
            //{
            //    return ResourcesManager.LoadAsset(path);
            //};
            //await m_qualityManager.Initialize();
            m_luaManager = LuaManager.GetInstance();
            await m_luaManager.Initialize();
            m_luaManager.Start(luaMainFunc);//启动lua后，就加载登陆界面
            m_updateSource.gameObject.AddComponent<LuaLooper>().luaState = LuaManager.mainState;
            Log.Info("GameStart", "Info");
            if (m_updateProxy!=null)
            {
                UnityEngine.Object.Destroy(m_updateProxy.gameObject);
            }
        }
        #endregion
    }

}