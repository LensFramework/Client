﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Gameplay.Config
{
    using Core;
    using Framework.Config;
    public class MemConfig:IMemConfig
    {
        /// <summary>
        /// 当前关卡
        /// </summary>
        public ESceneType sceneType { get;private set; }

        public void SetSceneType(int type)
        {
            sceneType = (ESceneType)type;
        }
    }
}