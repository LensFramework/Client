﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Managers
{
    public enum ENetState
    {
        Connected,
        Connecting,
        Disconnected,
        ReconnectFailure,
        Reconnecting,
        Reconnected
    }
}
