﻿/***************************************************************
 * Description: 
 *
 * Documents: https://github.com/hiramtan/HiSocket
 * Author: hiramtan@live.com
***************************************************************/

namespace Lens.Framework.Managers
{
    public interface INetPlugin
    {

        /// <summary>
        /// Plugin belong to this tcpConnection
        /// </summary>
        TcpSocket TcpSocket { get; set; }
        void Initialize();
        void Update();
    }
}
