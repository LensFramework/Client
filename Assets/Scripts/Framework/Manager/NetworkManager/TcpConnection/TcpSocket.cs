﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Lens.Framework.Managers
{
    public class TcpSocket
    {
#if UNITY_IPHONE
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern string IPv6_getIpAddr(string ip, string port);
#endif
        /// <summary>
        /// Get socket and modify it(for example: set timeout)
        /// </summary>
        public System.Net.Sockets.Socket Socket { get; private set; }
        public IPEndPoint IPPoint { get; private set; }
        /// <summary>
        /// if connected(should use heart beat check if server disconnect)
        /// </summary>
        public bool IsConnected
        {
            get { return Socket != null && Socket.Connected; }
        }
        /// <summary>
        /// connected count
        /// </summary>
        public int ConnectedCount {get;private set;}

        /// <summary>
        /// trigger when connecting
        /// </summary>
        public event Action connectingHandler;

        /// <summary>
        /// trigger when connected
        /// </summary>
        public event Action connectedHandler;

        /// <summary>
        /// Trigger when disconnected
        /// </summary>
        public event Action disconnectedHandler;
        /// <summary>
        /// trigger connect failure
        /// </summary>
        public event Action<string> connectFailureHandler;
        /// <summary>
        /// trigger send failure
        /// </summary>
        public event Action<string> sendFailureHandler;
        /// <summary>
        /// triger receive failure
        /// </summary>
        public event Action<string> receiveFailureHandler;
        /// <summary>
        /// trigger when get message from server, it havent unpacked
        /// use .net socket api
        /// </summary>
        public event Action<byte[]> receivedHandler;

        /// <summary>
        /// trigger when send message to server, it already packed
        /// use .net socket api
        /// </summary>
        public event Action<byte[]> sendedHandler;

        /// <summary>
        /// Send buffer
        /// If disconnect, user can operate the remain data
        /// </summary>
        public IBlockBuffer<byte> SendBuffer { get; private set; }
        /// <summary>
        /// Receive buffer
        /// </summary>
        public IBlockBuffer<byte> ReceiveBuffer { get; private set; }
        public Queue<byte[]> unPackedBytes=new Queue<byte[]>();
        public NetMessage message {get;private set;}
        private readonly object m_locker = new object();
        private Queue<byte[]> m_waitSendQueue = new Queue<byte[]>();
        private Dictionary<int, INetPlugin> m_plugins = new Dictionary<int, INetPlugin>();
        private INetListener m_listener;
        private bool m_isRawUnPack;
        /// <summary>
        /// The default buffer is 1<<16, if small will automatically add buffer block
        /// </summary>
        /// <param name="bufferSize"></param>
        public TcpSocket(INetListener listener,int bufferSize = 1 << 16)
        {
            m_listener=listener;
            SendBuffer = new BlockBuffer<byte>(bufferSize);
            ReceiveBuffer = new BlockBuffer<byte>(bufferSize);
            message=new NetMessage(listener);
        }
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            lock (m_locker)
            {
                Disconnect();
                Socket = null;
                connectingHandler = null;
                connectedHandler = null;
                disconnectedHandler = null;
                receivedHandler = null;
                sendedHandler = null;
                SendBuffer.Dispose();
                ReceiveBuffer.Dispose();
            }
        }
        public void Update()
        {
            foreach (var item in m_plugins)
            {
                item.Value.Update();
            }
            message.Update();
            if (IsConnected)
            {
                while (m_waitSendQueue.Count > 0)
                {
                    var s = m_waitSendQueue.Dequeue();
                    Send(s);
                }
            }
        }
        public void SetRawUnpack(bool rawUnpack)
        {
            m_isRawUnPack=rawUnpack;
        }
        public void Send(int msgId, byte[] bytes)
        {
            Send(Pack(msgId, bytes));
        }
        /// <summary>
        /// Add plugin to extend logic
        /// </summary>
        /// <param name="plugin"></param>
        public void AddPlugin(ENetPluginType pluginType, INetPlugin plugin)
        {
            plugin.TcpSocket = this;
            plugin.Initialize();
            m_plugins.Add((int)pluginType, plugin);
        }
        /// <summary>
        /// Get plugin by name
        /// </summary>
        /// <param name="name">plugin's name</param>
        /// <returns>plugin</returns>
        public INetPlugin GetPlugin(ENetPluginType pluginType)
        {
            return m_plugins[(int)pluginType];
        }
        public void ClearCache()
        {
            m_waitSendQueue.Clear();
        }
        public void Reconnect()
        {
            if (IPPoint!=null && !IsConnected)
            {
                lock (m_locker)
                {
                    if (IsConnected)
                    {
                        try
                        {
                            Socket.Shutdown(SocketShutdown.Both);
                            Socket.Close();
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.ToString());
                        }
                    }
                }
                Connect(IPPoint);
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        public void Connect(IPEndPoint iep)
        {
            IPPoint = iep;
            lock (m_locker)
            {
                if(IsConnected){
                    UnityEngine.Debug.LogError("Already Connected");
                    return;
                }
                if(iep==null){
                    UnityEngine.Debug.LogError("iep is null");
                    return;
                }
                if (connectingHandler != null)
                {
                    connectingHandler();
                }
                try
                {
                    AddressFamily addressFamily = iep.AddressFamily;
#if UNITY_IPHONE
                    if (Application.platform == RuntimePlatform.IPhonePlayer)
                    {
                        string ipv6 = IPv6_getIpAddr(IPPoint.Address.ToString(), IPPoint.Port.ToString());
                        Debug.Log("IPV6 := " + ipv6);
                        if (!string.IsNullOrEmpty(ipv6))
                        {
                            addressFamily = AddressFamily.InterNetworkV6;
                        }
                    }
#endif
                    Socket = new System.Net.Sockets.Socket(addressFamily, SocketType.Stream, ProtocolType.Tcp);
                    ConnectedCount++;

                }
                catch (Exception e)
                {
                    if (connectFailureHandler != null)
                    {
                        connectFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
                try
                {
                    Socket.BeginConnect(iep, delegate (IAsyncResult ar)
                    {
                        try
                        {
                            var socket = ar.AsyncState as System.Net.Sockets.Socket;
                            if(socket==null){
                                UnityEngine.Debug.LogError("Socket is null when end connect");
                            }
                            socket.EndConnect(ar);
                            if (IsConnected)
                            {
                                if (connectedHandler != null)
                                {
                                    connectedHandler();
                                }
                                ToReceive();
                            }
                            else
                            {
                                if (connectFailureHandler!=null)
                                {
                                    connectFailureHandler("Connect faild");
                                }
                                UnityEngine.Debug.LogError("Connect faild");
                            }
                        }
                        catch (Exception e)
                        {
                            if (connectFailureHandler != null)
                            {
                                connectFailureHandler(e.ToString());
                            }
                            throw new Exception(e.ToString());
                        }

                    }, Socket);
                }
                catch (Exception e)
                {
                    if (connectFailureHandler != null)
                    {
                        connectFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        /// <param name="ip">ipv4/ipv6</param>
        /// <param name="port"></param>
        public void Connect(string ip, int port)
        {
            lock (m_locker)
            {
                if(string.IsNullOrEmpty(ip))
                {
                    UnityEngine.Debug.LogError("ip is null or empty");
                    return;
                }
                var iep = new IPEndPoint(IPAddress.Parse(ip), port);
                Connect(iep);
            }
        }
        /// <summary>
        /// Connect to server
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public void Connect(IPAddress ip, int port)
        {
            lock (m_locker)
            {
                if(ip==null){
                    UnityEngine.Debug.LogError("ip is null");
                }
                var iep = new IPEndPoint(ip, port);
                Connect(iep);
            }
        }
        public void Disconnect()
        {
            lock (m_locker)
            {
                if (IsConnected)
                {
                    ConnectedCount = 0;
                    IPPoint = null;
                    try
                    {
                        if (disconnectedHandler != null)
                        {
                            disconnectedHandler();
                        }
                    }
                    catch { }
                    try
                    {
                        Socket.Shutdown(SocketShutdown.Both);
                        Socket.Close();
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.ToString());
                    }
                }
            }
        }
        /// <summary>
        /// Send bytes to server
        /// </summary>
        /// <param name="bytes"></param>
        private void Send(byte[] bytes)
        {
            if (IsConnected)
            {
                lock (SendBuffer)
                {
                    int index = 0;
                    int length = bytes.Length;
                    if(!IsConnected){
                        UnityEngine.Debug.LogError("From send : disconnected");
                    }
                    SendBuffer.Write(bytes, index, length);
                    try
                    {
                        Socket.BeginSend(m_listener.Encrypt(bytes), index, length, SocketFlags.None, EndSend, Socket);
                    }
                    catch (Exception e)
                    {
                        if (sendFailureHandler!=null)
                        {
                            sendFailureHandler(e.ToString());
                        }
                        m_waitSendQueue.Enqueue(bytes);
                        throw new Exception(e.ToString());
                    }
                }
            }
            else
            {
                m_waitSendQueue.Enqueue(bytes);
            }
        }
        private void EndSend(IAsyncResult ar)
        {
            lock (SendBuffer)
            {
                if (IsConnected)//User disconnect tcpConnection proactively
                {
                    int length = 0;
                    try
                    {
                        var socket = ar.AsyncState as System.Net.Sockets.Socket;
                        if(socket==null){
                            UnityEngine.Debug.LogError("Socket is null when end send");
                        }
                        length = socket.EndSend(ar);
                    }
                    catch (Exception e)
                    {
                        if (sendFailureHandler != null)
                        {
                            sendFailureHandler(e.ToString());
                        }
                        throw new Exception(e.ToString());
                    }
                    byte[] sendBytes = SendBuffer.Read(length);
                    SendBuffer.ResetIndex();
                    if (sendedHandler != null)
                    {
                        sendedHandler(sendBytes);
                    }
                }
            }
        }
        private void ToReceive()
        {
            lock (ReceiveBuffer)
            {
                try
                {
                    var count = ReceiveBuffer.Size - ReceiveBuffer.WritePosition;
                    Socket.BeginReceive(ReceiveBuffer.Buffer, ReceiveBuffer.WritePosition, count, SocketFlags.None,
                        EndReceive, Socket);
                }
                catch (Exception e)
                {
                    if (receiveFailureHandler != null)
                    {
                        receiveFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
            }
        }
        private void EndReceive(IAsyncResult ar)
        {
            lock (ReceiveBuffer)
            {
                int length = 0;
                try
                {
                    var socket = ar.AsyncState as System.Net.Sockets.Socket;
                    if(socket==null){
                        UnityEngine.Debug.LogError("Socket is null when end receive");
                    }
                    length = socket.EndReceive(ar);
                }
                catch (Exception e)
                {
                    if (receiveFailureHandler != null)
                    {
                        receiveFailureHandler(e.ToString());
                    }
                    throw new Exception(e.ToString());
                }
                //UnityEngine.Debug.Log("ReceiveBuffer:"+ length+"   " + ReceiveBuffer.WritePosition + "   " + ReceiveBuffer.ReadPosition);
                int curLength = 0;
                while (curLength < length)
                {
                    try
                    {
                        ushort msgId=0;
                        // UnityEngine.Debug.Log(msgId+"   "+curLength + "   "+ bodyLength+"   "+ ReceiveBuffer.WritePosition + "   " + ReceiveBuffer.ReadPosition);
                        int unpackLength=0;
                        if (m_isRawUnPack)
                        {
                            var b=ReceiveBuffer.Read(length);
                            if (unPackedBytes.Count>100){
                                unPackedBytes.Dequeue();
                            }
                            unPackedBytes.Enqueue(b);
                            curLength=length;
                            if (receivedHandler != null)
                            {
                                receivedHandler(b);
                            }
                        }
                        else {
                            var result = Unpack(ReceiveBuffer,ref msgId,ref unpackLength);
                            curLength+=unpackLength;
                            if (receivedHandler != null)
                            {
                                receivedHandler(result);
                            }
                            message.ToQueue(msgId, result);
                        }
                    }
                    catch (Exception exc)
                    {
                        if (receiveFailureHandler != null)
                        {
                            receiveFailureHandler(exc.ToString());
                        }
                        curLength=length;
                    }
                }
                ReceiveBuffer.ResetIndex();
                // UnityEngine.Debug.Log("ResetIndex:" + ReceiveBuffer.WritePosition+"   "+ ReceiveBuffer.ReadPosition);
                if (length > 0)
                {
                    ToReceive();
                }
                else
                {
                    if (disconnectedHandler != null)
                    {
                        disconnectedHandler();
                    }
                }
            }
        }

        /// <summary>
        /// Unpack
        /// </summary>
        /// <param name="source"></param>
        /// <param name="onUnpacked"></param>
        private byte[] Unpack(IBlockBuffer<byte> buffer,ref ushort msgId,ref int length)
        {
            msgId = EndianBitConverter.GetBigEndian(BitConverter.ToUInt16(buffer.Read(2), 0));
            if(msgId>=ushort.MaxValue)
            {
                UnityEngine.Debug.LogError("msdId is greater than ushor.MaxValue:   "+msgId);
                return null;
            }
            var bodyLength = EndianBitConverter.GetBigEndian(BitConverter.ToUInt16(buffer.Read(2), 0));
            if(bodyLength>=ushort.MaxValue){
                UnityEngine.Debug.LogError("Unpack: need to uncompress ");
                return null;
            }
            else{
                length += bodyLength + 2+2;
                var data = buffer.Read(bodyLength);
                buffer.ResetIndex();
                UnityEngine.Debug.Log("Unpack: " +msgId+"  "+ buffer.ReadPosition + "  " + buffer.Buffer.Length);
                return m_listener.Decrypt(data);
            }
        }

        /// <summary>
        /// Pack
        /// </summary>
        /// <param name="source"></param>
        /// <param name="onPacked"></param>
        private byte[] Pack(int msgId,byte[] source)
        {
            if(msgId>0)
            {
                using (BlockBuffer<byte> buffer = new BlockBuffer<byte>(source))
                {
                    //Use int as header
                    int length = buffer.WritePosition;
                    var msgIdByte= BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)msgId));
                    var lengthByte = BitConverter.GetBytes(EndianBitConverter.GetBigEndian((ushort)length));
                    var newBytes = new BlockBuffer<byte>(msgIdByte.Length+length + lengthByte.Length);
                    //Write header and body to buffer
                    newBytes.Write(msgIdByte);
                    newBytes.Write(lengthByte);
                    newBytes.Write(buffer.Buffer);
                    UnityEngine.Debug.Log("Pack" + length + "   " + lengthByte.Length + "   " + newBytes.Buffer.Length);
                    //Notice pack funished
                    return newBytes.Buffer;
                }
            }
            else 
            {
                return source;
            }
        }
    }
}