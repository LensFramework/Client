﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class UEDevicesGrading
{
    public enum DeviceGrade
    {     
        NONE = -1,
        VERY_HIGH = 0,
        HIGH,
        MEDIUM,
        LOW,
    }

    public const string KEY = "UEScalability_QualityLevel";

    private UEMoblieCPUConfig m_cpuConfig;
    private DeviceGrade m_grade = DeviceGrade.NONE;    

    public delegate void GradeChange(DeviceGrade grade);

    public event GradeChange OnGradeChange;

    public delegate void GradeLog(object o);

    public GradeLog m_logFunction;

    public bool m_cacheGrading = false;
    public string DeviceInfo
    {
        get;
        private set;
    }
    public DeviceGrade DeviceGradeInfo
    {
        get { return m_grade; }
    }   
    public void Init(ScriptableObject config, GradeChange changeFunc,GradeLog logFunc,bool cacheGrading = false)
    {
        m_cacheGrading = cacheGrading;
        m_logFunction = logFunc != null ? logFunc : Debug.Log;
        if(changeFunc == null)
        {
            Log("Scalability: should specify changeFunc!");
            return;
        }
        OnGradeChange += changeFunc;
        if (config == null)
        {
            Log("Scalability: should specify config!");
            return;
        }
        m_cpuConfig = config as UEMoblieCPUConfig;
        if(m_cpuConfig == null)
        {
            Log("Scalability: should specify the config of UEMoblieCPUConfig!");
            return;
        }
        if (CheckPlayerPrefs())
        {
            Log("Scalability: set quality from player prefs, " + m_grade);            
        }
        else if (CheckDeviceGrade())
        {
            Log("Scalability: set quality from device grade, " + m_grade);            
        }
        else if (CheckDeviceGradeByNormal())
        {
            Log("Scalability: set quality from device hardware, " + m_grade);            
        }
        else
        {
            m_grade = DeviceGrade.LOW;
        }
        NotifyQuality();
    }
    private bool CheckPlayerPrefs()
    {
        if (m_cacheGrading == false)
            return false;
        if (!PlayerPrefs.HasKey(KEY))
        {
            return false;
        }
        m_grade = (DeviceGrade)PlayerPrefs.GetInt(KEY);
        return true;
    }

    private void Log(object o)
    {
        if (m_logFunction != null)
        {
            m_logFunction(o);
        }
    }

    private void NotifyQuality()
    {
        if(m_grade != DeviceGrade.NONE && m_cacheGrading == true)
            PlayerPrefs.SetInt(KEY, (int)m_grade);
        if (OnGradeChange != null)
        {
            OnGradeChange(m_grade);
        }
    }
#if UNITY_ANDROID && !UNITY_EDITOR
    DeviceGrade CheckDeviceGradeByRank(string info)
    {
        DeviceInfo = info;
        var result = Regex.Match(info, "(\\d+)");
        if (result.Success)
        {
            var id = result.Groups[0].ToString();
            var rank = Int32.MaxValue;
            if (info.Contains("qualcomm") || info.Contains("qcom"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["qualcomm"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                    else if(vendorCpu.m_name.Contains("x"))
                    {
                        var ret = Regex.Match(vendorCpu.m_name, "(\\d+)x(\\d+)(\\w+)");
                        if (ret.Success && ret.Groups.Count == 4)
                        {
                            var comp1 = ret.Groups[1].ToString();
                            var comp2 = ret.Groups[2].ToString();
                            var comp3 = ret.Groups[3].ToString();
                            if (info.Contains(comp1) && info.Contains(comp2) && info.Contains(comp3))
                            {
                                rank = vendorCpu.m_rank;
                            }
                        }
                    }
                }
            }
            else if (info.Contains("samsung"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["samsung"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                }
            }
            else if (info.Contains("hisilicon") || info.Contains("kirin"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["hisilicon"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                }
            }
            else if (info.Contains("intel"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["intel"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                }
            }
            else if (info.Contains("nvida"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["nvida"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                }
            }
            else if (info.Contains("mediatex"))
            {
                foreach (var vendorCpu in m_cpuConfig.m_dicvendor["mediatex"])
                {
                    if (vendorCpu.m_name.Contains(id))
                        rank = vendorCpu.m_rank;
                }
            }

            if (rank != Int32.MaxValue)
            {
                if (rank <= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.VERY_HIGH].androidCpuRank)
                {
                    return DeviceGrade.VERY_HIGH;
                }
                else if (rank <= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.HIGH].androidCpuRank)
                {
                    return DeviceGrade.HIGH;
                }
                else if (rank <= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.MEDIUM].androidCpuRank)
                {
                    return DeviceGrade.MEDIUM;
                }
                else
                {
                    return DeviceGrade.LOW;
                }
            }
        }
        Log(string.Format("can't find cpu grade {0}", info));
//        GUILayout.Label(string.Format("can't find cpu grade {0}", info));
        return DeviceGrade.NONE;
    }    
#elif UNITY_IOS && !UNITY_EDITOR
    DeviceGrade CheckDeviceGradeByRank()
    {
        var currentDevice = UnityEngine.iOS.Device.generation;
        DeviceInfo = currentDevice.ToString();
        if (currentDevice >= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.VERY_HIGH].iosDevice)
        {
            return DeviceGrade.VERY_HIGH;
        }
        else if (currentDevice >= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.HIGH].iosDevice)
        {
            return DeviceGrade.HIGH;
        }
        else if (currentDevice >= m_cpuConfig.m_settings.m_Ranks[(int)DeviceGrade.MEDIUM].iosDevice)
        {
            return DeviceGrade.MEDIUM;
        }
        else
        {
            return DeviceGrade.LOW;
        }
    }
#endif


    bool CheckDeviceGradeByNormal()
    {
        m_grade = DeviceGrade.NONE;

        var coreCount = SystemInfo.processorCount;
        var frequency = SystemInfo.processorFrequency;
        var memorySize = SystemInfo.graphicsMemorySize;

        for(int i = 0;i < m_cpuConfig.m_settings.m_Ranks.Length;i++)
        {
            if (coreCount >= m_cpuConfig.m_settings.m_Ranks[i].coreCount
                && frequency >= m_cpuConfig.m_settings.m_Ranks[i].frequency
                && memorySize >= m_cpuConfig.m_settings.m_Ranks[i].memorySize)
            {
                m_grade = (DeviceGrade)i;
                break;
            }
        }        
        return m_grade != DeviceGrade.NONE;
    }

    private string GetDeviceFromJni()
    {
        var osBuild = AndroidJNI.FindClass("android/os/Build");
        var hardWareField = AndroidJNIHelper.GetFieldID<string>(osBuild, "HARDWARE", true);
        var boardField = AndroidJNIHelper.GetFieldID<string>(osBuild, "BOARD", true);
        return AndroidJNI.GetStaticStringField(osBuild, hardWareField) + AndroidJNI.GetStaticStringField(osBuild, boardField);
    }

    bool CheckDeviceGrade()
    {
        m_cpuConfig.Init();
        m_grade = DeviceGrade.NONE;
#if UNITY_ANDROID && !UNITY_EDITOR        
        using (var f = File.OpenRead("/proc/cpuinfo"))
        {
            if (f.CanRead)
            {
                using (StreamReader sr = new StreamReader(f))
                {
                    while (!sr.EndOfStream)
                    {
                        var info = sr.ReadLine();
                        if (info != null && info.Contains("Hardware"))
                        {
                            m_grade = CheckDeviceGradeByRank(info.ToLower());
                            Log(string.Format("CheckDeviceGradeByRank: {0}", m_grade.ToString()));
//                            GUILayout.Label(string.Format("CheckDeviceGradeByRank: {0}", ret.ToString()));
                        }
                    }
                }
                f.Close();
            }
        }
        if(m_grade == DeviceGrade.NONE)
        {
            m_grade = CheckDeviceGradeByRank(GetDeviceFromJni().ToLower());
            Log(string.Format("CheckDeviceGradeByRank: {0}", m_grade.ToString()));
        }        
#elif UNITY_IOS && !UNITY_EDITOR
        m_grade = CheckDeviceGradeByRank(); 
#else
        m_grade = DeviceGrade.VERY_HIGH;
#endif
        return m_grade != DeviceGrade.NONE;
    }
}
