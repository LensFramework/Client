﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Lens.Tools.TempMenu
{
    public class TempMenu
    {
        [MenuItem("Tools/TempMenu/ReladLua")]
        public static void CReloadLua()
        {
            //var f=Framework.Managers.LuaManager.GetTable<Framework.Managers.L_Delegate_S>("toReload");
            //f(Path.Combine(Application.dataPath,"../Lua/src/view/pages/login"));
            var setting = AssetDatabase.LoadAssetAtPath<UnityEditor.AddressableAssets.Settings.AddressableAssetSettings>("Assets/AddressableAssetsData/AddressableAssetSettings.asset");
            var dataBuilder= AssetDatabase.LoadAssetAtPath<UnityEditor.AddressableAssets.Build.DataBuilders.BuildScriptCustomMode>("Assets/AddressableAssetsData/DataBuilders/BuildScriptCustomMode.asset");
            setting.AddDataBuilder(dataBuilder);
        }
    }
}