#!/bin/bash

platform=$1

curDir=$(cd `dirname $0`; pwd)
cd ..
project_path=$(cd `dirname $0`; pwd)
cd $curDir

Unity -quit -batchmode -projectPath $project_path  -logFile logs/switchTarget.log -buildTarget ${platform}
