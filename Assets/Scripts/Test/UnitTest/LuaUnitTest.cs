﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.UnitTest
{
    using Framework.Managers;
    public class LuaUnitTest : MonoBehaviour
    {
        public string mainLua="main";
        LuaManager m_luaManager;
        // Use this for initialization
        void Start()
        {
            m_luaManager= LuaManager.GetInstance();
            //m_luaManager.Initialize(new LuaManager.MyOptions(){
            //    luaUrl=Gameplay.Core.Define.luaUrl
            //});
            //m_luaManager.Preload();
            m_luaManager.Start(mainLua);
        }

        // Update is called once per frame
        void Update()
        {
            m_luaManager.Update();
        }
        void OnDestroy()
        {
            m_luaManager.Dispose();
        }
    }
}