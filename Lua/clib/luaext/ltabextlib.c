/*
** $Id: ltablib.c,v 1.38.1.3 2008/02/14 16:46:58 roberto Exp $
** Library for Table Manipulation
** See Copyright Notice in lua.h
*/


#include <stddef.h>

#include "lua.h"

#include "lauxlib.h"
#include "luaextlib.h"


#define aux_getn(L,n)	(luaL_checktype(L, n, LUA_TTABLE), luaL_getn(L, n))

static int count(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	int count = 0;
	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pop(L, 1);  /* remove value */
		count++;
	}
	lua_pushinteger(L, count);
	return 1;
}

static int values(lua_State *L)
{
	luaL_checktype(L, 1, LUA_TTABLE);
	lua_newtable(L);
	lua_pushnil(L);  /* first key */
	int i = 1;
	while (lua_next(L, 1)) {
		lua_pushvalue(L, -1);  /* value */
		lua_rawseti(L, 2, i++);
		lua_pop(L, 1);  /* remove value */
	}
	return 1;
}

static int keys(lua_State *L)
{
	luaL_checktype(L, 1, LUA_TTABLE);
	lua_newtable(L);
	lua_pushnil(L);  /* first key */
	int i = 1;
	while (lua_next(L, 1)) {
		lua_pushvalue(L, -2);  /* key */
		lua_rawseti(L, 2, i++);
		lua_pop(L, 1);  /* remove value */
	}
	return 1;
}

static int filterto(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TFUNCTION);

	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_newtable(L);
	}
	else
	{
		luaL_checktype(L, 3, LUA_TTABLE);
	}

	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -3);  /* key */
		lua_pushvalue(L, -3);  /* value */
		lua_call(L, 2, 1);
		if (lua_toboolean(L, -1))
		{
			lua_pushvalue(L, -3); /* copy key */
			lua_pushvalue(L, -3); /* copy value */
			lua_rawset(L, 3);
		}
		lua_pop(L, 2);  /* remove value and result */
	}
	return 1;
}

static int filter(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -3);  /* key */
		lua_pushvalue(L, -3);  /* value */
		lua_call(L, 2, 1);
		if (!lua_toboolean(L, -1))
		{
			lua_pushvalue(L, -3);
			lua_pushnil(L);
			lua_rawset(L, 1);
		}
		lua_pop(L, 2);  /* remove value and result */
	}
	return 1;
}

static int ifiterto(lua_State *L)
{
	int i;
	int ti = 0;
	int n = aux_getn(L, 1);
	luaL_checktype(L, 2, LUA_TFUNCTION);

	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_newtable(L);
	}
	else
	{
		luaL_checktype(L, 3, LUA_TTABLE);
	}

	for (i = 1; i <= n; i++)
	{
		lua_rawgeti(L, 1, i);  /* get value */
		lua_pushvalue(L, 2);  /* function */
		lua_pushinteger(L, i);  /* 1st argument */
		lua_pushvalue(L, -3);  /* 2nd argument */
		lua_call(L, 2, 1);
		if (lua_toboolean(L, -1))
		{
			lua_pop(L, 1);  /* remove result */
			lua_rawseti(L, 3, ++ti);
		}
		else
		{
			lua_pop(L, 1);  /* remove result */
		}
	}
	return 1;
}

static int ifilter(lua_State *L) {
	ifiterto(L);
	int n1 = aux_getn(L, 1);
	int n2 = aux_getn(L, 3);
	for (int i = 1; i <= n2; i++)
	{
		lua_rawgeti(L, 3, i);
		lua_rawseti(L, 1, i);
	}
	for (int i = n2 + 1; i <= n1; i++)
	{
		lua_pushnil(L);
		lua_rawseti(L, 1, i);  /* t[i] = nil */
	}

	luaL_setn(L, 1, n2);
	lua_pop(L, 1);
	return 1;
}

static int mergeto(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TTABLE);
	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pushvalue(L, -2);  /* key */
		lua_pushvalue(L, -2);  /* value */
		lua_rawset(L, 2);
		lua_pop(L, 1);  /* remove value */
	}
	return 1;
}

static int imergeto(lua_State *L)
{
	int i;
	int ti = 0;
	int n1 = aux_getn(L, 1);
	int n2 = aux_getn(L, 2);
	int argscount = lua_gettop(L);
	int begin;
	if (argscount == 2)
	{
		begin = n2 + 1;
	}
	else
	{
		begin = lua_tointeger(L, 3);
		if (begin <= 0)
		{
			begin = n2 + 1;
		}
	}
	for (i = n2; i >= begin ; i--)
	{
		lua_rawgeti(L, 2, i);  /* get value */
		lua_rawseti(L, 2, i + n1);
	}
	for (i = 0; i < n1; i++)
	{
		lua_rawgeti(L, 1, i + 1);  /* get value */
		lua_rawseti(L, 2, i + begin);
	}
	return 1;
}

static int map(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -3);  /* key */
		lua_pushvalue(L, -3);  /* value */
		lua_call(L, 2, 1);
		lua_pushvalue(L, -3);	/* key */
		lua_pushvalue(L, -2);	/* result */
		lua_rawset(L, 1);
		lua_pop(L, 2);  /* remove value and result */
	}
	return 1;
}

static int mapto(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TFUNCTION);

	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_newtable(L);
	}
	else
	{
		luaL_checktype(L, 3, LUA_TTABLE);
	}

	lua_pushnil(L);  /* first key */
	while (lua_next(L, 1)) {
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -3);  /* key */
		lua_pushvalue(L, -3);  /* value */
		lua_call(L, 2, 1);
		lua_pushvalue(L, -3);	/* key */
		lua_pushvalue(L, -2);	/* result */
		lua_rawset(L, 3);
		lua_pop(L, 2);  /* remove value and result */
	}
	return 1;
}

static int imapto(lua_State *L)
{
	int i;
	int n = aux_getn(L, 1);
	luaL_checktype(L, 2, LUA_TFUNCTION);

	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_newtable(L);
	}
	else
	{
		luaL_checktype(L, 3, LUA_TTABLE);
	}

	for (i = 1; i <= n; i++)
	{
		lua_pushvalue(L, 2);  /* function */
		lua_pushinteger(L, i);  /* 1st argument */
		lua_rawgeti(L, 1, i);  /* 2nd argument */
		lua_call(L, 2, 1);
		lua_rawseti(L, 3, i);
	}
	return 1;
}

static int imap(lua_State *L) {
	imapto(L);
	int n = aux_getn(L, 1);
	for (int i = 1; i <= n; i++)
	{
		lua_rawgeti(L, 3, i);
		lua_rawseti(L, 1, i);
	}
	lua_pop(L, 1);
	return 1;
}

static int reduce(lua_State *L) {
	luaL_checktype(L, 1, LUA_TTABLE);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_pushnil(L);  /* first key */
		if (lua_next(L, 1))
		{
			lua_insert(L, 3);
		}
	}
	else
	{
		lua_pushvalue(L, 3);
		lua_pushnil(L);  /* first key */
	}
	while (lua_next(L, 1)) {
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -4);	/* initial value */
		lua_pushvalue(L, -4);  /* key */
		lua_pushvalue(L, -4);  /* value */
		lua_call(L, 3, 1);
		lua_replace(L, -4);
		lua_pop(L, 1);  /* remove value */
	}
	return 1;
}

static int ireduce(lua_State *L)
{
	int i = 1;
	int n = aux_getn(L, 1);
	luaL_checktype(L, 2, LUA_TFUNCTION);
	int argscount = lua_gettop(L);
	if (argscount == 2)
	{
		lua_rawgeti(L, 1, 1);
		i = 2;
	}
	else
	{
		lua_pushvalue(L, 3);
	}

	for (; i <= n; i++)
	{
		lua_pushvalue(L, 2);  /* function */
		lua_pushvalue(L, -2);	/* initial value */
		lua_pushinteger(L, i);  /* index */
		lua_rawgeti(L, 1, i);  /* value */
		lua_call(L, 3, 1);
		lua_replace(L, -2);
	}
	return 1;
}

static int indexof(lua_State *L)
{
	int i = 1;
	int n = aux_getn(L, 1);
	luaL_checkany(L, 2);
	int argscount = lua_gettop(L);
	if (argscount == 3)
	{
		i = lua_tonumber(L, 3);
	}

	for (; i <= n; i++)
	{
		lua_rawgeti(L, 1, i);  /* value */
		int b = lua_equal(L, -1, 2);
		lua_pop(L, 1);
		if (b)
		{
			lua_pushinteger(L, i);
			return 1;
		}
	}
	lua_pushboolean(L, 0);
	return 1;
}

static int sub(lua_State *L)
{
	int i = 1;
	int n = aux_getn(L, 1);
	int begin = lua_tonumber(L, 2);
	int end = lua_tonumber(L, 3);
	if (end <= 0)
	{
		end += n;
	}
	else if (end > n)
	{
		end = n;
	}
	lua_newtable(L);
	for (; begin < end; i++, begin++)
	{
		lua_rawgeti(L, 1, begin);
		lua_rawseti(L, -2, i);
	}
}

/* }====================================================== */


static const luaL_Reg tab_funcs[] = {
	{ "getvalues", values },
	{ "getkeys", keys },
	{ "filterto", filterto },
	{ "filtervalue", filter },
	{ "ifiltervalue", ifilter },
	{ "ifiterto", ifiterto },
	{ "count", count },
	{ "mergeto", mergeto },
	{ "imergeto", imergeto },
	{ "mapvalue", map },
	{ "mapto", mapto },
	{ "imapvalue", imap },
	{ "imapto", imapto },
	{ "reduce", reduce },
	{ "ireduce", ireduce },
	{ "getindexof", indexof },
	{ "subarray", sub },
  {NULL, NULL}
};


LUALIB_API int luaopen_table_ext(lua_State *L) {
  luaL_register(L, LUA_TABLIBNAME, tab_funcs);
  return 1;
}

