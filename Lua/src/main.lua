if jit then
	jit.off();
	jit.flush()
end
require "alias"
require "enum.index"
-- L_DataConfig=require "config.dataConfig"
-- L_GlConfig=require "config.glConfig"
L_Net=require 'net'
L_Proto=require 'proto.index'
L_Store = require "store.index"
L_UI = require "ui.index"
local tag="main"
local function _checkProject()
	setmetatable(
		_G,
		{
			__newindex = function(v1, v2, v3)
				if (v2 ~= "i" and v2 ~= "j" and v2 ~= "socket" and v2 ~= "ltn12" and v2~="reload") then
					error("不能添加全局变量 " .. v2, 2)
				else
					rawset(v1, v2, v3)
				end
			end,
			__index = function(t, k)
				if (k ~= "jit") then
					error("未注册的全局变量" .. k, 2)
				end
			end
		}
	)
end
C_LuaManager.ReadByteFiles("proto/protocal.bytes",function(proto)
	L_Proto.initialize(proto)
	L_Net.initialize(L_Proto)
	L_UI.initialize():next(function()
		L_UI.open("login")
		_checkProject()
		print(tag,"Game Start")
	end)
end)