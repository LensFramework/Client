﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Lens.Tools.TempMenu
{
    public class CreateScriptableObjectWindow : EditorWindow
    {
        private static CreateScriptableObjectWindow m_instance;
        private string m_clsName;
        private string m_dstPath;
        [MenuItem("Tools/CreateScriptableObject")]
        static void Init()
        {
            m_instance = EditorWindow.GetWindow<CreateScriptableObjectWindow>("CreateScriptableObjectWindow");
            m_instance.Show();
        }
        void OnDestroy()
        {
            m_instance = null;
        }
        void OnGUI()
        {
            m_clsName = EditorGUILayout.TextField("ClassName",m_clsName);
            m_dstPath = EditorGUILayout.TextField("DistPath",m_dstPath);
            if (GUILayout.Button("Create"))
            {
                var s = ScriptableObject.CreateInstance(m_clsName);
                AssetDatabase.CreateAsset(s, m_dstPath);
            }
        }
    }
}