﻿using Lens.Framework.Config;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEditor.Build.Pipeline;
using UnityEditor.Build.Pipeline.Interfaces;
using UnityEngine;

public class ConfigBuildTask 
{
    public void Run()
    {
        Config.ForceDispose();
        Config config = Config.GetInstance();
        config.SaveTable(new Config.MyOptions()
        {
            tableConfigType = typeof(Lens.Gameplay.Config.TableConfig),
        }, Application.dataPath + "/ResourcesAssets/Config/Table",(str)=> {
            UnityEditor.AssetDatabase.ImportAsset("Assets/ResourcesAssets/Config/Table/" + str);
        });
        config.Dispose();
    }
}