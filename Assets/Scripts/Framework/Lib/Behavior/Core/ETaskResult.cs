﻿using System;
using System.Collections.Generic;

namespace Lens.Framework.Libs
{
    public enum ETaskResult
    {
        Successed,
        Failed,
        Running
    }
}
