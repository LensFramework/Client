﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Lens.Gameplay.Core
{
    public enum ESceneType
    {
        Login=1,
        Sandbox=2,
        Battle=3
    }
}