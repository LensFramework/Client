cd luajit-2.1/src

# Android/ARM, armeabi-v7a (ARMv7 VFP), Android 4.0+ (ICS)
NDK=/Volumes/laohan-SSD/software/Android/android-ndk-r10e
NDKABI=19
NDKVER=$NDK/toolchains/arm-linux-androideabi-4.9
NDKP=$NDKVER/prebuilt/darwin-x86_64/bin/arm-linux-androideabi-
NDKF="--sysroot $NDK/platforms/android-$NDKABI/arch-arm" 
NDKARCH="-march=armv7-a -mfloat-abi=softfp -Wl,--fix-cortex-a8"

make clean
make HOST_CC="gcc -m32" CROSS=$NDKP TARGET_SYS=Linux TARGET_FLAGS="$NDKF $NDKARCH"
cp ./libluajit.a ../../android/jni/libluajit.a
make clean


export PATH=$PATH:$NDK

cd ../../pbc
ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk clean APP_ABI="armeabi-v7a,x86"
ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk APP_ABI="armeabi-v7a,x86"
cp ./obj/local/armeabi-v7a/libpbc.a ../android/jni/libpbc.a
ndk-build NDK_PROJECT_PATH=. APP_BUILD_SCRIPT=./Android.mk clean APP_ABI="armeabi-v7a,x86"


cd ../android
$NDK/ndk-build clean APP_ABI="armeabi-v7a,x86,arm64-v8a"
$NDK/ndk-build APP_ABI="armeabi-v7a"
cp libs/armeabi-v7a/libtolua.so ../Plugins/Android/libs/armeabi-v7a
$NDK/ndk-build clean APP_ABI="armeabi-v7a,x86,arm64-v8a"