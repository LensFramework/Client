return {
    state=require("store/global/state"),
    getters = require("store/global/getters"),
    mutations = require("store/global/mutations"),
    actions = require("store/global/actions")
}
