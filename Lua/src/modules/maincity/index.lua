local maincity={}
local mainCityProxy
local tag="modules:maincity"
function maincity.initialize(options)
    
end

function maincity.start(options)
    mainCityProxy=C_MainCityProxy.GetInstance()
    mainCityProxy:Initialize(nil)
end

function maincity.dispose()
    mainCityProxy:Dispose()
end

return maincity