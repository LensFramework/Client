
local pb = require('protobuf')
local protobuf={}
local protoCSCmd=require('proto/protoCSCmd')
local protoSCCmd=require('proto/protoSCCmd')

local decodeed = true;
----------------------------------------------------------------------------------------------
local c = require("protobuf.c")
local cext = require("protobuf.c.ext")
local _wmessage_int64 = cext._wmessage_int64
assert(nil ~= _wmessage_int64, "pbc-int64 is missing!!!!")
c._wmessage_int64 = function(t, k, v)
    local vt = type(v)
    if vt == "number" then
        return _wmessage_int64(t, k, v)
    elseif vt == "string" and #v == 8 then--TODO:test
        return _wmessage_int64(t, k, v)
    else
        local v64 = tolua.toint64(v)
        return _wmessage_int64(t, k, v64)
    end
end
c._wmessage_int52 = c._wmessage_int64
c._decode = cext._decode 

lltoa=cext.int64ToString
atoll=cext.int64ToNumber
local default_cache_tb = {}
local __decode = pb.decode
local decode = function(typename, buffer, length)
	if nil ~= buffer then
		local ret, err = __decode(typename, buffer, length);
		if false == ret then
			print(string.format("<color=#FF0000>%s</color>",err.."\t"..typename));
			decodeed = false;
			return false;
		end
		return ret
	end
	--default data
	local def = default_cache_tb[typename];
	if nil == def then
		def = pb.default(typename, {});
		default_cache_tb[typename] = def;
	end
	return def;
end

function protobuf.initialize(bytes)
    -- local bytes=C_LuaManager.LoadFileBytes("proto/protocal.pb")
    pb.register(bytes)
end
function protobuf.encode( type,cmd,data )
    if type=="cs" then
        local enum=protoCSCmd[cmd]
        if enum==nil then
            error("serialize cmd is null > "..cmd)
        else
            return pb.encode(enum.value,data),enum.id
        end
    elseif type=="sc" then
        local enum=protoSCCmd[cmd]
        if enum==nil then
            error("serialize cmd is null > "..cmd)
        else
            return pb.encode(enum.value,data),enum.id
        end
    else
        error("serialize type is error > "..type)
        return nil
    end
end

function protobuf.decode( type,cmd,data )
    if type=="cs" then
        local name=protoCSCmd[cmd]
        if name==nil then
            error("serialize cmd is null > "..cmd)
        else
            return decode(protoCSCmd[cmd].value,data)
        end
    elseif type=="sc" then
        local name=protoSCCmd[cmd]
        if name==nil then
            error("serialize cmd is null > "..cmd)
        else
            return decode(protoSCCmd[cmd].value,data)
        end
    else
        error("serialize type is error > "..type)
        return nil
    end
end

function protobuf.getSCCmd( id )
    return protoSCCmd:parse(id)
end

function protobuf.getSCCmd(id)
    return protoSCCmd:parse(id)
end

return protobuf