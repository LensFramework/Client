﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Lens.Framework.Managers;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;


namespace Lens.Gameplay.Modules.Update
{
    public class UpdateProxy : MonoBehaviour
    {
        public Slider progressSlider;
        public GameObject certainPanel; 
        private AsyncOperationHandle<List<IResourceLocator>> m_handler;
        private List<string> m_catelogs;
        // Use this for initialization
        void Start()
        {
            progressSlider.minValue = 0;
            progressSlider.maxValue = 1;
        }
        // Update is called once per frame
        void Update()
        {
            if (progressSlider!=null)
            {
                if (m_handler.IsValid())
                {
                    progressSlider.value = m_handler.PercentComplete;
                    if (m_handler.Status == AsyncOperationStatus.Succeeded)
                    {
                        //gameObject.SetActive(false);
                    }
                    else if (m_handler.Status == AsyncOperationStatus.Failed)
                    {
                        UnityEngine.Debug.LogError("更新失败");
                        if (certainPanel != null)
                        {
                            certainPanel.SetActive(true);
                        }
                    }
                }
            }
        }
        void OnDestroy()
        {
        }
        public async Task Initialize()
        {
            await UnityEngine.AddressableAssets.Addressables.InitializeAsync().Task;
            m_catelogs = await Addressables.CheckForCatalogUpdates().Task;
            if (m_catelogs != null && m_catelogs.Count > 0)
            {
                m_handler = ToPullContent(m_catelogs);
            }
            else
            {
                //gameObject.SetActive(false);
            }
        }
        public void Btn_ok()
        {
            m_handler = ToPullContent(m_catelogs);
            certainPanel.SetActive(false);
        }
        public void Btn_cancel()
        {
            Application.Quit();
        }
        private AsyncOperationHandle<List<IResourceLocator>> ToPullContent(List<string> catelogs)
        {
            return Addressables.UpdateCatalogs(catelogs);
        }
    }
}