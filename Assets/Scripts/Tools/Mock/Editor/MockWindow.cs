﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace ProtocolSimulation
{
    public class MockWindow : EditorWindow
    {
        private Vector2 m_packageScroll;
        private Vector2 m_paramScroll;
        private Mock m_protoSim;
        private List<MockMessage> m_searchResults=new List<MockMessage>();
        private MockMessage m_selectedMessage;
        private int m_selectedIndex;
        private string m_searchContent;
        private int m_msgId;
        private bool m_needRepaint;
        private bool m_isSearchIgnoreCase=true;
        private bool m_isSearchIgnoreNoIDExist=false;
        private Dictionary<string, int> m_nameToId=new Dictionary<string, int>();
        private static MockWindow m_instance;

        [MenuItem("Tools/Mock")]
        static void Init()
        {
            m_instance = EditorWindow.GetWindow<MockWindow>("网络模拟工具");
            m_instance.Initialize();
            m_instance.Show();
        }
        void Initialize()
        {
            m_protoSim = new Mock();
            m_msgId = 0;
            m_selectedIndex = 0;
            m_searchContent = "";
            m_nameToId=MockEditorInterval.GenProtoName2ID(m_protoSim.config.protoIDDefinePaths);
            Search();
        }
        void OnGUI()
        {
            if (m_protoSim==null)
            {
                Initialize();
            }
            GUI.color = Color.white;
            m_needRepaint = false;

            DrawTopToolbar();
            EditorGUILayout.BeginHorizontal();
            {
                DrawMsgList();

                DrawMsgDetails();
            }
            EditorGUILayout.EndHorizontal();

            if (m_needRepaint)
            {
                Repaint();
            }
        }

        /// <summary>
        /// 绘制顶部工具栏
        /// </summary>
        private void DrawTopToolbar()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                if (GUILayout.Button("Gen Proto", EditorStyles.toolbarButton))
                {
                    MockEditorInterval.GenAllProto();
                }
                if (GUILayout.Button("Refresh", EditorStyles.toolbarButton))
                {
                    Initialize();
                    m_needRepaint = true;
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Settings", EditorStyles.toolbarButton))
                {
                    PingObject(m_protoSim.config);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制搜索框
        /// </summary>
        private void DrawSearchBox()
        {
            EditorGUILayout.BeginHorizontal();
            {
                var searchContent = SearchField(m_searchContent);
                if (!searchContent.Equals(m_searchContent))
                {
                    m_searchContent = searchContent;
                    Search();
                    m_needRepaint = true;
                }
                if (!m_isSearchIgnoreCase)
                {
                    GUI.color = Color.green;
                }
                string a = m_isSearchIgnoreCase ? "Aa" : "[Aa]";
                if (GUILayout.Button(a,EditorStyles.label))
                {
                    m_isSearchIgnoreCase = !m_isSearchIgnoreCase;
                    Search();
                    m_needRepaint = true;
                }
                GUI.color = Color.white;

                if (m_nameToId.Count>0)
                {
                    if (m_isSearchIgnoreNoIDExist)
                    {
                        GUI.color = Color.green;
                    }
                    string b = m_isSearchIgnoreNoIDExist ? "[ID]" : "ID";
                    if (GUILayout.Button(b, EditorStyles.label))
                    {
                        m_isSearchIgnoreNoIDExist = !m_isSearchIgnoreNoIDExist;
                        Search();
                        m_needRepaint = true;
                    }
                    GUI.color = Color.white;
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制协议列表
        /// </summary>
        private void DrawMsgList()
        {
            EditorGUILayout.BeginVertical(EditorStyles.textField, GUILayout.MaxWidth(300));
            {
                DrawSearchBox();
                EditorGUILayout.LabelField("", EditorStyles.textField, GUILayout.Height(1));

                m_packageScroll = EditorGUILayout.BeginScrollView(m_packageScroll);
                {
                    if (m_selectedIndex < 0 || m_selectedIndex > m_protoSim.list.Count - 1)
                    {
                        m_selectedIndex = 0;
                    }
                    List<MockMessage> mocks = m_searchResults;
                    for (int i = 0; i < mocks.Count; i++)
                    {
                        var rect = EditorGUILayout.BeginHorizontal();
                        {
                            if (m_selectedIndex == i)
                            {
                                GUI.color = Color.green;
                                GUI.Button(rect, "", EditorStyles.helpBox);
                                GUI.color = Color.white;
                            }
                            EditorGUILayout.LabelField(mocks[i].name);
                        }
                        EditorGUILayout.EndHorizontal();
                        if (GUI.Button(rect, "", EditorStyles.helpBox))
                        {
                            m_selectedIndex = i;
                            m_needRepaint = true;
                            m_msgId = 0;
                        }
                    }
                    if (m_selectedIndex >= 0 && m_selectedIndex < mocks.Count)
                    {
                        m_selectedMessage = mocks[m_selectedIndex];
                    }

                }
                EditorGUILayout.EndScrollView();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 绘制协议详情
        /// </summary>
        private void DrawMsgDetails()
        {
            EditorGUILayout.BeginVertical();
            {
                m_paramScroll = EditorGUILayout.BeginScrollView(m_paramScroll);
                {
                    if (m_selectedMessage != null)
                    {
                        EditorGUILayout.LabelField("Name", m_selectedMessage.name);
                        EditorGUILayout.LabelField("NameSpace", m_selectedMessage.nameSpace);
                        EditorGUILayout.LabelField("", EditorStyles.textField, GUILayout.Height(1));
                        for (int i = 0; i < m_selectedMessage.members.Count; i++)
                        {
                            DrawMsgParams(m_selectedMessage.members[i]);
                        }
                    }
                }
                EditorGUILayout.EndScrollView();
                GUILayout.FlexibleSpace();
                DrawFooterToolbar();
            }
            EditorGUILayout.EndVertical();
        }

        /// <summary>
        /// 绘制页脚的工具栏
        /// </summary>
        private void DrawFooterToolbar()
        {
            if (!EditorApplication.isPlaying)
            {
                GUI.color = Color.red;
                EditorGUILayout.LabelField("使用发送功能需要先启动Unity播放");
                GUI.color = Color.white;
            }
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            {
                GUI.enabled = EditorApplication.isPlaying;
                GUILayout.Label("msgID");
                var existKeyIndex = false;
                if (m_selectedMessage != null)
                {
                    existKeyIndex = m_nameToId.ContainsKey(m_selectedMessage.name);
                }
                if (m_msgId == 0 && m_selectedMessage != null && existKeyIndex )
                {
                    m_msgId = m_nameToId[m_selectedMessage.name];
                }
                m_msgId = EditorGUILayout.IntField(m_msgId);
                if (m_msgId <= 0)
                {
                    GUI.color = Color.red;
                    GUILayout.Label("请输入协议ID");
                    GUI.color = Color.white;
                }
                GUILayout.FlexibleSpace();
                GUI.enabled = EditorApplication.isPlaying && m_msgId > 0;
                if (GUILayout.Button("发送到服务器", EditorStyles.toolbarButton))
                {
                    m_protoSim.ClientToServer(m_msgId, m_selectedMessage);
                }
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("发送到客户端", EditorStyles.toolbarButton))
                {
                    m_protoSim.ServerToClient(m_msgId, m_selectedMessage);
                }
                GUI.enabled = true;
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 绘制协议数据结构
        /// </summary>
        /// <param name="member"></param>
        /// <param name="interval"></param>
        private void DrawMsgParams(MockMember member,float interval=1)
        {
            if (member.type == typeof(int) || member.type == typeof(long) || member.type == typeof(uint) || member.type == typeof(ulong))
            {
                if (member.value == null)
                {
                    member.value = 0;
                }
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(interval);
                    member.value = EditorGUILayout.IntField(member.name, (int)member.value);
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (member.type.IsEnum)
            {
                if (member.value == null)
                {
                    member.value = 1;
                }
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(interval);
                    member.value = EditorGUILayout.IntField(member.name, (int)member.value);
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (member.type == typeof(string))
            {
                if (member.value == null)
                {
                    member.value = "";
                }
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(interval);
                    member.value = EditorGUILayout.TextField(member.name, (string)member.value);
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (member.type == typeof(float) || member.type == typeof(double))
            {
                if (member.value == null)
                {
                    member.value = 0;
                }
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(interval);
                    member.value = EditorGUILayout.FloatField(member.name, (float)member.value);
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (member.type == typeof(bool))
            {
                if (member.value == null)
                {
                    member.value = false;
                }
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space(interval);
                    member.value = EditorGUILayout.Toggle(member.name, (bool)member.value);
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (member.type.IsGenericType)
            {
                if (IsClass(member.genericType))
                {
                    var list = (List<MockMessage>)member.value;
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(member.name);
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("+", EditorStyles.label))
                        {
                            list.Add(new MockMessage(member.genericType));
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    for (int j = 0; j < list.Count; j++)
                    {
                        var m = list[j].members;
                        EditorGUILayout.BeginVertical();
                        {
                            for (int i = 0; i < m.Count; i++)
                            {
                                DrawMsgParams(m[i], interval + 18);
                            }
                            EditorGUILayout.BeginHorizontal();
                            {
                                GUILayout.FlexibleSpace();
                                if (GUILayout.Button("-", EditorStyles.label))
                                {
                                    list.RemoveAt(j);
                                }
                            }
                            EditorGUILayout.EndHorizontal();
                        }
                        EditorGUILayout.EndVertical();
                        if (j != list.Count - 1)
                        {
                            EditorGUILayout.LabelField("", EditorStyles.textField, GUILayout.Height(1));
                        }
                    }
                }
                else
                {
                    var list = (List<MockMember>)member.value;
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField(member.name);
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("+", EditorStyles.label))
                        {
                            list.Add(new MockMember(member.genericType));
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                    for (int j = 0; j < list.Count; j++)
                    {
                        DrawMsgParams(list[j], interval + 18);
                        EditorGUILayout.BeginHorizontal();
                        {
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("-", EditorStyles.label))
                            {
                                list.RemoveAt(j);
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }
            else
            {
                if (member == null || member.value == null)
                {
                    return;
                }
                var m = ((MockMessage)member.value).members;
                EditorGUILayout.LabelField(member.name);
                for (int i = 0; i < m.Count; i++)
                {
                    DrawMsgParams(m[i],interval+18);
                }
            }
        }

        /// <summary>
        /// 获得Unity原生的搜索框
        /// </summary>
        /// <param name="value"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static string SearchField(string value, params GUILayoutOption[] options)
        {
            System.Reflection.MethodInfo info = typeof(EditorGUILayout).GetMethod("ToolbarSearchField", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static, null, new System.Type[] { typeof(string), typeof(GUILayoutOption[]) }, null);
            if (info != null)
            {
                value = (string)info.Invoke(null, new object[] { value, options });
            }
            return value;
        }

        private bool IsClass(Type paramType)
        {
            if (paramType == typeof(int) ||
                paramType == typeof(uint) ||
                paramType == typeof(long) ||
                paramType == typeof(ulong) ||
                paramType == typeof(string) ||
                paramType == typeof(float) ||
                paramType == typeof(double) ||
                paramType == typeof(bool))
            {
                return false;
            }
            else if (paramType.IsEnum)
            {
                return false;
            }
            else if (paramType.IsGenericType)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 跳转并选中编辑Unity Asset
        /// </summary>
        /// <param name="obj"></param>
        void PingObject(UnityEngine.Object obj)
        {
            int instanceId = obj.GetInstanceID();
            EditorGUIUtility.PingObject(instanceId);
            Selection.activeObject = obj;
        }

        /// <summary>
        /// 搜索过滤，根据条件改变可选数据列表的条目
        /// </summary>
        private void Search()
        {
            m_selectedIndex = 0;
            m_searchResults.Clear();
            var searchOption = StringComparison.CurrentCulture;
            if (m_isSearchIgnoreCase)
            {
                searchOption = StringComparison.CurrentCultureIgnoreCase;
            }
            for (int i = 0; i < m_protoSim.list.Count; i++)
            {
                var msg = m_protoSim.list[i];
                var isCheckConditionSuc = true;
                if (!string.IsNullOrEmpty(m_searchContent) &&
                    !Contains(msg.name,m_searchContent, searchOption))
                {
                    isCheckConditionSuc = false;
                }
                if (m_isSearchIgnoreNoIDExist &&
                    !m_nameToId.ContainsKey(msg.name))
                {
                    isCheckConditionSuc = false;
                }
                if (isCheckConditionSuc)
                {
                    m_searchResults.Add(msg);
                }
            }
        }
        public static bool Contains(string source, string toCheck, StringComparison comp = StringComparison.OrdinalIgnoreCase)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}