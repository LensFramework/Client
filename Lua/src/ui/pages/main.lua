local page=class("main",G_StackBase)
local maincity = require "modules.maincity.index"
local tag="page:main"


local globalStates={
    playerId=function (oldValue,newValue)
        print(tag,"playerId",oldValue,newValue)
    end,
    rank=function (oldValue,newValue)
        print(tag,"rank",oldValue,newValue)
    end,
    tag=function (oldValue,newValue)
        print(tag,"tag",oldValue,newValue)
    end
}
local userStates={
    userId=function (oldValue,newValue)
        print(tag,"userId",oldValue,newValue)
    end
}
local function onBack()
    L_UI.open("confirm",{
        title="确定退出吗？",
        ok=function ()
            print(tag,"ok")
            L_UI.back()
            L_UI.close("heroinfo")
        end,
        cancel=function ()
            print(tag,"cancel")
        end
    })
end

local function onClickPackage()
    L_UI.open("package")
end

function page:ctor(...)
    
end

function page.created(obj,config)
    print(tag,obj,config)
    local loginBtn=obj.transform:Find("MainForms/Btn_Exit")
    local btn=loginBtn:GetComponent(typeof(UnityEngine.UI.Button))
    local package=obj.transform:Find("MainForms/Btn_Package")
    local pagekagebtn=package:GetComponent(typeof(UnityEngine.UI.Button))
    btn.onClick:AddListener(onBack)
    pagekagebtn.onClick:AddListener(onClickPackage)
end

function page.open(options)
    L_Store.mapStates("global",globalStates)
    L_Store.mapStates("user",userStates)
    C_Config.GetMemConfig():SetSceneType(E_SceneType.Sandbox)
    maincity.initialize()
    maincity.start()
    print(tag,"open")
    L_UI.open("heroinfo")
	-- 获取配置表参数
	-- local globalVari=L_DataConfig.LoadConfig("global_variables")
	-- local va=globalVari["init_attack_radii"].config_name_chs
	-- print(tag,va)
	-- 测试store单项数据流，未完善
	L_Store.dispatch("global","testActions",{
        playerId=100,
        rank=9999,
        tag="globalstate=>"
    })
    local playerId=L_Store.getter("global","playerId")
    print(tag,"getter playerId",playerId)
    L_Store.dispatch("user","getUserInfo",{
        userId=9920
    }):next(function (data)
        print(tag,"getUserInfo",data)
    end,function (error)
        print(tag,"error",error)
    end)
    L_UI.closeMask()
end

function page.close(options)
    L_Store.unmapStates("global",globalStates)
    L_Store.unmapStates("user",userStates)
    maincity.dispose()
end

function page.destroy(options)
    
end

return page